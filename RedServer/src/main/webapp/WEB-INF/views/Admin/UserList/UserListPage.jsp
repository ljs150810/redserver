<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ include file="../../Section/Admin/Header.jsp"%>

<style>
* {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
		font-family: 'Nanum Gothic', sans-serif;
		font-size: 14px;
	}

	/* Global Styles */
	body {
		line-height: 1.6;
		background-color: #f8f8f8;
		color: #333;
	}

	.container-fluid {
		padding: 20px;
	}

	/* Table Styles */
	.table-responsive {
		overflow-x: auto;
	}

	.table-bordered th, .table-bordered td {
		white-space: nowrap;
		text-align: left;
	}

	/* Pagination CSS */
	.pagination1 {
		text-align: center;
		margin-top: 20px;
	}

	.pagination1 ul {
		padding: 0;
	}

	.pagination1 a {
		display: inline-block;
		padding: 5px 10px;
		margin: 2px;
		border: 1px solid #ccc;
		background-color: #fff;
		text-decoration: none;
		color: #333;
		border-radius: 3px;
	}

	.pagination1 a:hover {
		background-color: #f5f5f5;
		color: black;
	}

	.pagination1 .selected-page {
		background-color: #ee5e5e;
		color: #fff;
		border-color: #ee5e5e;
		font-weight: bold;
	}

	/* Mobile Styles */
	@media (max-width: 768px) {
		h1, h6 {
			font-size: 1.2rem;
			text-align: center;
		}

		.table-bordered th, .table-bordered td {
			font-size: 0.9rem;
			padding: 8px;
		}

		.table-responsive {
			overflow-x: auto;
		}

		.pagination1 {
			font-size: 0.9rem;
		}

		.pagination1 a {
			padding: 5px;
			margin: 1px;
		}

		.pagination1 ul {
			display: flex;
			justify-content: center;
			flex-wrap: wrap;
		}

		.container-fluid {
			padding: 10px;
		}
	}

	@media (max-width: 480px) {
		h1, h6 {
			font-size: 1rem;
		}

		.table-bordered th, .table-bordered td {
			font-size: 0.8rem;
		}

		.pagination1 a {
			padding: 4px;
			margin: 1px;
		}
	}
	
	.pagination1 li{
	list-style: none;
	display: inline-block;
}
</style>

<script>
	
	function doAjax(param, mappingUrl) {
		var returnVal;
		$.ajax({
			type : "POST",
			async : false,
			url : mappingUrl,
			data : param,
			contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			traditional : true,
			success : function(res) {
				returnVal = res;
			},
			error : function(xhr, status, error) {
				if(xhr.status == 500){
					location.href = "/";
				} else {
					alert('오류가 발생했습니다.');
					return;
				}
			},
			beforeSend: function(xhr){
				xhr.setRequestHeader("AJAX", "true");
			}
		});
		
		return returnVal;
	}
	
	function doSubmit(str) {
		var form = $("#MainForm");

		form.attr("action", str);
		form.attr("method", "POST");

		form.submit();
	}
	
	function changePage(pageNum) {
	    var param = {
	        pageNum : pageNum
	    }
	    
	    var mappingUrl = "/Admin/UserInfo.do";
	    
	    $.ajax({
	        type : "POST",
	        url : mappingUrl,
	        data : param,
	        contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
	        success : function(res) {
	            $('body').html(res);
	        },
	        error : function(xhr, status, error) {
				if(xhr.status == 500){
					location.href = "/";
				} else {
					alert('오류가 발생했습니다.');
					return;
				}
			},
			beforeSend: function(xhr){
				xhr.setRequestHeader("AJAX", "true");
			}
	    });
	}

</script>
<!-- Body Start -->
<body>
<div style="height: 100%;" class="container-fluid">

	<h1 class="h3 mb-2 text-gray-800">레드서버 계정 목록</h1>
	
	<form id="MainForm">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">가입 유저정보 목록</h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>아이디</th>
								<th>비밀번호</th>
								<th>신청자</th>
								<th>이메일</th>
								<th>생성일</th>
								<th>IP</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${getUserInfoList == null}">
								
							</c:if>
							
							<c:choose>
				                <c:when test="${empty getUserInfoList}">
				                    <tr>
				                    	<td colspan="6">계정목록이 없습니다.</td>
				                    </tr>
				                </c:when>
				                <c:otherwise>
				                    <c:forEach var="user" items="${getUserInfoList}">
							            <tr>
							                <td>${user.userId}</td>
							                <td>${user.userPw}</td>
							                <td>${user.userNick}</td>
							                <td>${user.userEmail}</td>
							                <td>${user.regDate}</td>
							                <td>${user.clientIp}</td>
							            </tr>
							        </c:forEach>
				                </c:otherwise>
				            </c:choose>
					    </tbody>
					</table>
				</div>
			</div>
			<section id="pagination" class="pagination1">
			    <c:if test="${pagingVo.totalPage > 1}">
			        <ul>
			            <c:forEach var="i" begin="1" end="${pagingVo.totalPage}">
			                <li>
			                    <a href="javascript:changePage(${i});" class="${i == pagingVo.pageNum ? 'selected-page' : ''}">${i}</a>
			                </li>
			            </c:forEach>
			        </ul>
			    </c:if>
			</section>
		</div>
	</form>
</div>
</body>
<!-- Body End -->

<%@ include file="../../Section/Admin/Footer.jsp"%>