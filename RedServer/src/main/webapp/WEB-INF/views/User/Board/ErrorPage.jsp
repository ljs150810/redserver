<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="/img/cross-gate__1.jpg"/>
<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
<meta charset="EUC-KR">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<title>레드서버 | 에러 페이지</title>
<link rel="stylesheet" href="/css/DownLoad.css?ver=20240506" />
<style>
body {
    background-color: #f8f9fa;
    color: #333;
}

.error-container {
    text-align: center;
    padding: 100px 20px;
}

.error-container img {
    width: 330px;
}

.error-title {
    font-size: 30px;
    color: #d9534f;
    font-weight: bold;
    margin-bottom: 20px;
}

.error-message {
    font-size: 18px;
    margin-bottom: 30px;
}

.backButton {
    display: inline-block;
    padding: 12px 30px;
    background-color: #5bc0de;
    color: white;
    border-radius: 5px;
    text-decoration: none;
    font-weight: bold;
    transition: background-color 0.3s ease;
}

.backButton:hover {
    background-color: #31b0d5;
}

.go-home {
    margin-top: 20px;
}

@keyframes float {
    0%, 100% {
        transform: translateY(0);
    }
    50% {
        transform: translateY(-10px);
    }
}

.error-container img {
    animation: float 3s infinite ease-in-out;
    border-radius: 50%;
    padding: 30px;
}
</style>
</head>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
    <!-- Main Content -->
    <main>
        <div class="container error-container">
            <img src="/img/fixQue.png" alt="Error Icon" />
            <h1 class="error-title">뀨! 죄송해요.. 오류가 발생했네요..뀨!</h1>
            <p class="error-message">원활한 서비스를 제공하기위해 정신없이 고치고 있어요 뀨!</p>
            <a href="/Login/goMainPage.do" class="backButton">메인페이지로 돌아가기</a>
        </div>
    </main>

    <%@ include file="../../Section/User/Footer.jsp"%>
</body>
</html>
