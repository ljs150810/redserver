<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/Board.css?ver=20240506" />
</head>
<script>

</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	<form id="boardRead">	
	    <main>
	        <div class="container">
	            <section class="categoriesTable">
	            	<h1>※레드서버※</h1>
	            </section>
	
	            <section class="featured">
	            
	            </section>
	        </div>
	        
	    <!-- Footer -->
	       <%@ include file="../../Section/User/Footer.jsp"%>
	    </main>
	</form>
</body>
</html>