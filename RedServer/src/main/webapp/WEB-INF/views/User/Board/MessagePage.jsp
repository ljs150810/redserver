<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
<style>
body {
	font-family: Arial, sans-serif;
	background-color: #f4f4f4;
	margin: 0;
	padding: 20px;
}

h1 {
	color: #333;
}

.message-container {
	background: #fff;
	padding: 20px;
	border-radius: 8px;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
}

.message-item {
	display: flex;
	align-items: center;
	padding: 10px;
	border-bottom: 1px solid #e0e0e0;
}

.message-item:last-child {
	border-bottom: none;
}

.message-item input[type="checkbox"] {
	margin-right: 15px;
}

.message-details {
	flex: 1;
}

.message-title {
	font-weight: bold;
	color: #007bff;
}

.message-time {
	font-size: 0.9em;
	color: #888;
}

.message-actions button {
	background: #007bff;
	color: #fff;
	border: none;
	padding: 5px 10px;
	border-radius: 4px;
	margin-left: 10px;
	cursor: pointer;
	transition: background 0.3s;
}

.message-actions button:hover {
	background: #0056b3;
}
</style>

</head>
<body>
    <h1>메시지 함</h1>
    <div class="message-container">
    
        <div class="message-item">
            <input type="checkbox">
            <div class="message-details">
                <div class="message-title">제목: 예시</div>
                <div class="message-time">보낸 시간: 2024-07-14 10:00</div>
            </div>
            <div class="message-actions">
                <button onclick="replyMessage()">답장</button>
                <button onclick="deleteMessage()">삭제</button>
            </div>
        </div>
        <div class="message-item">
            <input type="checkbox">
            <div class="message-details">
                <div class="message-title">제목: 예시</div>
                <div class="message-time">보낸 시간: 2024-07-13 15:30</div>
            </div>
            <div class="message-actions">
                <button onclick="replyMessage()">답장</button>
                <button onclick="deleteMessage()">삭제</button>
            </div>
        </div>
        
    </div>
</body>
</html>