<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/CharacterView.css?ver=20241120" />
</head>
<script>
$(document).ready(function() {
	leftBtnClick();
    rightBtnClick();
    
    var selectAccountId = "${selectAccountId}";
    
    if(selectAccountId != ""){
    	$("#acSelectBox").val(selectAccountId).prop("seletect", true);
    }
    
    itemTotalView();
    petTotalView();
    currencyTotalView();
});

function leftBtnClick() {
    $('#leftBtn').click(function() {
        var registNumber = parseInt($(this).val());
        var accountId = $('#acId').val();
        
        $('#registNo').val(registNumber);
        doSubmit("/Login/UserAcProc.do");
    });
}

function rightBtnClick() {
    $('#rightBtn').click(function() {
        var registNumber = parseInt($(this).val());
        var accountId = $('#acId').val();
        
        $('#registNo').val(registNumber);
        
        doSubmit("/Login/UserAcProc.do");
    });
}

function itemTotalView() {
	$('#itemTotalView').click(function(){
		doSubmit("/Login/TotalItemViewProc.do");	
	});
}

function petTotalView() {
	$('#petTotalView').click(function(){
		doSubmit("/Login/TotalPetViewProc.do");	
	});
}

function currencyTotalView(){
	$('#currencyTotalView').click(function(){
		doSubmit("/Login/TotalCurrencyViewProc.do");	
	});
}



function doSubmit(str) {
	var form = $("#characterView");

	form.attr("action", str);
	form.attr("method", "POST");

	form.submit();
}

</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	<form id="characterView" method="post">
    <main>
    <input type="hidden" id="accountId" name="accountId" value="${selectAccountId}" />
    <input type="hidden" id="accountId_1" name="accountId_1" value="${acId}"/>
    <input type="hidden" id="registNo" name="registNo" />
        <div class="container">
			<section class="featured-container">
				<section class="featured">
					<input type="button" id="itemTotalView"  class= "searchBtn" value="아이템 종합"/>
					<input type="button" id="petTotalView" class= "searchBtn" value="수마 종합"/>
					<input type="button" id="currencyTotalView" class= "searchBtn" value="화폐 종합"/>
				</section>
				<c:choose>
					<c:when test="${empty CharacterItemInfo and empty CharacterPetInfo}">
				        <section class="featured">
			            <div style="padding: 20px;">
			            	<c:if test="${regNo == 14}">
			            		<div style="padding: 1px;">
			            			<span style="font-weight: bold; color: blue;">[좌측 캐릭터 정보]</span>
			            		</div>
			            	</c:if>
							<c:if test="${regNo == 15}">
								<div style="padding: 1px;">
									<span style="font-weight: bold; color: blue;">[우측 캐릭터 정보]</span>
								</div>
							</c:if>
			            	<table class="acView1">
			        			<thead>
			            			<tr>
			                			<th colspan="3" style="width : 85%; table-layout: fixed;">
			                			 	캐릭터명 : X
			                			 </th>
			            			</tr>
			        			</thead>
			        			<tbody class="equipmentItem">
			            			<tr>
			            				<td style="padding-right: 0px;">장식1</td>
			            				<td>머리</td>
			            				<td style="padding-right: 15px;">장식2</td>
			            			</tr>
			            			<tr>
			            				<td style="padding-right: 5px;">오른손</td>
			                			<td rowspan="2">
			                				X
			                			</td>
			            				<td style="padding-right: 15px;">왼손</td>
			            			</tr>
			            			<tr>
			            				<td style="padding-right: 5px;">몸</td>
			            				<td style="padding-right: 14px;">발</td>
			            			</tr>
			            			<tr>
			            				<td class="none"></td>
			            				<td class="none"></td>
			            				<td style="padding-right: 0px;">속성</td>
			            			</tr>
			        			</tbody>
			    			</table>
			            	<table class="acView2">
			        			<thead>
			            			<tr>
			                			<th colspan="6" >캐릭터 상태</th>
			            			</tr>
			        			</thead>
			        			<tbody>
			            			<tr>
			                			<th>레벨</th>
			                			<td></td>
			                			<th>HP</th>
			                			<td></td>
			                			<th>랭크업</th>
			                			<td></td>
			            			</tr>
			            			<tr>
			                			<th>직업</th>
			                			<td></td>
			                			<th>MP</th>
			                			<td></td>
			                			<th>로그인 횟수</th>
			                			<td></td>
			            			</tr>
			            			<tr>
			                			<th>보유금액(G)</th>
			                			<td></td>
			                			<th>작업 시간</th>
			                			<td></td>
			                			<th>환 생</th>
			                			<td></td>
			            			</tr>
			        			</tbody>
			    			</table>
			    		</div>
						<c:if test="${regNo == 15}">
							<div class="pageBtn left-btn">
								<button title="좌측 캐릭터" id='leftBtn' value='14'>&lt;</button>
							</div>
						</c:if>
						<c:if test="${regNo == 14}">
							<div class="pageBtn right-btn">
								<button title="우측 캐릭터" id='rightBtn' value='15'>&gt;</button>
							</div>
						</c:if>
						<hr class="hr-10">
			            <div  style="display: flex; justify-content: space-around;">
			            <section class="featured">
				            <div style="font-weight : bold;">인벤토리</div>
				        </section>
				        <section class="featured">
				            <div style="font-weight : bold;">창고</div>
				        </section>
				        </div>
			            <div class="itemsView">
				            <div class="itemView1">
								<h1>소지하고 있는 아이템이 없습니다.</h1>
				            </div>
				            <div class="itemView2">
								<h1>소지하고 있는 아이템이 없습니다.</h1>
				            </div>
						</div>
						
						<hr class="hr-10">
						
						<div class="petsView">
				            <div class="petView1">
					           	<table class="petTable1">
					           		<tbody>
					           			<h1>소지한 펫이 없습니다.</h1>		
					           		</tbody>
					           	</table>
				            </div>
				            <div class="petView2">
					           	<table class="petTable2">
					           		<tbody>
					           			<h1>소지한 펫이 없습니다.</h1>		
					           		</tbody>
					           	</table>
				            </div>
						</div>
		            </section>
				    </c:when>
					<c:otherwise>
		            <section class="featured">
			            <div style="padding: 20px;">
			            	<c:if test="${regNo == 14}">
			            		<div style="padding: 1px;">
			            			<span style="font-weight: bold; color: blue;">[좌측 캐릭터 정보]</span>
			            		</div>
			            	</c:if>
							<c:if test="${regNo == 15}">
								<div style="padding: 1px;">
									<span style="font-weight: bold; color: blue;">[우측 캐릭터 정보]</span>
								</div>
							</c:if>
			            	<table class="acView1">
			        			<thead>
			            			<tr>
			                			<th colspan="3" style="width : 85%; table-layout: fixed;">캐릭터명 : ${CharacterItemInfo[0].name}</th>
			            			</tr>
			        			</thead>
			        			<tbody class="equipmentItem">
			            			<tr>
			            			<c:choose>
			            				<c:when test="${Equipment5 == null}">
			            					<td style="padding-right: 0px;">장식1</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 0px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment5}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
			                						<h1>${EquipName5}</h1> <br> ${EquipLevel5} 랭크 <br> 내구도 : ${EquipDurability5} / ${EquipMaxDurability5}
			                						<br>
			                						<h1>[ 능력치 ]</h1>
			                						LP : ${EquipLP5} / FP : ${EquipFP5}<br>  
			                						공격력 : ${EquipAtk5} / 방어력 : ${EquipDef5}<br>
			                						민첩 : ${EquipAgil5} / 정신 : ${EquipMagic5}<br>
			                						치명 : ${EquipCri5} / 반격 : ${EquipCounter5}<br>
			                						명중 : ${EquipHit5} / 회피 : ${EquipAvoid5} / 회복 : ${EquipRcv5}
			                						<h1>[ 저항 ]</h1>
			                						독 : ${EquipPoison5} / 수면 : ${EquipSleep5} / 석화 : ${EquipStone5}<br>
			                						취기 : ${EquipDrunk5} / 혼란 : ${EquipConf5} / 망각 : ${EquipAmne5}
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			                		<c:choose>
			            				<c:when test="${Equipment0 == null}">
			            					<td>머리</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 0px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment0}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
				                					<h1>${EquipName0}</h1> <br> ${EquipLevel0} 랭크 <br> 내구도 : ${EquipDurability0} / ${EquipMaxDurability0} 
				                					<br>
													<h1>[ 능력치 ]</h1>
													LP : ${EquipLP0} / FP : ${EquipFP0}<br>  
													공격력 : ${EquipAtk0} / 방어력 : ${EquipDef0}<br>
													민첩 : ${EquipAgil0} / 정신 : ${EquipMagic0}<br>
													치명 : ${EquipCri0} / 반격 : ${EquipCounter0}<br>
													명중 : ${EquipHit0} / 회피 : ${EquipAvoid0} / 회복 : ${EquipRcv0}
													<h1>[ 저항 ]</h1>
													독 : ${EquipPoison0} / 수면 : ${EquipSleep0} / 석화 : ${EquipStone0}<br>
													취기 : ${EquipDrunk0} / 혼란 : ${EquipConf0} / 망각 : ${EquipAmne0}
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			                		<c:choose>
			            				<c:when test="${Equipment6 == null}">
			            					<td style="padding-right: 15px;">장식2</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 15px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment6}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
				                					<h1>${EquipName6}</h1> <br> ${EquipLevel6} 랭크 <br> 내구도 : ${EquipDurability6} / ${EquipMaxDurability6}
				                					<br>
					                				<h1>[ 능력치 ]</h1>
					                				LP : ${EquipLP6} / FP : ${EquipFP6}<br>  
					                				공격력 : ${EquipAtk6} / 방어력 : ${EquipDef6}<br>
					                				민첩 : ${EquipAgil6} / 정신 : ${EquipMagic6}<br>
					                				치명 : ${EquipCri6} / 반격 : ${EquipCounter6}<br>
					                				명중 : ${EquipHit6} / 회피 : ${EquipAvoid6} / 회복 : ${EquipRcv6}
					                				<h1>[ 저항 ]</h1>
					                				독 : ${EquipPoison6} / 수면 : ${EquipSleep6} / 석화 : ${EquipStone6}<br>
					                				취기 : ${EquipDrunk6} / 혼란 : ${EquipConf6} / 망각 : ${EquipAmne6} 
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			            			</tr>
			            			<tr>
			                		<c:choose>
			            				<c:when test="${Equipment2 == null}">
			            					<td style="padding-right: 5px;">오른손</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 5px;" class="tooltipForItem"> 
			                					<img src="/imgs/ItemNo/${Equipment2}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
				                					<h1>${EquipName2}</h1> <br> ${EquipLevel2} 랭크 <br> 내구도 : ${EquipDurability2} / ${EquipMaxDurability2}
				                					<br>
					                				<h1>[ 능력치 ]</h1>
					                				LP : ${EquipLP2} / FP : ${EquipFP2}<br>  
					                				공격력 : ${EquipAtk2} / 방어력 : ${EquipDef2}<br>
					                				민첩 : ${EquipAgil2} / 정신 : ${EquipMagic2}<br>
					                				치명 : ${EquipCri2} / 반격 : ${EquipCounter2}<br>
					                				명중 : ${EquipHit2} / 회피 : ${EquipAvoid2} / 회복 : ${EquipRcv2}
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			                			<td rowspan="2">
			                				<img src="/imgs/ItemImages/${CharacterItemInfo[0].baseImageNumber}.gif?ref_type=heads">
			                			</td>
			                		<c:choose>
			            				<c:when test="${Equipment3 == null}">
			            					<td style="padding-right: 15px;">왼손</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 15px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment3}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
			                						<h1>${EquipName3}</h1> <br> ${EquipLevel3} 랭크 <br> 내구도 : ${EquipDurability3} / ${EquipMaxDurability3}
			                						<br>
			                						<h1>[ 능력치 ]</h1>
			                						LP : ${EquipLP3} / FP : ${EquipFP3}<br>  
			                						공격력 : ${EquipAtk3} / 방어력 : ${EquipDef3}<br>
			                						민첩 : ${EquipAgil3} / 정신 : ${EquipMagic3}<br>
			                						치명 : ${EquipCri3} / 반격 : ${EquipCounter3}<br>
			                						명중 : ${EquipHit3} / 회피 : ${EquipAvoid3} / 회복 : ${EquipRcv3}
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			            			</tr>
			            			<tr>
			                		<c:choose>
			            				<c:when test="${Equipment1 == null}">
			            					<td style="padding-right: 5px;">몸</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 0px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment1}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
			                						<h1>${EquipName1}</h1> <br> ${EquipLevel1} 랭크 <br> 내구도 : ${EquipDurability1} / ${EquipMaxDurability1}
			                						<br>
			                						<h1>[ 능력치 ]</h1>
			                						LP : ${EquipLP1} / FP : ${EquipFP1}<br>  
			                						공격력 : ${EquipAtk1} / 방어력 : ${EquipDef1}<br>
			                						민첩 : ${EquipAgil1} / 정신 : ${EquipMagic1}<br>
			                						치명 : ${EquipCri1} / 반격 : ${EquipCounter1}<br>
			                						명중 : ${EquipHit1} / 회피 : ${EquipAvoid1} / 회복 : ${EquipRcv1}
			                						<h1>[ 저항 ]</h1>
			                						독 : ${EquipPoison1} / 수면 : ${EquipSleep1} / 석화 : ${EquipStone1}<br>
			                						취기 : ${EquipDrunk1} / 혼란 : ${EquipConf1} / 망각 : ${EquipAmne1} 
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			                		<c:choose>
			            				<c:when test="${Equipment4 == null}">
			            					<td style="padding-right: 14px;">발</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-right: 10px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment4}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
			                						<h1>${EquipName4}</h1> <br> ${EquipLevel4} 랭크 <br> 내구도 : ${EquipDurability4} / ${EquipMaxDurability4}
			                						<br>
			                						<h1>[ 능력치 ]</h1>
			                						LP : ${EquipLP4} / FP : ${EquipFP4}<br>  
			                						공격력 : ${EquipAtk4} / 방어력 : ${EquipDef4}<br>
			                						민첩 : ${EquipAgil4} / 정신 : ${EquipMagic4}<br>
			                						치명 : ${EquipCri4} / 반격 : ${EquipCounter4}<br>
			                						명중 : ${EquipHit4} / 회피 : ${EquipAvoid4} / 회복 : ${EquipRcv4}
			                						<h1>[ 저항 ]</h1>
			                						독 : ${EquipPoison4} / 수면 : ${EquipSleep4} / 석화 : ${EquipStone4}<br>
			                						취기 : ${EquipDrunk4} / 혼란 : ${EquipConf4} / 망각 : ${EquipAmne4}
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			            			</tr>
			            			<tr>
			            				<td class="none"></td>
			            				<td class="none"></td>
			                		<c:choose>
			            				<c:when test="${Equipment7 == null}">
			            					<td style="padding-right: 0px;">속성</td>
			            				</c:when>
			            				<c:otherwise>
			                				<td style="padding-top: 0px;" class="tooltipForItem">
			                					<img src="/imgs/ItemNo/${Equipment7}.gif?ref_type=heads">
			                					<span class="tooltipForItemtext">
			                						<h1>${EquipName7}</h1> <br> ${EquipLevel7} 랭크 <br> 내구도 : ${EquipDurability7} / ${EquipMaxDurability7}
			                						<br>
			                						<h1>[ 능력치 ]</h1>
			                						LP : ${EquipLP7} / FP : ${EquipFP7}<br>  
			                						공격력 : ${EquipAtk7} / 방어력 : ${EquipDef7}<br>
			                						민첩 : ${EquipAgil7} / 정신 : ${EquipMagic7}<br>
			                						치명 : ${EquipCri7} / 반격 : ${EquipCounter7}<br>
			                						명중 : ${EquipHit7} / 회피 : ${EquipAvoid7} / 회복 : ${EquipRcv7}
			                						<h1>[ 저항 ]</h1>
			                						독 : ${EquipPoison7} / 수면 : ${EquipSleep7} / 석화 : ${EquipStone7}<br>
			                						취기 : ${EquipDrunk7} / 혼란 : ${EquipConf7} / 망각 : ${EquipAmne7} 
			                					</span>
			                				</td>
			            				</c:otherwise>
			            			</c:choose>
			            			</tr>
			        			</tbody>
			    			</table>
			    			
			            	<table class="acView2">
			        			<thead>
			            			<tr>
			                			<th colspan="6" >캐릭터 상태</th>
			            			</tr>
			        			</thead>
			        			<tbody>
			            			<tr>
			                			<th>레벨</th>
			                			<td>${CharacterItemInfo[0].lv}</td>
			                			<th>HP</th>
			                			<td>${CharacterItemInfo[0].hp}</td>
			                			<th>랭크업</th>
			                			<td>${CharacterItemInfo[0].jobRank}차</td>
			            			</tr>
			            			<tr>
			                			<th>직업</th>
			                			<td>${CharacterItemInfo[0].mainJob}</td>
			                			<th>MP</th>
			                			<td>${CharacterItemInfo[0].forcePoint}</td>
			                			<th>로그인 횟수</th>
			                			<td>${CharacterItemInfo[0].loginCount}회</td>
			            			</tr>
			            			<tr>
			                			<th>보유금액(G)</th>
			                			<td>${FormattedGold}</td>
			                			<th>작업 시간</th>
			                			<td>${FormattedFeverHaveTime}</td>
			                			<th>환 생</th>
			                			<c:choose>
			                				<c:when test="${CharacterItemInfo[0].nameColor == '4'}">
			                					<td>1환</td>
			                				</c:when>
			                				<c:when test="${CharacterItemInfo[0].nameColor == '5'}">
			                					<td>2환</td>
			                				</c:when>
			                				<c:when test="${CharacterItemInfo[0].nameColor == '3'}">
			                					<td>3환</td>
			                				</c:when>
			                				<c:when test="${CharacterItemInfo[0].nameColor == '6'}">
			                					<td>4환</td>
			                				</c:when>
			                				<c:when test="${CharacterItemInfo[0].nameColor == '130'}">
			                					<td>5환</td>
			                				</c:when>
			                				<c:otherwise>
			                					<td>0환</td>
			                				</c:otherwise>
			                			</c:choose>
			            			</tr>
			        			</tbody>
			    			</table>
			    		</div>
						<c:if test="${regNo == 15}">
							<div class="pageBtn left-btn">
								<button title="좌측 캐릭터" id='leftBtn' value='14'>&lt;</button>
							</div>
						</c:if>
						<c:if test="${regNo == 14}">
							<div class="pageBtn right-btn">
								<button title="우측 캐릭터" id='rightBtn' value='15'>&gt;</button>
							</div>
						</c:if>
						<hr class="hr-10">
			            <div  style="display: flex; justify-content: space-around;">
			            <section class="featured">
				            <div style="font-weight : bold;">인벤토리</div>
				        </section>
				        <section class="featured">
				            <div style="font-weight : bold;">창고</div>
				        </section>
				        </div>
			            <div class="itemsView">
				            <div class="itemView1">
								<table class="itemTable1">
									<tbody>
										<c:set var="itemDisplayed" value="false" />
									    <c:set var="itemCount" value="0" />
									    <c:forEach var="item" items="${CharacterItemInfo}">
										    <c:if test="${item.itemSaveIspoolItem eq '0' && item.itemDataPlaceNumber > 7}">
										        <c:set var="itemDisplayed" value="true" />
										        <c:if test="${itemCount % 5 eq 0}">
										            <tr>
										        </c:if>
										        <td class="tooltipForItem">
										            <img class="itemImage" src="/imgs/ItemNo/${item.itemBaseImageNumber}.gif?ref_type=heads">
										            <span class="tooltipForItemtext">
										            	<h1>${item.itemTrueName}</h1>
											            <br>${item.itemLevel} 랭크
											            <br>수량 : ${item.itemRemain} 개
											            	<c:if test="${item.itemDurability > '0'}">
											            	 	<br>
											            	 	내구도 : ${item.itemDurability} / ${item.itemMaxDurability}
											            	</c:if>
												            <c:if test="${item.itemModifyAttack != '0' and not empty item.itemModifyAttack}">
												            	<br>
												            	공격력 : ${item.itemModifyAttack}
												            </c:if>
												            <c:if test="${item.itemModifyDefence != '0' and not empty item.itemModifyDefence}">
												            	<br>
												            	방어력 : ${item.itemModifyDefence}
												            </c:if>
												            <c:if test="${item.itemModifyAgility != '0' and not empty item.itemModifyAgility}">
												            	<br>
												            	민첩 : ${item.itemModifyAgility}
												            </c:if>
												            <c:if test="${item.itemModifyMagic != '0' and not empty item.itemModifyMagic}">
												            	<br>
												            	정신 : ${item.itemModifyMagic}
												            </c:if>
												            <c:if test="${item.itemModifyHp != '0' and not empty item.itemModifyHp}">
												            	<br>
												            	HP : ${item.itemModifyHp}
												            </c:if>
												            <c:if test="${item.itemModifyForcePoint != '0' and not empty item.itemModifyForcePoint}">
												            	<br>
												            	FP : ${item.itemModifyForcePoint}
												            </c:if>
												            <c:if test="${item.itemModifyCritical != '0' and not empty item.itemModifyCritical}">
												            	<br>
												            	치명 : ${item.itemModifyCritical}
												            </c:if>
												            <c:if test="${item.itemModifyAvoid != '0' and not empty item.itemModifyAvoid}">
												            	<br>
												            	회피 : ${item.itemModifyAvoid}
												            </c:if>
												            <c:if test="${item.itemModifyHitRate != '0' and not empty item.itemModifyHitRate}">
												            	<br>
												            	명중 : ${item.itemModifyHitRate}
												            </c:if>
												            <c:if test="${item.itemModifyCounter != '0' and not empty item.itemModifyCounter}">
												            	<br>
												            	반격 : ${item.itemModifyCounter}
												            </c:if>
												            <c:if test="${item.itemModifyRecovery != '0' and not empty item.itemModifyRecovery}">
												            	<br>
												            	회복 : ${item.itemModifyRecovery}
												            </c:if>
												            <c:if test="${item.poison != '0' and not empty item.poison}">
												            	<br>
												            	독 저항 : ${item.poison}
												            </c:if>
												            <c:if test="${item.sleep != '0' and not empty item.sleep}">
												            	<br>
												            	수면 저항 : ${item.sleep}
												            </c:if>
												            <c:if test="${item.confusion != '0' and not empty item.confusion}">
												            	<br>
												            	수면 저항 : ${item.confusion}
												            </c:if>
												            <c:if test="${item.stone != '0' and not empty item.stone}">
												            	<br>
												            	석화 저항 : ${item.stone}
												            </c:if>
												            <c:if test="${item.drunk != '0' and not empty item.drunk}">
												            	<br>
												            	취기 저항 : ${item.drunk}
												            </c:if>
												            <c:if test="${item.amnesia != '0' and not empty item.amnesia}">
												            	<br>
												            	망각 저항 : ${item.amnesia}
												            </c:if>
										            </span>
										        </td>
										        <c:set var="itemCount" value="${itemCount + 1}" />
										    </c:if>
										</c:forEach>
									</tbody>
								</table>
								<c:if test="${itemDisplayed eq 'false'}">
								    <h1>소지하고 있는 아이템이 없습니다.</h1>
								</c:if>
				            </div>
				            <div class="itemView2">
					           	<table class="itemTable2">
					           		<tbody>
					           			<c:set var="itemDisplayed" value="false" />
									    <c:set var="itemCount" value="0" />
									    <c:forEach var="item" items="${CharacterItemInfo}">
									        <c:if test="${item.itemSaveIspoolItem eq '1'}">
									        	<c:set var="itemDisplayed" value="true" />
									            <c:if test="${itemCount % 5 eq 0}">
									                <tr>
									            </c:if>
									            <td class="tooltipForItem">
										            <img class="itemImage" src="/imgs/ItemNo/${item.itemBaseImageNumber}.gif?ref_type=heads">
										            <span class="tooltipForItemtext">
										            <h1>${item.itemTrueName}</h1>
										            <br>${item.itemLevel} 랭크
										            <br>수량 : ${item.itemRemain} 개
										            	<c:if test="${item.itemDurability > '0'}">
										            	 	<br>
										            	 	내구도 : ${item.itemDurability} / ${item.itemMaxDurability}
										            	</c:if>
										            <c:if test="${item.itemModifyAttack != '0' and not empty item.itemModifyAttack}">
										            	<br>
										            	공격력 : ${item.itemModifyAttack}
										            </c:if>
										            <c:if test="${item.itemModifyDefence != '0' and not empty item.itemModifyDefence}">
										            	<br>
										            	방어력 : ${item.itemModifyDefence}
										            </c:if>
										            <c:if test="${item.itemModifyAgility != '0' and not empty item.itemModifyAgility}">
										            	<br>
										            	민첩 : ${item.itemModifyAgility}
										            </c:if>
										            <c:if test="${item.itemModifyMagic != '0' and not empty item.itemModifyMagic}">
										            	<br>
										            	정신 : ${item.itemModifyMagic}
										            </c:if>
										            <c:if test="${item.itemModifyHp != '0' and not empty item.itemModifyHp}">
										            	<br>
										            	HP : ${item.itemModifyHp}
										            </c:if>
										            <c:if test="${item.itemModifyForcePoint != '0' and not empty item.itemModifyForcePoint}">
										            	<br>
										            	FP : ${item.itemModifyForcePoint}
										            </c:if>
										            <c:if test="${item.itemModifyCritical != '0' and not empty item.itemModifyCritical}">
										            	<br>
										            	치명 : ${item.itemModifyCritical}
										            </c:if>
										            <c:if test="${item.itemModifyAvoid != '0' and not empty item.itemModifyAvoid}">
										            	<br>
										            	회피 : ${item.itemModifyAvoid}
										            </c:if>
										            <c:if test="${item.itemModifyHitRate != '0' and not empty item.itemModifyHitRate}">
										            	<br>
										            	명중 : ${item.itemModifyHitRate}
										            </c:if>
										            <c:if test="${item.itemModifyCounter != '0' and not empty item.itemModifyCounter}">
										            	<br>
										            	반격 : ${item.itemModifyCounter}
										            </c:if>
										            <c:if test="${item.itemModifyRecovery != '0' and not empty item.itemModifyRecovery}">
										            	<br>
										            	회복 : ${item.itemModifyRecovery}
										            </c:if>
										            <c:if test="${item.poison != '0' and not empty item.poison}">
												    	<br>
												    	독 저항 : ${item.poison}
												    </c:if>
												    <c:if test="${item.sleep != '0' and not empty item.sleep}">
												    	<br>
												    	수면 저항 : ${item.sleep}
												    </c:if>
												    <c:if test="${item.confusion != '0' and not empty item.confusion}">
												    	<br>
												    	수면 저항 : ${item.confusion}
												    </c:if>
												    <c:if test="${item.stone != '0' and not empty item.stone}">
												    	<br>
												    	석화 저항 : ${item.stone}
												    </c:if>
												    <c:if test="${item.drunk != '0' and not empty item.drunk}">
												    	<br>
												    	취기 저항 : ${item.drunk}
												    </c:if>
												    <c:if test="${item.amnesia != '0' and not empty item.amnesia}">
												    	<br>
												    	망각 저항 : ${item.amnesia}
												    </c:if>
										            </span>
										        </td>
									            <c:set var="itemCount" value="${itemCount + 1}" />
									        </c:if>
									    </c:forEach>
									</tbody>
					           	</table>
					           	<c:if test="${itemDisplayed eq 'false'}">
								    <h1>소지하고 있는 아이템이 없습니다.</h1>
								</c:if>
				            </div>
						</div>
						
						<hr class="hr-10">
						
						<div class="petsView">
				            <div class="petView1">
					           	<table class="petTable1">
					           		<tbody>
					           			<c:forEach var="pet" items="${CharacterPetInfo}">
					           				<c:if test="${not empty pet and pet.unk98 == '0'}">
							           			<tr>
							           				<td class="tooltipForPet" rowspan="4" style="width : 25%;">
							           					<img class="petImage" src="/imgs/ItemImages/${pet.baseImgNum}.gif?ref_type=heads">
							           					<span class="tooltipForPettext">
							           						<h1>[종족]</h1>
							           						${pet.race}
							           						<br>
							           						<h1>[펫 상태]</h1>
							           						HP : ${pet.hp} &nbsp;/&nbsp; FP : ${pet.mp} 
							           						<br>
							           						<h1>[특수 능력치]</h1>
							           						치명 : ${pet.petCri} &nbsp;/&nbsp; 반격 : ${pet.petCounter}<br>
							           						명중 : ${pet.petHit} &nbsp;/&nbsp; 회피 : ${pet.petAvoid}
							           						<br>
							           						<h1>[저항 능력치]</h1>
							           						독 : ${pet.poison} &nbsp;/&nbsp; 석화 : ${pet.stone} &nbsp;/&nbsp; 혼란 : ${pet.confusion}<br>
							           						수면 : ${pet.sleep} &nbsp;/&nbsp; 취기 : ${pet.drunk} &nbsp;/&nbsp; 망각 : ${pet.amnesia}
							           						<br>
							           						<h1>[성장 포인트 분배 상태]</h1>
							           						체력 : ${pet.petVital}
							           						<br>
							           						근력 : ${pet.petStr}
							           						<br>
							           						방어력 : ${pet.petTgh}
							           						<br>
							           						순발력 : ${pet.petQuick}
							           						<br>
							           						정신력 : ${pet.petMagic}
							           						<h1>[속성 상태]</h1>
									                        <div class="bar-container">
															    지 :&nbsp;
															    <c:forEach var="i" begin="1" end="${pet.earth.intValue()}">
															        <div class="bar earth"></div>
															    </c:forEach>
															    <c:if test="${pet.earth.intValue() < pet.earth}">
															        <div class="bar" style="width: ${pet.earth % 1 * 20}px;"></div>
															    </c:if>
															    <c:if test="${pet.earth > 0}">
															    	<span>[${pet.earth}]</span>
															    </c:if>
															</div>
									                        <div class="bar-container">
									                            수 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.water.intValue()}">
									                                <div class="bar water"></div>
									                            </c:forEach>
									                            <c:if test="${pet.water.intValue() < pet.water}">
									                                <div class="bar" style="width: ${pet.water % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.water > 0}">
															        <span>[${pet.water}]</span>
															    </c:if>
									                        </div>
									                        <div class="bar-container">
									                            화 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.fire.intValue()}">
									                                <div class="bar fire"></div>
									                            </c:forEach>
									                            <c:if test="${pet.fire.intValue() < pet.fire}">
									                                <div class="bar" style="width: ${pet.fire % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.fire > 0}">
															        <span>[${pet.fire}]</span>
															    </c:if>
									                        </div>
									                        <div class="bar-container">
									                            풍 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.wind.intValue()}">
									                                <div class="bar wind"></div>
									                            </c:forEach>
									                            <c:if test="${pet.wind.intValue() < pet.wind}">
									                                <div class="bar" style="width: ${pet.wind % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.wind > 0}">
															        <span>[${pet.wind}]</span>
															    </c:if>
									                        </div>
							           					</span>
							           				</td>
							           			</tr>
							           			<tr>
							           				<td>이름 : <span>${pet.petName}</span></td>
							           			</tr>
							           			<tr>
							           				<td>닉네임 : <span>${pet.userPetName}</span></td>
							           			</tr>
							           			<tr>
							           				<td>레벨 : <span>${pet.level}</span></td>
							           			</tr>
					           				</c:if>
					           			</c:forEach>
					           			<c:if test="${empty CharacterPetInfo}">
					           				<h1>소지한 펫이 없습니다.</h1>		
					           			</c:if>
					           		</tbody>
					           	</table>
				            </div>
				            <div class="petView2">
					           	<table class="petTable2">
					           		<tbody>
					           			<c:forEach var="pet" items="${CharacterPetInfo}">
					           				<c:if test="${pet.unk98 == '1'}">
							           			<tr>
							           				<td class="tooltipForPet" rowspan="4" style="width : 25%;">
							           					<img class="petImage" src="/imgs/ItemImages/${pet.baseImgNum}.gif?ref_type=heads">
							           					<span class="tooltipForPettext">
							           						<h1>[종족]</h1>
							           						${pet.race}
							           						<h1>[펫 상태]</h1>
							           						HP : ${pet.hp} &nbsp;/&nbsp; FP : ${pet.mp} 
							           						<br>
							           						<h1>[특수 능력치]</h1>
							           						치명 : ${pet.petCri} &nbsp;/&nbsp; 반격 : ${pet.petCounter}<br>
							           						명중 : ${pet.petHit} &nbsp;/&nbsp; 회피 : ${pet.petAvoid}
							           						<br>
							           						<h1>[저항 능력치]</h1>
							           						독 : ${pet.poison} &nbsp;/&nbsp; 석화 : ${pet.stone} &nbsp;/&nbsp; 혼란 : ${pet.confusion}<br>
							           						수면 : ${pet.sleep} &nbsp;/&nbsp; 취기 : ${pet.drunk} &nbsp;/&nbsp; 망각 : ${pet.amnesia}
							           						<br>
							           						<h1>[성장 포인트 분배 상태]</h1>
							           						체력 : ${pet.petVital}
							           						<br>
							           						근력 : ${pet.petStr}
							           						<br>
							           						방어력 : ${pet.petTgh}
							           						<br>
							           						순발력 : ${pet.petQuick}
							           						<br>
							           						정신력 : ${pet.petMagic}
							           						<br>
							           						<h1>[속성 상태]</h1>
									                        <div class="bar-container">
									                            지 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.earth.intValue()}">
									                                <div class="bar earth"></div>
									                            </c:forEach>
									                            <c:if test="${pet.earth.intValue() < pet.earth}">
									                                <div class="bar" style="width: ${pet.earth % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.earth > 0}">
															        <span>[${pet.earth}]</span>
															    </c:if>
									                        </div>
									                        <div class="bar-container">
									                            수 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.water.intValue()}">
									                                <div class="bar water"></div>
									                            </c:forEach>
									                            <c:if test="${pet.water.intValue() < pet.water}">
									                                <div class="bar" style="width: ${pet.water % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.water > 0}">
															        <span>[${pet.water}]</span>
															    </c:if>
									                        </div>
									                        <div class="bar-container">
									                            화 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.fire.intValue()}">
									                                <div class="bar fire"></div>
									                            </c:forEach>
									                            <c:if test="${pet.fire.intValue() < pet.fire}">
									                                <div class="bar" style="width: ${pet.fire % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.fire > 0}">
															        <span>[${pet.fire}]</span>
															    </c:if>
									                        </div>
									                        <div class="bar-container">
									                            풍 :&nbsp;
									                            <c:forEach var="i" begin="1" end="${pet.wind.intValue()}">
									                                <div class="bar wind"></div>
									                            </c:forEach>
									                            <c:if test="${pet.wind.intValue() < pet.wind}">
									                                <div class="bar" style="width: ${pet.wind % 1 * 20}px;"></div>
									                            </c:if>
									                            <c:if test="${pet.wind > 0}">
															        <span>[${pet.wind}]</span>
															    </c:if>
									                        </div>
							           					</span>
							           				</td>
							           			</tr>
							           			<tr>
							           				<td>이름 : <span>${pet.petName}</span></td>
							           			</tr>
							           			<tr>
							           				<td>닉네임 : <span>${pet.userPetName}</span></td>
							           			</tr>
							           			<tr>
							           				<td>레벨 : <span>${pet.level}</span></td>
							           			</tr>
					           				</c:if>
					           			</c:forEach>
					           			<c:if test="${empty CharacterPetInfo}">
					           				<h1>소지한 펫이 없습니다.</h1>		
					           			</c:if>
					           		</tbody>
					           	</table>
				            </div>
						</div>
		            </section>
					</c:otherwise>
				</c:choose>
			</section>
		</div>
        
    <!-- Footer -->
       <%@ include file="../../Section/User/Footer.jsp"%>
    </main>
    </form>
</body>
</html>