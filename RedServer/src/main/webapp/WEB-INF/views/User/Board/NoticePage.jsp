<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/BoardWrite.css?ver=20241110" />
</head>
<script>
var boardCode;

$(document).ready(function() {
	
	boardCode = "${boardCode}";
	
	submitBtnClick();
	
	CKEDITOR.replace('content', {
	       removePlugins: 'sourcearea,maximize,elementspath',
	       filebrowserUploadUrl: '/Board/ImageUpload.do',
	       extraPlugins: 'youtube',
	       youtube_width: '640',       // YouTube 동영상 너비 설정
	       youtube_height: '480',      // YouTube 동영상 높이 설정
	       youtube_responsive: true,   // 반응형 설정
	       filebrowserVideoUploadUrl: '/Board/VideoUpload.do',
	       enterMode: CKEDITOR.ENTER_BR,
	       shiftEnterMode: CKEDITOR.ENTER_P,
	       toolbar: [
	           { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'Undo', 'Redo'] }, // 클립보드 옵션
	           { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'] }, // 기본 스타일 옵션
	           { name: 'styles', items: ['Font', 'FontSize'] }, // 폰트 및 폰트 크기
	           { name: 'colors', items: ['TextColor', 'BGColor'] }, // 글자색과 배경색
	           { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] }, // 리스트 및 정렬 옵션
	           { name: 'insert', items: ['Image', 'Table', 'Youtube', 'SpecialChar','Link', 'Unlink'] }, // 이미지, 테이블, 유튜브, 특수문자 버튼
	           { name: 'tools', items: ['Maximize'] }, // 확대 버튼 추가
	           { name: 'editing', items: ['Scayt'] } // 맞춤법 검사
	       ],
	       font_names: 'Nanum Gothic/Nanum Gothic; Nanum Myeongjo/Nanum Myeongjo; Nanum Pen Script/Nanum Pen Script; \
	                   Noto Sans KR/Noto Sans KR; Noto Serif KR/Noto Serif KR; Malgun Gothic/Malgun Gothic; \
	                   Batang/Batang; Dotum/Dotum; Gothic/Gulim; Arial/Arial; Times New Roman/Times New Roman; Verdana/Verdana',
	       fontSize_sizes: '8/8px;10/10px;12/12px;14/14px;16/16px;18/18px;20/20px;24/24px;28/28px;32/32px;36/36px',
	       pasteFromWordRemoveFontStyles: true,
	       pasteFromWordRemoveStyles: true,
	       allowedContent: 'p; a[!href]; img[!src,alt,width,height]; br; strong; em; u; li; ul; ol; span{*}; font{*}; div{*}; iframe[!src,width,height];',
	       extraAllowedContent: 'a[!href]; img[!src,alt,width,height]; span{color,background-color,font-size,font-family}; font{color,background-color,size}; h1 h2 h3 h4 h5 h6; iframe[!src,width,height];',
	       disallowedContent: 'script; *[on*]' // 보안상 위험한 요소만 제거
	   });

	var escapedContent = $('#escapedContent').val();

    
    CKEDITOR.instances.content.setData(escapedContent);
    
    if (boardCode == 6) {
        updateTitlePrefix();

        $('#typeSelect').change(function() {
            updateTitlePrefix();
        });
    }
    
});

function updateTitlePrefix() {
    var selectedValue = $('#typeSelect').val();  
    var titleInput = $('#title');
    
    var currentValue = titleInput.val().replace(/^\[(구매|판매)\]\s*/, '');
    
    var prefix = selectedValue === 'buy' ? '[구매]' : '[판매]';
    titleInput.val(prefix + ' ' + currentValue);
}


function submitBtnClick(){
	
	$('#submitBtn').click(function (){
		
		var content = CKEDITOR.instances.content.getData(); 
		var title = $('#title').val();
        var boardNo = $('#boardNo').val();
        var sendParam = "";
        var tradeType = $('#typeSelect').val();
        
		var param = {
			content : content,
        	title : title,
        	boardCode : boardCode,
        	tradeType : tradeType
        };
		
		if(boardNo) {
			boardNo = parseInt(boardNo);
	    	param.boardNo = boardNo;
	    }		
		
		var mappingUrl = "/Board/BoardRegistProc.do";
		sendParam = "boardCode=" + boardCode;
        
		if(confirm("등록하시겠습니까?")){
			var returnAjax = doAjax(param, mappingUrl);
	        
			returnAjax = decodeURI(returnAjax)
			
			if (returnAjax.indexOf('오류') == 0
					|| returnAjax.indexOf('입력') == 0) {
				alert(returnAjax);
				return;
			} else {
				alert(returnAjax);
				doSubmit("/Board/GoBoardProc.do?" + sendParam);
			} 
		}
	}); 
}


function doSubmit(str) {
	var form = $("#doBoard");

	form.attr("action", str);
	form.attr("method", "POST");
	form.attr("enctype", "multipart/form-data");

	form.submit();
}


function doAjax(param, mappingUrl) {
	var returnVal;
	$.ajax({
		type : "POST",
		async: false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	return returnVal;
}



</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	    <main>
	    	<input type="hidden" id="category" name="category" value="${boardName}"/>
	    	<input type="hidden" id="boardNo" name="boardNo" value="${boardDetail.boardNo}"/>
	    	<input type="hidden" id="escapedContent" value="<c:out value='${boardDetail.content}'/>" />
	        <div class="container">
	            <section class="categoriesTable" style="margin-top: 1.5%;">
	            	<p>※레드서버
	            		<c:choose>
	            			<c:when test="${boardCode == 1}"> 공지사항※</c:when>
	            			<c:when test="${boardCode == 2}"> 패치노트※</c:when>
	            			<c:when test="${boardCode == 3}"> 기능※</c:when>
	            			<c:when test="${boardCode == 4}"> 가이드※</c:when>
	            			<c:when test="${boardCode == 5}"> 이벤트※</c:when>
	            			<c:when test="${boardCode == 6}"> 경매장※</c:when>
	            			<c:when test="${boardCode == 7}"> 거래게시판※</c:when>
	            			<c:when test="${boardCode == 8}"> 자유게시판※</c:when>
	            			<c:when test="${boardCode == 9}"> 정보게시판※</c:when>
	            			<c:when test="${boardCode == 10}"> 공략게시판※</c:when>
	            			<c:when test="${boardCode == 11}"> 퀘스트게시판※</c:when>
	            			<c:when test="${boardCode == 12}"> 건의게시판※</c:when>
	            			<c:otherwise> QnA게시판※</c:otherwise>
	            		</c:choose> 
	            	</p>
	            </section>
				<section class="contents" id="contents">
				    <form id="doBoard" method="POST">
				        <div class="form-group">
				        	<c:if test="${boardCode == 7}">
				        		<select id="typeSelect" class="select" onchange="updateTitlePrefix()">
				        			<option value="buy" selected="selected">구매</option>
				        			<option value="sell">판매</option>
				        		</select>
				        	</c:if>
				            <label for="title">제목:</label>
				            <c:choose>
    							<c:when test="${not empty boardDetail}">
        							<input type="text" class="title" id="title" name="title" value="${boardDetail.title}">
    							</c:when>
    							<c:otherwise>
				            		<input type="text" class="title" id="title" name="title">
    							</c:otherwise>
							</c:choose>
				        </div>
				        <div class="form-group" id="content">
				        	<c:choose>
    							<c:when test="${not empty boardDetail}">
    								<textarea id="content" name="content" rows="4"><c:out value="${boardDetail.content}" /></textarea>
    							</c:when>
    							<c:otherwise>
    								<textarea id="content" name="content" rows="4"></textarea>
    							</c:otherwise>
							</c:choose>
				        </div>
				        <input type="button" class="button btnFade btnBlueGreen" id="submitBtn" value="등록" />
				    </form>
				</section>
	        </div>
	        
	    <!-- Footer -->
		<%@ include file="../../Section/User/Footer.jsp"%>
    </main>
    <!-- CKEditor 적용 -->
</body>
</html>
