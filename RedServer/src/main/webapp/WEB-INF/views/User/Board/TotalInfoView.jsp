<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/CharacterView.css?ver=20250207" />
</head>
<script>
$(document).ready(function() {
	searchItemBtnClick();
	searchPetBtnClick();
    itemTotalView();
    petTotalView();
    acInfoDetailView();
    currencyTotalView();
});

function searchItemBtnClick() {
    $('#searchItemBtn').click(function () {
        handleSearch();
    });

    $('#searchItemName').on('keypress', function (e) {
        if (e.key === 'Enter' || e.which === 13) { 
            e.preventDefault(); 
            handleSearch();
        }
    });

    function handleSearch() {
        var searchItemName = $('#searchItemName').val().trim(); 

        if (searchItemName === '') {
            alert('검색어를 입력하세요.');
            return;
        }
        doSubmit("/Login/TotalItemViewProc.do?sin=" + encodeURIComponent(searchItemName));
    }
}

function searchPetBtnClick() {
    $('#searchPetBtn').click(function () {
        handleSearch();
    });

    $('#searchPetName').on('keypress', function (e) {
        if (e.key === 'Enter' || e.which === 13) { 
            e.preventDefault(); 
            handleSearch();
        }
    });

    function handleSearch() {
        var searchPetName = $('#searchPetName').val().trim(); 

        if (searchPetName === '') {
            alert('검색어를 입력하세요.');
            return;
        }
        doSubmit("/Login/TotalPetViewProc.do?sin=" + encodeURIComponent(searchPetName));
    }
}





function itemTotalView() {
	$('#itemTotalView').click(function(){
		doSubmit("/Login/TotalItemViewProc.do");	
	});
}

function petTotalView() {
	$('#petTotalView').click(function(){
		doSubmit("/Login/TotalPetViewProc.do");	
	});
}

function currencyTotalView(){
	$('#currencyTotalView').click(function(){
		doSubmit("/Login/TotalCurrencyViewProc.do");	
	});
}

function acInfoDetailView() {
	$('.acSearchBtns').click(function(){
		var acId = $(this).val();
		$('#accountId').val(acId); 
    	doSubmit("/Login/UserAcProc.do");
	})
}


function doSubmit(str) {
	var form = $("#characterTotalView");

	form.attr("action", str);
	form.attr("method", "POST");

	form.submit();
}

function sortTable(columnIndex) {
	const clickedTh = event.target.closest("th");
    const table = clickedTh.closest("table");
    const tbody = table.querySelector("tbody");
    const rows = Array.from(tbody.querySelectorAll("tr"));
    const headers = Array.from(table.querySelectorAll(".sortable"));

    headers.forEach(header => header.classList.remove("asc", "desc"));

    let sortedRows;
    let header = headers[columnIndex];

    if (header.dataset.order === "asc") {
        sortedRows = rows.sort((a, b) => compareCells(b, a, columnIndex + 1)); 
        header.dataset.order = "desc";
        header.classList.add("desc");
    } else {
        sortedRows = rows.sort((a, b) => compareCells(a, b, columnIndex + 1)); 
        header.dataset.order = "asc";
        header.classList.add("asc");
    }

    tbody.innerHTML = "";
    sortedRows.forEach(row => tbody.appendChild(row));
}

function compareCells(rowA, rowB, index) {
    const cellA = rowA.cells[index].innerText.trim().replace(/,/g, "");
    const cellB = rowB.cells[index].innerText.trim().replace(/,/g, "");

    const numA = parseFloat(cellA);
    const numB = parseFloat(cellB);

    return isNaN(numA) || isNaN(numB) ? cellA.localeCompare(cellB) : numA - numB;
}




</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	<form id="characterTotalView" method="post">
    <main>
    <input type="hidden" id="accountId" name="accountId" />
        <div class="container">
			<section class="featured-container">
				<section class="featured">
					<input type="button" id="itemTotalView"  class= "searchBtn" value="아이템 종합"/>
					<input type="button" id="petTotalView" class= "searchBtn" value="수마 종합"/>
					<input type="button" id="currencyTotalView" class= "searchBtn" value="화폐 종합"/>
				</section>
				<c:if test="${empty totalItemList and not empty emptyItemList}">
				    <section class="featured">
				    	<input type="text" id="searchItemName" name="searchItemName" placeholder="아이템 이름을 입력하세요." class="search-input">
						<input type="button" id="searchItemBtn" class="search-btn" value="검색" />
				        <table style="width : 100%;">
						    <colgroup>
						        <col style="width: 5%;">
						        <col style="width: 18%;">
						        <col style="width: 10%;">
						        <col style="width: 12%;">
						        <col style="width: 15%;">
						        <col style="width: 10%;">
						        <col style="width: 15%;">
						        <col style="width: 15%;">
						    </colgroup>
						    <thead>
						        <tr class="totalItemsView">
						            <th>이미지</th>
								    <th class="sortable" onclick="sortTable(0)">아이템 이름</th>
								    <th class="sortable" onclick="sortTable(1)">수량</th>
								    <th class="sortable" onclick="sortTable(2)">내구도<br>(현재/최대)</th>
								    <th class="sortable" onclick="sortTable(3)">생성 시간</th>
								    <th class="sortable" onclick="sortTable(4)">위치</th>
								    <th class="sortable" onclick="sortTable(5)">캐릭명</th>
								    <th>소유 계정</th>
						        </tr>
						    </thead>
						    <tbody>
							    <c:if test="${not empty emptyItemList}">
							        <tr>
							            <td colspan="8" style="text-align: center; font-weight: bold;">
							                <strong>${emptyItemList}</strong>
							            </td>
							        </tr>
							    </c:if>
							</tbody>
						</table>
				    </section>
				</c:if>
				<c:if test="${empty totalCurrencyList and empty totalGoldList and not empty emptyGoldList}">
				    <section class="featured">
					    <table style="width : 100%;">
					        <colgroup>
								<col style="width: 10%;">
								<col style="width: 15%;">
								<col style="width: 10%;">
								<col style="width: 18%;">
								<col style="width: 15%;">
								<col style="width: 12%;">
								<col style="width: 20%;">
							</colgroup>
					        <thead>
					            <tr class="totalItemsView">
					                <th>이미지</th>
					                <th class="sortable" onclick="sortTable(0)">캐릭명</th>
					                <th class="sortable" onclick="sortTable(1)">레벨</th>
					                <th class="sortable" onclick="sortTable(2)">인벤토리 골드(G)</th>
					                <th class="sortable" onclick="sortTable(3)">창고 골드(G)</th>
					                <th class="sortable" onclick="sortTable(4)">레드코인</th>
					                <th>소유 계정</th>
					            </tr>
					        </thead>
					        <tbody>
					            <c:if test="${!empty emptyGoldList}">
								    <tr>
								        <td colspan="7" style="text-align: center;"><strong>${emptyGoldList}</strong></td>
								    </tr>
								</c:if>
					        </tbody>
					    </table>
					</section>
				</c:if>
				<c:if test="${empty totalPetList and not empty emptyPetList}">
				    <section class="featuredTotPetView">
				    	<input type="text" id="searchPetName" name="searchPetName" placeholder="수마 이름을 입력하세요." class="search-input">
						<input type="button" id="searchPetBtn" class="search-btn" value="검색" />
			            	<table style="width : 100%;">
					            <colgroup>
									<col style="width: 15%;">
									<col style="width: 15%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 20%;">
								</colgroup>
						        <thead>
						            <tr class="totalPetsView">
						                <th>이미지</th>
						                <th class="sortable" onclick="sortTable(0)">수마 이름</th>
						                <th class="sortable" onclick="sortTable(1)">수마 닉네임</th>
						                <th class="sortable" onclick="sortTable(2)">레벨</th>
						                <th class="sortable" onclick="sortTable(3)">소유 캐릭</th>
						                <th class="sortable" onclick="sortTable(4)">위치</th>
						                <th>소유 계정</th>
						            </tr>
						        </thead>
						        <tbody>
						            <c:if test="${!empty emptyPetList}">
				                		<tr>
				                    		<td colspan="7" style="text-align: center;"><strong>${emptyPetList}</strong></td>
				               			</tr>
				            		</c:if>
						        </tbody>
						    </table>
		            	</section>
		        </c:if>
				<c:choose>
					<c:when test="${not empty totalPetList}">
				        <section class="featuredTotPetView">
				        	<input type="text" id="searchPetName" name="searchPetName" placeholder="수마 이름을 입력하세요." class="search-input">
							<input type="button" id="searchPetBtn" class="search-btn" value="검색" />
			            	<table style="width : 100%;">
					            <colgroup>
									<col style="width: 15%;">
									<col style="width: 15%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 20%;">
								</colgroup>
						        <thead>
						            <tr class="totalPetsView">
						                <th>이미지</th>
						                <th class="sortable" onclick="sortTable(0)">수마 이름</th>
						                <th class="sortable" onclick="sortTable(1)">수마 닉네임</th>
						                <th class="sortable" onclick="sortTable(2)">레벨</th>
						                <th class="sortable" onclick="sortTable(3)">소유 캐릭</th>
						                <th class="sortable" onclick="sortTable(4)">위치</th>
						                <th>소유 계정</th>
						            </tr>
						        </thead>
						        <tbody>
						            <c:forEach items="${totalPetList}" var="totPet">
						                <tr>
						                    <td style="text-align: center;"><img class="petTotImage" src="/imgs/ItemImages/${totPet.baseImgNum}.gif?ref_type=heads"></td>
						                    <td style="text-align: center;">${totPet.petName}</td>
						                    <td style="text-align: center;">${totPet.userPetName}</td>
						                    <td style="text-align: center;">${totPet.level}</td>
						                    <td style="text-align: center;">${totPet.ownerName}</td>
						                    <td style="text-align: center;">
						                    	<c:if test="${totPet.unk98 == 0}">
						                    		인벤토리
											</c:if>
											<c:if test="${totPet.unk98 == 1}">
						                    		창고
											</c:if>						                        		
						                    </td>
						                    <td style="text-align: center;"><input class="acSearchBtns" type="button" value="${totPet.cdKey}" /></td>
						                </tr>
						            </c:forEach>
						            <c:if test="${!empty emptyPetList}">
				                		<tr>
				                    		<td colspan="6" style="text-align: center;"><strong>${emptyPetList}</strong></td>
				               			</tr>
				            		</c:if>
						        </tbody>
						    </table>
		            	</section>
				    </c:when>
					<c:when test="${not empty totalItemList}">
			            <section class="featured">
			            	<input type="text" id="searchItemName" name="searchItemName" placeholder="아이템 이름을 입력하세요." class="search-input">
                			<input type="button" id="searchItemBtn" class="search-btn" value="검색" />
				            <table style="width : 100%;">
							    <colgroup>
							        <col style="width: 5%;">
							        <col style="width: 18%;">
							        <col style="width: 10%;">
							        <col style="width: 12%;">
							        <col style="width: 15%;">
							        <col style="width: 10%;">
							        <col style="width: 15%;">
							        <col style="width: 15%;">
							    </colgroup>
							    <thead>
							        <tr class="totalItemsView">
							            <th>이미지</th>
									    <th class="sortable" onclick="sortTable(0)">아이템 이름</th>
									    <th class="sortable" onclick="sortTable(1)">수량</th>
									    <th class="sortable" onclick="sortTable(2)">내구도<br>(현재/최대)</th>
									    <th class="sortable" onclick="sortTable(3)">생성 시간</th>
									    <th class="sortable" onclick="sortTable(4)">위치</th>
									    <th class="sortable" onclick="sortTable(5)">캐릭명</th>
									    <th>소유 계정</th>
							        </tr>
							    </thead>
							    <tbody>
									<c:forEach items="${totalItemList}" var="totItem">
									    <tr>
									        <td style="text-align: center;"><img class="itemTotImage" src="/imgs/ItemNo/${totItem.itemBaseImageNumber}.gif?ref_type=heads"></td>
									        <td style="text-align: center;">${totItem.itemTrueName}</td>
									        <td style="text-align: center;">${totItem.itemRemain}</td>
									        <td style="text-align: center; color: #b14545;">
									            <c:choose>
									                <c:when test="${totItem.itemDurability < 1 or totItem.itemMaxDurability < 1}">
									                    <span style="color: black;">없음</span>
									                </c:when>
									                <c:otherwise>
									                    ${totItem.itemDurability} / ${totItem.itemMaxDurability}
									                </c:otherwise>
									            </c:choose>
									        </td>
									        <td style="text-align: center;">${totItem.formattedCreateTime}</td>
									        <td style="text-align: center;">
									            <c:if test="${totItem.itemSaveIspoolItem == 0}">
									                인벤토리
									            </c:if>
									            <c:if test="${totItem.itemSaveIspoolItem == 1}">
									                창고
									            </c:if>
									        </td>
									        <td style="text-align: center;">${totItem.name}</td>
									        <td style="text-align: center;"><input class="acSearchBtns" type="button" value="${totItem.cdKey}" /></td>
									    </tr>
									</c:forEach>
								    <c:if test="${!empty emptyItemList}">
								        <tr>
								            <td colspan="8" style="text-align: center; font-weight: bold;">
								                <strong>${emptyItemList}</strong>
								            </td>
								        </tr>
								    </c:if>
								</tbody>
							</table>
			            </section>
					</c:when>
					<c:when test="${not empty totalCurrencyList or not empty totalGoldList}">
						<section class="featured">
				            <table style="width : 100%;">
						        <colgroup>
									<col style="width: 10%;">
									<col style="width: 15%;">
									<col style="width: 10%;">
									<col style="width: 18%;">
									<col style="width: 15%;">
									<col style="width: 12%;">
									<col style="width: 20%;">
								</colgroup>
						        <thead>
						            <tr class="totalItemsView">
						                <th>이미지</th>
						                <th class="sortable" onclick="sortTable(0)">캐릭명</th>
						                <th class="sortable" onclick="sortTable(1)">레벨</th>
						                <th class="sortable" onclick="sortTable(2)">인벤토리 골드(G)</th>
						                <th class="sortable" onclick="sortTable(3)">창고 골드(G)</th>
						                <th class="sortable" onclick="sortTable(4)">레드코인</th>
						                <th>소유 계정</th>
						            </tr>
						        </thead>
						        <tbody>
						            <c:if test="${not empty totalCurrencyList}">
						                <c:forEach items="${totalCurrencyList}" var="totCurrency">
						                    <tr>
						                        <td style="text-align: center;"><img class="petTotImage" src="/imgs/ItemImages/${totCurrency.baseImageNumber}.gif?ref_type=heads"></td>
						                        <td style="text-align: center;">${totCurrency.name}</td>
						                        <td style="text-align: center;">${totCurrency.lv}</td>
						                        <td style="text-align: center;">${totCurrency.formattedGold}</td>
						                        <td style="text-align: center;">${totCurrency.formattedPoolGold}</td>
						                        <td style="text-align: center;">${totCurrency.itemRemain}</td>
						                        <td style="text-align: center;"><input class="acSearchBtns" type="button" value="${totCurrency.cdKey}" /></td>
						                    </tr>
						                </c:forEach>
						            </c:if>
						            <c:if test="${!empty emptyGoldList}">
									    <tr>
									        <td colspan="7" style="text-align: center;"><strong>${emptyGoldList}</strong></td>
									    </tr>
									</c:if>
						        </tbody>
						    </table>
			            </section>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			</section>
		</div>
        
    <!-- Footer -->
       <%@ include file="../../Section/User/Footer.jsp"%>
    </main>
    </form>
</body>
</html>