<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/Board.css?ver=20250207" />
</head>
<script>
var boardCode;
var userRole;
var currentUser;
var backSpace = 0;

$(document).ready(function() {
	
	bidBtn();
    replyBtn();
	writeBoardBtn();
	deleteBoardBtn();
	viewBoardDetail();
	backBoardBtn();
	updateBoardBtn();
    replyDeleteBtn();
    searchBoardBtnClick();
	userRole = "${userInfo.userRole}";
    boardCode = "${boardCode}";
    currentUser = "${userNick}";
    
});

function fetchBoardData() {
    var tradeType = $('#typeSelect').val(); 
     
    var boardCode = $('#boardCode').val();
    $('#pageNum').val("1");
	$("#boardNo").val("1");
    $('#tradeType').val(tradeType);
    doSubmitAction("/Board/GoBoardProc.do");
}


function searchBoardBtnClick() {
    $('#boardSearchBtn').click(function () {
        handleSearch();
    });

    $('#boardSearch').on('keypress', function (e) {
        if (e.key === 'Enter' || e.which === 13) { 
            e.preventDefault(); 
            handleSearch();
        }
    });

    function handleSearch() {
    	
        var boardSearch = $('#boardSearch').val().trim(); 
        var boardCode = $('#boardCode').val();
        console.log(boardSearch);
        console.log(boardCode);
        
        
        $('#boardSearchHidden').val(boardSearch);
        
        doSubmit('/Board/GoBoardProc.do?sin=' + encodeURIComponent(boardSearch) + 
                '&boardCode=' + encodeURIComponent(boardCode))
    }
}


function replyBtn(){	
	$(document).on("click", "#replySubmit", function() {
	    var replyContent = $('#replyContent').val();
	    var register = "${userNick}";
	    var boardNo = $('#replyBoardNo').val();
	   
	    var param = {
	    	replyContent : replyContent,
			register : register,
			boardNo : boardNo,
			boardCode : boardCode
	    };
	    
	    var mappingUrl = "/Board/ReplyRegistProc.do";
	    
	    var returnAjax = doAjax(param, mappingUrl);
	    
	    returnAjax = decodeURI(returnAjax);
	    
	    if (returnAjax.indexOf('오류') == 0
				|| returnAjax.indexOf('실패') == 0) {
			alert(returnAjax);
			return;
		} else {
			viewBoardDetailReply(boardNo);
		}
	    
	});
}

function bidBtn(){	
	$(document).on("click", "#bidSubmit", function() {
	    var bidAmount = $('#replyContent').val();
	    var register = "${userNick}";
	    var boardNo = $('#replyBoardNo').val();
	    var auctionPrice = $("#auctionPrice").html();
	    
	    if (!/^\d{1,8}$/.test(bidAmount)) {
            alert("입찰 금액은 8자리의 숫자까지만 입력 가능합니다.");
            return;
        }
	    
	    if(parseInt(auctionPrice) > parseInt(bidAmount)){
	    	alert("입찰 금액은 경매 시작 가격보다 높거나 같아야 합니다.");
	    	return;
	    }
	   
	    var param = {
	    	bidAmount : bidAmount,
	    	register : register,
			boardNo : boardNo,
			boardCode : boardCode
	    };
	    
	    var mappingUrl = "/Board/ReplyRegistProc.do";
	    
	    var returnAjax = doAjax(param, mappingUrl);
	    
	    returnAjax = decodeURI(returnAjax);
	    
	    if (returnAjax.indexOf('오류') == 0
				|| returnAjax.indexOf('실패') == 0
					|| returnAjax.indexOf('입찰') == 0
						|| returnAjax.indexOf('5분') == 0) {
	    	alert(returnAjax);
	    	return;
		} else {
			alert(returnAjax);
			viewBoardDetailReply(boardNo);
		}
	    
	});
}


function replyDeleteBtn(){
    $(document).on("click", "#replyDelete", function() {
        var replySeq = $(this).data("replyno");
        var register = "${userNick}";
        var boardNo = $('#replyBoardNo').val();

        var param = {
            replySeq: replySeq,
            register: register,
            boardNo : boardNo,
            boardCode : boardCode
        }

        var mappingUrl = "/Board/ReplyDeleteProc.do";

        if(confirm("정말 삭제하시겠습니까?")){
	        var returnAjax = doAjax(param, mappingUrl);
	
	        returnAjax = decodeURI(returnAjax);
	
	        if (returnAjax.indexOf('오류') == 0 || returnAjax.indexOf('실패') == 0) {
	            alert(returnAjax);
	            return;
	        } else {
	            viewBoardDetailReply(boardNo);
	        }
        }
    });
}
 
function deleteBoardBtn(){
	$("#deleteBoard").click(function(){
		
		var boardNo = $(this).data("boardno");
        
        var param = {
        		boardNo : boardNo,
        		boardCode : boardCode
    	    }
        
		var mappingUrl = "/Board/DeleteBoardProc.do";
		if(confirm("정말 삭제하시겠습니까?")){
			var returnAjax = doAjax(param, mappingUrl);		
			
			returnAjax = decodeURI(returnAjax)
			
			if (returnAjax.indexOf('오류') == 0
					|| returnAjax.indexOf('선택') == 0) {
				alert(returnAjax);
				return;
			} else {
				alert(returnAjax);
				location.reload();
			}
		}
    });
}

function viewBoardDetailReply(boardNo){
	
    $(document).on('keydown', function(event) {
        if (event.key === "Backspace") {
            var isInput = event.target.tagName === "INPUT" || event.target.tagName === "TEXTAREA";
            var isEditable = event.target.isContentEditable;

            if (!isInput && !isEditable) {
                event.preventDefault();
            }
        }
    });

	
	var param = {
		boardNo : boardNo,
		boardCode : boardCode
	}
	
	var mappingUrl = "/Board/BoardDetailViewProc.do";
	var returnAjax = doAjax(param, mappingUrl);
	
	try {
		if(returnAjax != ""){
			
			if(returnAjax.boardCode != 6){
				
				$("#pagination").hide();
				$("#writeBoard").hide();
				$("#deleteBoard").hide();
				$("#backBoard").show();	
				
				if (returnAjax.register == currentUser) {
					$("#updateBoard").attr("data-boardno", returnAjax.boardNo);
					$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
					$("#updateBoard").show();
					$("#deleteBoard").show();
				}	
				
				$('.featured').empty();
				
				var newRow = $('.featured');
				var replyRow = $('.featured');
				newRow.append("<h2>" + returnAjax.title + "</h2>");
				newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
					    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
					    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
				newRow.append("<hr class='hr-5'>");
				newRow.append("<br>");
				newRow.append(returnAjax.content);
				newRow.append("<br>");
				
				if(boardCode > 5){
					var htmlContent = "<section class='featured4'>";
					htmlContent += "<strong style='font-size: 16px;'>댓글</strong>";
					htmlContent += "<hr class='hr-5'>";
					htmlContent += "<br>";
					
					if (returnAjax.replyList && returnAjax.replyList.length > 0) {
						returnAjax.replyList.forEach(function(reply) {
							htmlContent += "<div class='featured5'>"
								+ "<div class='author-section'><img class='userImg' src='" + 
							    (reply.userImgname == null || reply.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + reply.userImgname + ".gif") + 
							    "'><strong>" + reply.register + "</strong></div>"
							if (reply.register === currentUser) {
								htmlContent += "<div class='reply-actions'>";
								htmlContent += "<div class='replyBtns'>";
								htmlContent += "<input type='button' id='replyDelete' value='삭제' class='button btnFade btnBlueGreen' data-replyno='" + reply.replySeq + "'>";
								htmlContent += "</div>";
								htmlContent += "</div>";
							}
							
							htmlContent +=  "<div><span class='date'>작성일: " + reply.replyDate + "</span></div>"
							htmlContent += "<div class='replyAdd'>" + reply.replyContent + "</div>"
							htmlContent += "</div>";
							
						});
					} else {
						htmlContent += "<div class='no-replies'>댓글이 없습니다.</div>";
					}
					
					htmlContent += "</section>";
					newRow.append(htmlContent);
					newRow.append("<br>");
					replyRow.append("<section class='featured3'>"
						+"<div style='margin-bottom: 5px;'>"
						+"<strong>" + currentUser + "</strong>"
						+"</div>"
						+"<div class='replyContainer'>"
						+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
						+"<input type='text' id='replyContent' class='content' placeholder='소중한 댓글을 입력해주세요.' />" 
						+"<input type='button' id='replySubmit' value='댓글 입력' class='replySubmit btnFade btnBlue'/>"
						+"</div>"
						+"</section>"
					);
    	       	} else if(returnAjax.boardCode == 6){
					
					$("#writeBoard").hide();
					$("#backBoard").show();
					
					if (returnAjax.register === currentUser) {
						$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
						$("#deleteBoard").attr("data-boardCode", returnAjax.boardCode);
						$("#deleteBoard").show();
					}
					
					$('.featured').empty();
					
					var newRow = $('.featured');
					var replyRow = $('.featured');
					
					newRow.append("<h2>"+ returnAjax.title + "</h2>");
					newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
						    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
						    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
					newRow.append("<hr class='hr-5'>");
					newRow.append("<br>");
					newRow.append("<br>");
					
					if(returnAjax.auctionType == '아이템'){
						newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemNo/" + returnAjax.itemImgNum + ".gif?ref_type=heads'></div>");
						newRow.append("<div class='form-group'><strong>아이템 정보</strong>"
							+ "<section id='itemName' class='featured2'>" + returnAjax.itemName + "</section></div>");
					} else {
						newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemImages/" + returnAjax.petImgNum + ".gif?ref_type=heads'></div>");
						newRow.append("<div class='form-group'><strong>수마 정보</strong>"
							+ "<section id='petName' class='featured2'>" + returnAjax.petName + "</section></div>");
					}
					
					if(returnAjax.startPrice == 'gold'){
						newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
							+ "<section id='auctionPriceType' class='featured2'> 골드 </section></div>");
					} else {
						newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
							+ "<section id='auctionPriceType' class='featured2'> 코인 </section></div>");
					}
					
					newRow.append("<div class='form-group'><strong>경매 시작 가격</strong><section id='auctionPrice' class='featured2'>" + returnAjax.formatAuctionPrice + "</section></div>");
					
					if(returnAjax.customBidUnit != null || returnAjax.customBidUnit !=""){
						newRow.append("<div class='form-group'><strong>골드 환산 비율</strong><section id='customBidUnit' class='featured2'>" + returnAjax.customBidUnit + "</section></div>");
    				}
					
					newRow.append("<div class='form-group'><strong>경매 종료일</strong>"
						+ "<section id='auctionEndDate' class='featured2'>" + returnAjax.auctionEndDate + "</section></div>"
						+ "<div class='form-group'><strong>경매품목 정보</strong>"
						+ "<section id='content' class='featured2'>" + returnAjax.content + "</section></div>");
					newRow.append("<br>");
					
					var htmlContent = "<section class='featured4'>";
					
					htmlContent += "<strong style='font-size: 16px;'>입찰 내역</strong><span style='color: red;'> &nbsp&nbsp※입찰내역 은 최고높은 금액 하위로 5개까지 기록됩니다.</span>";
					htmlContent += "<hr class='hr-5'>";
					htmlContent += "<br>";
					
					if (returnAjax.bidTopList && returnAjax.bidTopList.length > 0) {
						returnAjax.bidTopList.forEach(function(bid) {
							htmlContent += "<div class='featured5'>"
								+ "<div class='author-section'><img class='userImg' src='" + 
							    (bid.userImgname == null || bid.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + bid.userImgname + ".gif") + 
							    "'></div>"
								+ "<strong>" + bid.bidder + "</strong>: " + bid.bidAmount
							htmlContent +=  "<div><span class='bidDate'>작성일: " + bid.bidRegistDtm + "</span></div>"
							htmlContent += "</div>";
					});
					} else {
    	       		    htmlContent += "<div class='no-replies'>입찰내역이 없습니다.</div>";
    	       		}

    	       		htmlContent += "</section>";

    	       		newRow.append(htmlContent);
    	       		
    	       		newRow.append("<br>");
    	       		replyRow.append("<section class='featured3'>" 
    	    	       	+"<div style='margin-bottom: 5px;'>"
    	    	       	+"<strong>" + currentUser + "</strong>"
    	    	       	+"</div>"
    	    	       	+"<div class='replyContainer'>"
    	    	       	+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
    	    	       	+"<input type='text' id='replyContent' class='content' placeholder='신중히 고민 후 입찰하세요.' />" 
    	    	       	+"<input type='button' id='bidSubmit' value='경매 입찰' class='replySubmit btnFade btnBlue' />"
    	    	       	+"</div>"
    	    	       	+"</section>");
    	       	} else {
    	       		alert("게시글 응답이 올바르지 않습니다.");
    	       		return;
    	       	}
			}
		} else {
			alert('상세 정보가 없습니다.');
			
			return;
		}
	} catch (e){
		alert("게시글 응답 처리하는 중 오류가 발생하였습니다.");
		
		return;
	}
}

function replyModify(){
	
	var replySeq = $(this).data("replyno");
	var register = "${userNick}";
	var boardNo = $('#replyBoardNo').val();
	
	var param ={
			replySeq : replySeq,
			register : register,
			boardNo : boardNo
	}
	
	var mappingUrl = "/Board/ReplyModifyProc.do";
	
	var returnAjax = doAjax(param, mappingUrl);
	
	returnAjax = decodeURI(returnAjax);
    
    if (returnAjax.indexOf('오류') == 0
			|| returnAjax.indexOf('실패') == 0) {
		alert(returnAjax);
		return;
	} else {
		viewBoardDetailReply(boardNo);
	}
	
}


function replyDelete(){
	
	var replySeq = $(replyNo).data("replyno");
	var register = "${userNick}";
	var boardNo = $('#replyBoardNo').val();
	
	var param ={
			replySeq : replySeq,
			register : register,
			boardNo : boardNo
	}
	
	var mappingUrl = "/Board/ReplyDeleteProc.do";
	
	var returnAjax = doAjax(param, mappingUrl);
	
	returnAjax = decodeURI(returnAjax);
    
    if (returnAjax.indexOf('오류') == 0
			|| returnAjax.indexOf('실패') == 0) {
		alert(returnAjax);
		return;
	} else {
		viewBoardDetailReply(boardNo);
	}
}


function viewBoardDetailFromButton(button) {
	
	var boardNo = $(button).data("boardno");
    var boardCode = $(button).data("boardcode");

    if (!boardNo || !boardCode) {
        alert("게시글 정보를 가져오는 데 실패했습니다.");
        return;
    }
    
    var param = {
            boardNo: boardNo,
            boardCode: boardCode
        };
    
    var mappingUrl = "/Board/BoardDetailViewProc.do";
    
    var returnAjax = doAjax(param, mappingUrl);
    
    try {
    	if(returnAjax != ""){
    		
			backSpace = 1;
			
    		if(returnAjax.boardCode != 6){
    			$("#typeSelect").hide();
        		$("#pagination").hide();
        		$("#writeBoard").hide();
                $("#deleteBoard").hide();
                $("#backBoard").show();
                if (returnAjax.previousPost) {
                    $("#prevPost").attr("data-boardno", returnAjax.previousPost.boardNo);
                    $("#prevPost").attr("data-boardcode", returnAjax.previousPost.boardCode);
                    $("#prevPost").show();
                } else {
                    $("#prevPost").hide();
                }
                if (returnAjax.nextPost) {
                    $("#nextPost").attr("data-boardno", returnAjax.nextPost.boardNo);
                    $("#nextPost").attr("data-boardcode", returnAjax.nextPost.boardCode);
                    $("#nextPost").show();
                } else {
                    $("#nextPost").hide();
                }
				if (returnAjax.register === currentUser) {
					$("#updateBoard").attr("data-boardno", returnAjax.boardNo);
					$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
                    $("#updateBoard").show();
                    $("#deleteBoard").show();
                }
        		
        		$('.featured').empty();
        		var newRow = $('.featured');
        		var replyRow = $('.featured');
        		
        		newRow.append("<h2>" + returnAjax.title + "</h2>");
        		newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
					    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
					    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
        		newRow.append("<hr class='hr-5'>");
        		newRow.append("<br>");
        		newRow.append(returnAjax.content);
        		newRow.append("<br>");
        		
        		if(boardCode > 5){
        			
	        		var htmlContent = "<section class='featured4'>";
	        		htmlContent += "<strong style='font-size: 16px;'>댓글</strong>";
	        		htmlContent += "<hr class='hr-5'>";
	        		htmlContent += "<br>";
	        		
	        		if (returnAjax.replyList && returnAjax.replyList.length > 0) {
	        		    returnAjax.replyList.forEach(function(reply) {
	        		        htmlContent += "<div class='featured5'>"
	        		        	+ "<div class='author-section'><img class='userImg' src='" + 
	    					    (reply.userImgname == null || reply.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + reply.userImgname + ".gif") + 
	    					    "'><strong>" + reply.register + "</strong></div>"
	        		            if (reply.register === currentUser) {
	        		            	htmlContent += "<div class='reply-actions'>";
	        		            	htmlContent += "<div class='replyBtns'>";
	        		            	/* htmlContent += "<input type='button' id='replyModify' value='수정' class='button btnFade btnBlueGreen' data-replyno='" + reply.replySeq + "'>"; */
	        		            	htmlContent += "<input type='button' id='replyDelete' value='삭제' class='button btnFade btnBlueGreen' data-replyno='" + reply.replySeq + "'>";
	        		            	htmlContent += "</div>";
	        		            	htmlContent += "</div>";
	        		            }
	        		            htmlContent +=  "<div><span class='date'>작성일: " + reply.replyDate + "</span></div>"
	        		            htmlContent += "<div class='replyAdd'>" + reply.replyContent + "</div>"
	        		            
	        		            htmlContent += "</div>";
	        		    });
	        		} else {
	        		    htmlContent += "<div class='no-replies'>댓글이 없습니다.</div>";
	        		}
	
	        		htmlContent += "</section>";
	
	        		newRow.append(htmlContent);
        		
        			newRow.append("<br>");
        			replyRow.append("<section class='featured3'>"
	        				+"<div style='margin-bottom: 5px;'>"
	        				+"<strong>" + currentUser + "</strong>"
	        				+"</div>"
	        				+"<div class='replyContainer'>"
	        				+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
	        				+"<input type='text' id='replyContent' class='content' placeholder='소중한 댓글을 입력해주세요.' />" 
	        				+"<input type='button' id='replySubmit' value='댓글 입력' class='replySubmit btnFade btnBlue'/>"
	        				+"</div>"
	        				+"</section>");
        		}
        		if (returnAjax.previousPost) {
                    $("#prevPost")
                        .data("boardno", returnAjax.previousPost.boardNo)
                        .data("boardcode", returnAjax.previousPost.boardCode)
                        .show();
                } else {
                    $("#prevPost").hide();
                }

                // 다음 글 버튼 업데이트
                if (returnAjax.nextPost) {
                    $("#nextPost")
                        .data("boardno", returnAjax.nextPost.boardNo)
                        .data("boardcode", returnAjax.nextPost.boardCode)
                        .show();
                }
        	}else if(returnAjax.boardCode == 6){
        		
        		$("#writeBoard").hide();
                $("#backBoard").show();
                if (returnAjax.register === currentUser) {
                	$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
                	$("#deleteBoard").attr("data-boardCode", returnAjax.boardCode);
                	$("#deleteBoard").show();
                }
        		$('.featured').empty();
        		var newRow = $('.featured');
        		var replyRow = $('.featured');
        		newRow.append("<h2>"+ returnAjax.title + "</h2>");
        		newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
					    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
					    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
        		newRow.append("<hr class='hr-5'>");
        		newRow.append("<br>");
        		newRow.append("<br>");
        		if(returnAjax.auctionType == '아이템'){
        			newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemNo/" + returnAjax.itemImgNum + ".gif?ref_type=heads'></div>");
        			newRow.append("<div class='form-group'><strong>아이템 정보</strong>"
                 			+ "<section id='itemName' class='featured2'>" + returnAjax.itemName + "</section></div>");
        		}else{
        			newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemImages/" + returnAjax.petImgNum + ".gif?ref_type=heads'></div>");
        			newRow.append("<div class='form-group'><strong>수마 정보</strong>"
    						+ "<section id='petName' class='featured2'>" + returnAjax.petName + "</section></div>");
        		}
        		if(returnAjax.startPrice == 'gold'){
        			newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
                    	    + "<section id='auctionPriceType' class='featured2'> 골드 </section></div>");
        		}else{
        			newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
                    	    + "<section id='auctionPriceType' class='featured2'> 코인 </section></div>");
        		}
        		newRow.append("<div class='form-group'><strong>경매 시작 가격</strong><section id='auctionPrice' class='featured2'>" + returnAjax.formatAuctionPrice + "</section></div>");
				if(returnAjax.customBidUnit != null || returnAjax.customBidUnit !=""){
					newRow.append("<div class='form-group'><strong>골드 환산 비율</strong><section id='customBidUnit' class='featured2'>" + returnAjax.customBidUnit + "</section></div>");
				}
        		newRow.append("<div class='form-group'><strong>경매 종료일</strong>"
                        + "<section id='auctionEndDate' class='featured2'>" + returnAjax.auctionEndDate + "</section></div>"
                        + "<div class='form-group'><strong>경매품목 정보</strong>"
                        + "<section id='content' class='featured2'>" + returnAjax.content + "</section></div>");
        		newRow.append("<br>");
        		
        		var htmlContent = "<section class='featured4'>";
        		htmlContent += "<strong style='font-size: 16px;'>입찰 내역</strong><span style='color: red;'> &nbsp&nbsp※입찰내역 은 최고높은 금액 하위로 5개까지 기록됩니다.</span>";
        		htmlContent += "<hr class='hr-5'>";
        		htmlContent += "<br>";
        		
        		if (returnAjax.bidTopList && returnAjax.bidTopList.length > 0) {
        		    returnAjax.bidTopList.forEach(function(bid) {
        		        htmlContent += "<div class='featured5'>"
        		        	+ "<div class='author-section'><img class='userImg' src='" + 
    					    (bid.userImgname == null || bid.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + bid.userImgname + ".gif") + 
    					    "'></div>"
        		            + "<strong>" + bid.bidder + "</strong>: " + bid.bidAmount
        		            htmlContent +=  "<div><span class='bidDate'>작성일: " + bid.bidRegistDtm + "</span></div>"
        		            
        		            
        		            htmlContent += "</div>";
        		    });
        		} else {
        		    htmlContent += "<div class='no-replies'>입찰 내역이 존재하지 않습니다.</div>";
        		}

        		htmlContent += "</section>";

        		newRow.append(htmlContent);
        		
        		newRow.append("<br>");
        		replyRow.append("<section class='featured3'>" 
        				+"<div style='margin-bottom: 5px;'>"
        				+"<strong>" + currentUser + "</strong>"
        				+"</div>"
        				+"<div class='replyContainer'>"
        				+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
        				+"<input type='text' id='replyContent' class='content' placeholder='신중히 고민 후 입찰하세요.' />" 
        				+"<input type='button' id='bidSubmit' value='경매 입찰' class='replySubmit btnFade btnBlue' />"
        				+"</div>"
        				+"</section>");
        	}
        	else{
        		alert("게시글 응답이 올바르지 않습니다.");
        		return;
        	}
    	} else {
    		alert('상세 정보가 없습니다.');
    		
    		return;
    	}
    }catch (e){
    	alert("게시글 응답 처리하는 중 오류가 발생하였습니다.");
    	return;
    }
}

function viewBoardDetail(){
	
	$(".boardRow").off("click").on("click", function() {
		
        var boardNo = $(this).data("boardno");
        
        var param = {
        		boardNo : boardNo,
        		boardCode : boardCode
    	    }
        
        var mappingUrl = "/Board/BoardDetailViewProc.do";
        
        var returnAjax = doAjax(param, mappingUrl);
        
        try {
        	if(returnAjax != ""){
        		
    			backSpace = 1;
    			
        		if(returnAjax.boardCode != 6){
        			$("#typeSelect").hide();
            		$("#pagination").hide();
            		$("#writeBoard").hide();
                    $("#deleteBoard").hide();
                    $("#backBoard").show();
                    if (returnAjax.previousPost) {
                        $("#prevPost").attr("data-boardno", returnAjax.previousPost.boardNo);
                        $("#prevPost").attr("data-boardcode", returnAjax.previousPost.boardCode);
                        $("#prevPost").show();
                    } else {
                        $("#prevPost").hide();
                    }
                    if (returnAjax.nextPost) {
                        $("#nextPost").attr("data-boardno", returnAjax.nextPost.boardNo);
                        $("#nextPost").attr("data-boardcode", returnAjax.nextPost.boardCode);
                        $("#nextPost").show();
                    } else {
                        $("#nextPost").hide();
                    }
    				if (returnAjax.register === currentUser) {
    					$("#updateBoard").attr("data-boardno", returnAjax.boardNo);
    					$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
                        $("#updateBoard").show();
                        $("#deleteBoard").show();
                    }
            		
            		$('.featured').empty();
            		var newRow = $('.featured');
            		var replyRow = $('.featured');
            		
            		newRow.append("<h2>" + returnAjax.title + "</h2>");
            		newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
    					    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
    					    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
            		newRow.append("<hr class='hr-5'>");
            		newRow.append("<br>");
            		newRow.append(returnAjax.content);
            		newRow.append("<br>");
            		
            		if(boardCode > 5){
            			
    	        		var htmlContent = "<section class='featured4'>";
    	        		htmlContent += "<strong style='font-size: 16px;'>댓글</strong>";
    	        		htmlContent += "<hr class='hr-5'>";
    	        		htmlContent += "<br>";
    	        		
    	        		if (returnAjax.replyList && returnAjax.replyList.length > 0) {
    	        		    returnAjax.replyList.forEach(function(reply) {
    	        		        htmlContent += "<div class='featured5'>"
    	        		        	+ "<div class='author-section'><img class='userImg' src='" + 
    	    					    (reply.userImgname == null || reply.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + reply.userImgname + ".gif") + 
    	    					    "'><strong>" + reply.register + "</strong></div>"
    	        		            if (reply.register === currentUser) {
    	        		            	htmlContent += "<div class='reply-actions'>";
    	        		            	htmlContent += "<div class='replyBtns'>";
    	        		            	/* htmlContent += "<input type='button' id='replyModify' value='수정' class='button btnFade btnBlueGreen' data-replyno='" + reply.replySeq + "'>"; */
    	        		            	htmlContent += "<input type='button' id='replyDelete' value='삭제' class='button btnFade btnBlueGreen' data-replyno='" + reply.replySeq + "'>";
    	        		            	htmlContent += "</div>";
    	        		            	htmlContent += "</div>";
    	        		            }
    	        		            htmlContent +=  "<div><span class='date'>작성일: " + reply.replyDate + "</span></div>"
    	        		            htmlContent += "<div class='replyAdd'>" + reply.replyContent + "</div>"
    	        		            
    	        		            htmlContent += "</div>";
    	        		    });
    	        		} else {
    	        		    htmlContent += "<div class='no-replies'>댓글이 없습니다.</div>";
    	        		}
    	
    	        		htmlContent += "</section>";
    	
    	        		newRow.append(htmlContent);
            		
            			newRow.append("<br>");
            			replyRow.append("<section class='featured3'>"
    	        				+"<div style='margin-bottom: 5px;'>"
    	        				+"<strong>" + currentUser + "</strong>"
    	        				+"</div>"
    	        				+"<div class='replyContainer'>"
    	        				+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
    	        				+"<input type='text' id='replyContent' class='content' placeholder='소중한 댓글을 입력해주세요.' />" 
    	        				+"<input type='button' id='replySubmit' value='댓글 입력' class='replySubmit btnFade btnBlue'/>"
    	        				+"</div>"
    	        				+"</section>");
            		}
            	}else if(returnAjax.boardCode == 6){
            		
            		$("#writeBoard").hide();
                    $("#backBoard").show();
                    if (returnAjax.register === currentUser) {
                    	$("#deleteBoard").attr("data-boardno", returnAjax.boardNo);
                    	$("#deleteBoard").attr("data-boardCode", returnAjax.boardCode);
                    	$("#deleteBoard").show();
                    }
            		$('.featured').empty();
            		var newRow = $('.featured');
            		var replyRow = $('.featured');
            		newRow.append("<h2>"+ returnAjax.title + "</h2>");
            		newRow.append("<div class='info-line'><div class='author-section'><img class='userImg' src='" + 
    					    (returnAjax.userImgname == null || returnAjax.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + returnAjax.userImgname + ".gif") + 
    					    "'><span class='author'>작성자: " + returnAjax.register + "</span></div><span class='date'>작성일: " + returnAjax.sysRegDtm + "</span></div>");
            		newRow.append("<hr class='hr-5'>");
            		newRow.append("<br>");
            		newRow.append("<br>");
            		if(returnAjax.auctionType == '아이템'){
            			newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemNo/" + returnAjax.itemImgNum + ".gif?ref_type=heads'></div>");
            			newRow.append("<div class='form-group'><strong>아이템 정보</strong>"
                     			+ "<section id='itemName' class='featured2'>" + returnAjax.itemName + "</section></div>");
            		}else{
            			newRow.append("<div class='form-group'><img id='auctionImgElement' src='/imgs/ItemImages/" + returnAjax.petImgNum + ".gif?ref_type=heads'></div>");
            			newRow.append("<div class='form-group'><strong>수마 정보</strong>"
        						+ "<section id='petName' class='featured2'>" + returnAjax.petName + "</section></div>");
            		}
            		if(returnAjax.startPrice == 'gold'){
            			newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
                        	    + "<section id='auctionPriceType' class='featured2'> 골드 </section></div>");
            		}else{
            			newRow.append("<div class='form-group'><strong>경매 화폐 유형</strong>"
                        	    + "<section id='auctionPriceType' class='featured2'> 코인 </section></div>");
            		}
            		newRow.append("<div class='form-group'><strong>경매 시작 가격</strong><section id='auctionPrice' class='featured2'>" + returnAjax.formatAuctionPrice + "</section></div>");
    				if(returnAjax.customBidUnit != null || returnAjax.customBidUnit !=""){
    					newRow.append("<div class='form-group'><strong>골드 환산 비율</strong><section id='customBidUnit' class='featured2'>" + returnAjax.customBidUnit + "</section></div>");
    				}
            		newRow.append("<div class='form-group'><strong>경매 종료일</strong>"
                            + "<section id='auctionEndDate' class='featured2'>" + returnAjax.auctionEndDate + "</section></div>"
                            + "<div class='form-group'><strong>경매품목 정보</strong>"
                            + "<section id='content' class='featured2'>" + returnAjax.content + "</section></div>");
            		newRow.append("<br>");
            		
            		var htmlContent = "<section class='featured4'>";
            		htmlContent += "<strong style='font-size: 16px;'>입찰 내역</strong><span style='color: red;'> &nbsp&nbsp※입찰내역 은 최고높은 금액 하위로 5개까지 기록됩니다.</span>";
            		htmlContent += "<hr class='hr-5'>";
            		htmlContent += "<br>";
            		
            		if (returnAjax.bidTopList && returnAjax.bidTopList.length > 0) {
            		    returnAjax.bidTopList.forEach(function(bid) {
            		        htmlContent += "<div class='featured5'>"
            		        	+ "<div class='author-section'><img class='userImg' src='" + 
        					    (bid.userImgname == null || bid.userImgname.trim() === "" ? "/img/profileImg.png" : "/imgs/ItemImages/" + bid.userImgname + ".gif") + 
        					    "'></div>"
            		            + "<strong>" + bid.bidder + "</strong>: " + bid.bidAmount
            		            htmlContent +=  "<div><span class='bidDate'>작성일: " + bid.bidRegistDtm + "</span></div>"
            		            
            		            
            		            htmlContent += "</div>";
            		    });
            		} else {
            		    htmlContent += "<div class='no-replies'>입찰 내역이 존재하지 않습니다.</div>";
            		}

            		htmlContent += "</section>";

            		newRow.append(htmlContent);
            		
            		newRow.append("<br>");
            		replyRow.append("<section class='featured3'>" 
            				+"<div style='margin-bottom: 5px;'>"
            				+"<strong>" + currentUser + "</strong>"
            				+"</div>"
            				+"<div class='replyContainer'>"
            				+"<input type='hidden' id='replyBoardNo' name='boardNo' value='" + returnAjax.boardNo + "' />" 
            				+"<input type='text' id='replyContent' class='content' placeholder='신중히 고민 후 입찰하세요.' />" 
            				+"<input type='button' id='bidSubmit' value='경매 입찰' class='replySubmit btnFade btnBlue' />"
            				+"</div>"
            				+"</section>");
            	}
            	else{
            		alert("게시글 응답이 올바르지 않습니다.");
            		return;
            	}
        	} else {
        		alert('상세 정보가 없습니다.');
        		
        		return;
        	}
        }catch (e){
        	alert("게시글 응답 처리하는 중 오류가 발생하였습니다.");
        	return;
        }
        
    });
}

function writeBoardBtn(){
	
	$("#writeBoard").click(function(){
		
		$("#boardCode").val(boardCode);
		$("#pageNum").val("1");
		$("#boardNo").val("0");
		
		doSubmitAction("/Board/GoNoticeBoardProc.do");	
		
	});
}

function backBoardBtn() {
    $("#backBoard").click(function() {
        location.reload();
    });
}

function updateBoardBtn() {
    $("#updateBoard").click(function() {
    	
        var boardNo = $("#updateBoard").data("boardno");
        $("#pageNum").val("1");
        $("#boardCheck").val("1");
        $("#boardCode").val(boardCode);
        $("#boardNo").val(boardNo);
        doSubmitAction("/Board/GoNoticeBoardProc.do");
    });
}

function doSubmitAction(str) {
	var form = $("#board");
	
	form.attr("action", str);
	form.attr("method", "POST");

	form.submit();
}

function doAjax(param, mappingUrl) {
	var returnVal;
	
	$.ajax({
		type : "POST",
		async : false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	
	return returnVal;
}

function changePage(pageNum) {
	var boardNo = $("#updateBoard").data("boardno");
	var boardSearch = $('#boardSearchHidden').val();
	
	if (!pageNum) {
		pageNum = 1;
	}
	
	doSubmit('/Board/GoBoardProc.do?sin=' + encodeURIComponent(boardSearch) + 
            '&boardCode=' + encodeURIComponent(boardCode))
            
	$('#pageNum').val(pageNum);
    $("#boardCheck").val("1");
    $("#boardNo").val("1");
    if (boardSearch !== '') {
                
        doSubmit('/Board/GoBoardProc.do?sin=' + encodeURIComponent(boardSearch) + 
                '&boardCode=' + encodeURIComponent(boardCode) + '&pageNum=' + pageNum)
   	} else {
	    doSubmitAction("/Board/GoBoardProc.do?pageNum=" + pageNum +
            "&boardCode=" + encodeURIComponent(boardCode));
   		
   	}
    
}

</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	<form id="board">
		<input type="hidden" id="boardCheck" name="boardCheck" value="" />
		<input type="hidden" id="pageNum" name ="pageNum" value="" />
		<input type="hidden" id="boardCode" name="boardCode" value="${boardCode}" />
		<input type="hidden" id="boardNo" name="boardNo" value="" />
		<input type="hidden" id="tradeType" name="tradeType"> 
		<input type="hidden" id="boardSearchHidden" name="sin" value="${param.sin}">
	    <main>
	        <div class="container">
	            <section class="categoriesTable" style="margin-top: 1.5%;">
	            	<p>※레드서버
	            		<c:choose>
	            			<c:when test="${boardCode == 1}"> 공지사항※</c:when>
	            			<c:when test="${boardCode == 2}"> 패치노트※</c:when>
	            			<c:when test="${boardCode == 3}"> 레드서버 기능※</c:when>
	            			<c:when test="${boardCode == 4}"> 레드서버 가이드※</c:when>
	            			<c:when test="${boardCode == 5}"> 이벤트※</c:when>
	            			<c:when test="${boardCode == 6}"> 경매장※</c:when>
	            			<c:when test="${boardCode == 7}"> 거래게시판※</c:when>
	            			<c:when test="${boardCode == 8}"> 자유게시판※</c:when>
	            			<c:when test="${boardCode == 9}"> 정보게시판※</c:when>
	            			<c:when test="${boardCode == 10}"> 공략게시판※</c:when>
	            			<c:when test="${boardCode == 11}"> 퀘스트게시판※</c:when>
	            			<c:when test="${boardCode == 12}"> 건의게시판※</c:when>
	            			<c:otherwise> QnA게시판※</c:otherwise>
	            		</c:choose> 
	            	</p>
	            </section>
	            
		         <c:if test="${userInfo.userRole == 999 and boardCode > 0}">
		            <section class="categoriesTable">
		            	<button type="button" id="writeBoard" class="button btnFade btnBlueGreen">글쓰기</button>
		            	<c:if test="${boardCode == 7}">
		            		<select id="typeSelect" class="select" onchange="fetchBoardData()">
		            			<option disabled="disabled" selected="selected">선택하세요</option>
		            			<option value="all">전체</option>
		            			<option value="buy">구매</option>
		            			<option value="sell">판매</option>
		            		</select>
		            	</c:if>
		            	<button type="button" id="backBoard" class="button btnFade btnBlueGreen" style="display: none;">뒤로가기</button>
						<button type="button" id="updateBoard" class="button btnFade btnBlueGreen" style="display: none;">수정</button>
		            	<button type="button" id="deleteBoard" class="button btnFade btnBlueGreen" style="display: none;">삭제</button>
						<button type="button" id="prevPost" class="button btnFade btnBlueGreen" style="display: none;" onclick="viewBoardDetailFromButton(this)">이전 글</button>
						<button type="button" id="nextPost" class="button btnFade btnBlueGreen" style="display: none;" onclick="viewBoardDetailFromButton(this)">다음 글</button>
						<div class="search-box">
        					<input type="text" id="boardSearch" class="content" style="width: 300px;" placeholder="검색할 게시물을 입력해주세요">
        					<input type="button" id="boardSearchBtn" class="button btnFade btnBlueGreen leftButtons" value="검색">
    					</div>
					</section>
				</c:if>
		         <c:if test="${userInfo.userRole == 1 and boardCode >= 6}">
				    <section class="categoriesTable">
				        <button type="button" id="writeBoard" class="button btnFade btnBlueGreen">글쓰기</button>
				        <button type="button" id="backBoard" class="button btnFade btnBlueGreen" style="display: none;">뒤로가기</button>
				        <c:if test="${boardCode != 6}">
				            <button type="button" id="updateBoard" class="button btnFade btnBlueGreen" style="display: none;">수정</button>
				        </c:if>
				        <button type="button" id="deleteBoard" class="button btnFade btnBlueGreen" style="display: none;">삭제</button>
				        <div class="search-box">
        					<input type="text" id="boardSearch" class="content" style="width: 300px;" placeholder="검색할 게시물을 입력해주세요">
        					<input type="button" id="boardSearchBtn" class="button btnFade btnBlueGreen leftButtons" value="검색">
    					</div>
				    </section>
				</c:if>
				
	            <section class="featured">
		            <table>
			            <colgroup>
							<col style="width: 7%;">
							<col style="width: 48%;">
							<col style="width: 20%;">
							<col style="width: 20%;">
						</colgroup>
				        <thead>
				            <tr>
				                <th>번호</th>
				                <th>제목</th>
				                <th>작성자</th>
				                <th>작성일시</th>
				            </tr>
				        </thead>
				        <tbody>
				            <c:if test="${not empty boardList}">
				                <c:forEach items="${boardList}" var="board">
				                    <tr>
				                        <td style="text-align: center;">${board.boardNo}</td>
				                        <td class="boardRow" data-boardno="${board.boardNo}">${board.title}
					                        <c:if test="${board.replyCnt > 0}">
					                        	<span style="color : red; font-weight: bold;"> [${board.replyCnt}]</span>
					                        </c:if>
				                        </td>
				                        <td style="text-align: center;">${board.register}</td>
				                        <td style="text-align: center;">${board.sysRegDtm}</td>
				                    </tr>
				                </c:forEach>
				            </c:if>
				            <c:if test="${empty boardList}">
				                <tr>
				                    <td colspan="4" style="text-align: center;"><strong>${emptyList}</strong></td>
				                </tr>
				            </c:if>
				        </tbody>
				    </table>
	            </section>
				
	           <section id="pagination" class="pagination">
				    <c:if test="${pagingVo.totalPage > 1}">
				        <ul>
				            <c:forEach var="i" begin="1" end="${pagingVo.totalPage}">
				                <li>
				                    <a href="javascript:changePage(${i});" class="${i == pagingVo.pageNum ? 'selected-page' : ''}">${i}</a>
				                </li>
				            </c:forEach>
				        </ul>
				    </c:if>
				</section>

	        </div>
	        
	    <!-- Footer -->
	       <%@ include file="../../Section/User/Footer.jsp"%>
	    </main>
	</form>
</body>
</html>