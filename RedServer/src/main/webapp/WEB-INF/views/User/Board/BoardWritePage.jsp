<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/BoardWrite.css?ver=20240506" />
</head>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	    <main>
	        <div class="container">
	            <section class="categoriesTable">
	            				
	            	 <a href="#" class="button btnFade btnBlueGreen">글쓰기</a>
	            	
	            	 <div id="content">
	            	 
	            	 </div>
	            </section>
				<section class="contents" id="contents">
				    <form action="post_article.jsp" method="POST">
				        <div class="form-group">
				            <label for="title">제목:</label>
				            <input type="text" id="title" name="title" class="form-control" required>
				        </div>
				        <div class="form-group">
				            <label for="content">내용:</label>
				            <textarea id="content" name="content" class="form-control" rows="8" required></textarea>
				        </div>
				        <button class="button btnFade btnBlueGreen">등록</button>
				    </form>
				</section>
	        </div>
	        
	    <!-- Footer -->
		<%@ include file="../../Section/User/Footer.jsp"%>
    </main>
</body>
</html>