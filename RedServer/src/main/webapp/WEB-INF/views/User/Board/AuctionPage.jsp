<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/BoardWrite.css?ver=20240506" />
</head>
<script>
var boardCode;

$(document).ready(function() {
	
	submitBtnClick();
	auctionInfoBtn();
	selectPetAuctionList();
	selectItemAuctionList();
	
	boardCode = "${boardCode}";
	
	$("#auctionEndDate").datetimepicker({
        minDate: 0,
        maxDate: '+3d',
        dateFormat: 'yy-mm-dd',
        timeFormat: 'HH:mm',
        controlType: 'select', 
        oneLine: true, 
        onSelect: function(selectedDateTime) {
            $("#auctionEndDate").val(selectedDateTime); 
        }
    });
	
	$('#startPrice').change(function() {
	       if ($(this).val() === 'custom') {
	           $('#customBidUnitGroup').show();
	       } else {
	           $('#customBidUnitGroup').hide();
	       }
	});
	
	CKEDITOR.replace('content', {
		removePlugins: 'sourcearea,maximize,elementspath',
	    filebrowserUploadUrl: '/Board/ImageUpload.do'
	});
	
	
	var escapedContent = $('#escapedContent').val();
    var decodedContent = $('<textarea />').html(escapedContent).text();
    CKEDITOR.instances.content.setData(decodedContent);
	
});

function updateBidUnitOptions() {
    var selectedValue = $('#startPrice').val();
    var options = [];

    if (selectedValue === 'coin') {
        options = [10, 50, 100, 500, 1000, '직접입력'];
    } else if (selectedValue === 'gold') {
        options = [50000, 100000, 500000, 1000000, '직접입력'];
    }

    var $bidUnitSelect = $('#bidUnit');
    $bidUnitSelect.empty(); 

    $.each(options, function(index, value) {
        var $option = $('<option>').val(value === '직접입력' ? 'custom' : value).text(value);
        $bidUnitSelect.append($option);
    });
    
    $('#customBidUnitGroup').hide();
}



function selectPetAuctionList(){
	
	$('#acSelectBoxPet').change(function() {
        var baseImgNum = $(this).val();
        $('#title').val('[수마 경매] ');
        $('#auctionImg').css('display', 'block');
        $('#auctionImgElement').attr('src', '/imgs/ItemImages/' + baseImgNum + '.gif?ref_type=heads');
  	});
	
}

function selectItemAuctionList(){
	
	$('#acSelectBoxItem').change(function() {
        var baseImgNum = $(this).val();
        $('#title').val('[아이템 경매] ');
        $('#auctionImg').css('display', 'block');
        $('#auctionImgElement').attr('src', '/imgs/ItemNo/' + baseImgNum + '.gif?ref_type=heads');
  	});
	
}

function auctionInfoBtn(){
	
	
    $('#auctionInfoBtn').click(function (){
        var auctionType = $('input[name="selection"]:checked').val();
        var cdKey = $('#auctionSelectBox').val();
        
        var form = $('<form action="/Board/AuctionProc.do" method="POST"></form>');
        
        form.append('<input type="hidden" name="auctionType" value="' + auctionType + '">');
        form.append('<input type="hidden" name="cdKey" value="' + cdKey + '">');
        form.append('<input type="hidden" name="boardCode" value="' + boardCode + '">');
        
        $('body').append(form);
        form.submit();
        
        form.remove();
        
    });
}



function submitBtnClick(){
	
	$('#submitBtn').click(function (){
		
		var boardNo = $('#boardNo').val();
		var title = $('#title').val();
		var petName = $('#acSelectBoxPet option:selected').text();
        var itemName = $('#acSelectBoxItem option:selected').text();
		var date = $('#auctionEndDate').val();
		var auctionType = $('input[name="selection"]:checked').val();
		var auctionAccount = $('#auctionSelectBox').val();
        var petImgNum = $('#acSelectBoxPet').val();
        var itemImgNum = $('#acSelectBoxItem').val();
        var startPrice = $('#startPrice').val();
        var customBidUnit = $('#customBidUnit').val();
        var auctionEndDate = $('#auctionEndDate').val();
        var content = CKEDITOR.instances.content.getData();
        var auctionPrice = $('#auctionPrice').val();

        // 제목
        if (title.trim() === "") {
            alert("제목을 입력해주세요.");
            return;
        }

        // 경매 종료일
        if (auctionEndDate.trim() === "") {
            alert("경매 종료일을 선택해주세요.");
            return;
        }

        // 게임 계정 
        if (auctionAccount === "") {
            alert("게임 계정을 선택해주세요.");
            return;
        }

        // 펫 경매 및 아이템 경매 (둘 중 하나는 반드시 선택)
        if (petImgNum === "" && itemImgNum === "") {
            alert("펫 경매 또는 아이템 경매를 선택해주세요.");
            return;
        }

      	
        if (customBidUnit.trim() === "custom") {
        	bidUnit = 0;
            if (customBidUnit.trim() === "" || customBidUnit === null || customBidUnit <= 0) {
                alert("환산비율을 올바르게 입력해주세요.");
                return;
            }
        }
            
        // 경매 가격 
        if (isNaN(auctionPrice) || auctionPrice <= 0) {
            alert("경매 가격을 올바르게 입력해주세요.");
            return;
        }

        // 경매품목 정보
        if (content.trim() === "") {
            alert("경매품목 정보를 입력해주세요.");
            return;
        }
        
        if(customBidUnit == ""){
        	customBidUnit = 0;
        }
        
		var param = {
    			content : content,
            	title : title,
            	auctionType : auctionType,
            	petName : petName,
            	itemName : itemName,
            	auctionAccount : auctionAccount,
            	petImgNum : petImgNum,
            	itemImgNum : itemImgNum,
            	startPrice : startPrice,
            	customBidUnit : customBidUnit,
            	auctionEndDate : auctionEndDate,
            	auctionPrice : auctionPrice,
            	boardCode : boardCode
            };
        
        if(boardNo) {
			boardNo = parseInt(boardNo);
	    	param.boardNo = boardNo;
	    }
		
		var mappingUrl = "/Board/BoardRegistProc.do";
        var returnAjax = doAjax(param, mappingUrl);
        var sendParam = "";
        
		returnAjax = decodeURI(returnAjax);
		
		sendParam = "boardCode=" + boardCode;
		
		if (returnAjax.indexOf('오류') == 0
				|| returnAjax.indexOf('입력') == 0) {
			alert(returnAjax);
			return;
		} else {
			alert(returnAjax);
			doSubmit("/Board/GoBoardProc.do?" + sendParam);
		} 
	}); 
}


function doSubmit(str) {
	var form = $("#doBoard");

	form.attr("action", str);
	form.attr("method", "POST");
	form.attr("enctype", "multipart/form-data");
	
	form.submit();
}


function doAjax(param, mappingUrl) {
	var returnVal;
	$.ajax({
		type : "POST",
		async: false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	return returnVal;
}



</script>
<body>
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
	    <main>
	    	<input type="hidden" id="category" name="category" value="${boardName}"/>
	    	<input type="hidden" id="boardNo" name="boardNo" value="${boardDetail.boardNo}"/>
	    	<input type="hidden" id="escapedContent" value="<c:out value='${boardDetail.content}'/>" />
	        <div class="container">
	            <section class="categoriesTable">
	            	<h1>※레드서버 경매장※</h1>
	            </section>
				<section class="contents" id="contents">
				    <form id="doBoard" method="POST">
				        <div class="form-group">
				            <label for="title">제목:</label>
				            <input type="text" id="title" class="title" name="title">
				        </div>
				        
				        <div style="display: flex; margin-bottom: 10px;">
					        <c:choose>
						        <c:when test="${not empty auctionPetList}">
						        	<input id="auctionTypePet" type="radio" name="selection" value="pet" checked>&nbsp;수마
						        </c:when>
					        	<c:otherwise>
									<input id="auctionTypePet" type="radio" name="selection" value="pet">&nbsp;수마
					        	</c:otherwise>
					        </c:choose>
					        <c:choose>
						        <c:when test="${not empty auctionItemList}">
									<input id="auctionTypeItem" style="margin-left: 20px;" type="radio" name="selection" value="item" checked>&nbsp;아이템
						        </c:when>
					        	<c:otherwise>
									<input id="auctionTypeItem" style="margin-left: 20px;" type="radio" name="selection" value="item">&nbsp;아이템
					        	</c:otherwise>
					        </c:choose>
				        </div>
				        
				        <div style="display: flex; align-items: baseline;">
					        <select id="auctionSelectBox" class="select" style="margin-right: 5px;">
					        	<option disabled selected>계정 선택</option>
								<c:forEach var="userList" items="${user}" varStatus="stat">
									<option value="${userList.accountId}" ${userList.accountId == aucId ? 'selected' : ''}>${userList.accountId}</option>
								</c:forEach>
							</select>
							
							    <c:if test="${not empty auctionPetList}">
							        <select id="acSelectBoxPet" class="select" style="margin-right: 5px; width:250px;">
							            <option disabled selected>펫 경매</option>
							            <c:forEach var="auctionPet" items="${auctionPetList}" varStatus="stat">
							                <option value="${auctionPet.baseImgNum}">${auctionPet.name}</option>
							            </c:forEach>
							        </select>
							    </c:if>
							    
							    <c:if test="${not empty auctionItemList}">
							        <select id="acSelectBoxItem" class="select" style="margin-right: 5px; width:250px;">
							            <option disabled selected>아이템 경매</option>
							            <c:forEach var="auctionItem" items="${auctionItemList}" varStatus="stat">
							                <option value="${auctionItem.baseImgNum}">${auctionItem.name}</option>
							            </c:forEach>
							        </select>
							    </c:if>
					        <input style="margin-left: 10px;" type="button" class="button btnFade btnBlueGreen" id="auctionInfoBtn" value="검색" />
				        </div>
				        
						<section id="auctionImg" class="categoriesTable" style="display: none; margin-top: 10px;">
							<div class="auctionInfo">
						        <img id="auctionImgElement" class="auctionImg">
						        <div class="form-container">
								        <div class="form-container">
									        <div class="form-group">
									            <label for="startPrice">경매 시작가격</label>
									            <select class="select" id="startPrice">
									                <option value="coin">코인</option>
									                <option value="gold">골드</option>
									                <option value="custom">코인/골드</option>
									            </select>
									            <input id="auctionPrice" class="objectInfo" type="text">
									        </div>
									        <!-- <div class="form-group">
									            <label for="bidUnit">입찰 단위</label>
									            <select class="select" id="bidUnit">
									            </select>
									        </div> -->
									        <div class="form-group" id="customBidUnitGroup" style="display: none;">
									            <label for="customBidUnit">골드 환산 비율</label>
									            <input id="customBidUnit" class="objectInfo" type="text">
									        </div>
									    </div>
								    <div class="form-group">
								        <label for="auctionEndDate">경매 종료일</label>
								        <input id="auctionEndDate" class="objectInfo" type="text">
								        <div id="calendarContainer"></div>
								    </div>
								    <div class="form-group">
								        <label for="content">경매품목 정보<span style="color: red;">&nbsp;※간략하게 적어주세요.</span></label>
								        <textarea id="content" name="content" rows="4"></textarea>
								    </div>
								</div>
						    </div>
	            		</section>				        
				        
				        <input type="button" class="button btnFade btnBlueGreen" id="submitBtn" value="등록" style="margin-top: 5px;"/>
				    </form>
				</section>
	        </div>
	        
	    <!-- Footer -->
		<%@ include file="../../Section/User/Footer.jsp"%>
    </main>
    <!-- CKEditor 적용 -->
</body>
</html>
