<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html class="html">
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="ie-edge"/>
    <meta name="keywords" content="Yulian Brito, Yulian, Brito, Frontend Developer, Sign Up Sign In 1"/>
    <meta name="author" content="Yulian Brito"/>
    <title>레드서버 로그인</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/> 
    <link rel="stylesheet" href="/css/Login.css?ver=20241123" />
    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>

<script>

	/* 엔터키 이벤트 */
	$(document).ready(function(){
		$('#userId, #userPw').keypress(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault(); 
				doLogin();
			}
		});
	});

	function doLogin() {
		var userId = $('#userId').val();
		var userPw = $('#userPw').val();

		if (userId == "") {
			alert("아이디를 입력해 주세요.");
			return;
		} else if (userPw == "") {
			alert("패스워드를 입력해 주세요.");
			return;
		}

		var param = {
			userId : userId,
			userPw : userPw
		}

		var mappingUrl = "/Login/LoginProc.do";

		$.ajax({
			url : mappingUrl,
			type : 'POST',
			data : param,
			success : function(response) {

				if (response === 'success') {
					$('#litheader').addClass("poweron");
					$('#go').removeClass("denied").addClass("");
					$('#go').val("Connecting...");

					setTimeout(function() {
						doSubmit('/Login/MainProc.do');
					}, 1000);
				} else {
					$('#litheader').removeClass("");
					$('#go').removeClass("").addClass("denied");
					$('#go').val("Access Denied");

					alert(decodeURI(response));
				}
			},
			error : function(xhr, status, error) {
				if(xhr.status == 500){
					location.href = "/";
				} else {
					alert('오류가 발생했습니다.');
					return;
				}
			},
			beforeSend: function(xhr){
				xhr.setRequestHeader("AJAX", "true");
			}
		});
	}

	function doSubmit(str) {
		var form = $("#accesspanel");

		form.attr("action", str);
		form.attr("method", "POST");

		form.submit();
	}

	function r_func() {
		var mappingUrl = "/Login/RegPageProc.do";
		doSubmit(mappingUrl);
	}

	function m_func() {
		var mappingUrl = "/Login/MyPageProc.do";
		doSubmit(mappingUrl);
	}
</script>

<body class="body">
	<div class="background-wrap">
		<div class="background"></div>
	</div>
	
	<form id="accesspanel">
		<h1 id="litheader">RED SERVER</h1>
		<div class="inset">
			<p>
				<input type="text" name="userId" id="userId" placeholder="User Id">
			</p>
			<p>
				<input type="password" name="userPw" id="userPw" placeholder="Password">
			</p>
			
			<input class="loginLoginValue" type="hidden" name="service" value="login" />
		</div>
		<p class="p-container">
			<input type="button" name="Login" id="go" value="LOGIN" onclick="doLogin()">
		</p>
		<div class="button-container">
			<button class="btn" id="register" onclick="r_func()"><span>신규유저</span></button>
			<button class="btn" id="myPage" onclick="m_func()"><span>기존유저</span></button>
    	    <button class="btn" onclick="window.open('https://discord.gg/kxTDkvzbmp', '_blank')"><span>디스코드</span></button>
	        <button class="btn" onclick="window.open('https://cafe.naver.com/redongam', '_blank')"><span>네이버카페</span></button>
        </div> 
	</form>
</body>
</html>