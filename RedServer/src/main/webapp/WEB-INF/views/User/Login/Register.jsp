<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="html">
  <head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>  
    <meta name="viewport" content="width=device-width",initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="ie-edge"/>
    <meta name="keywords" content="Yulian Brito, Yulian, Brito, Frontend Developer, Sign Up Sign In 1"/>
    <meta name="author" content="Yulian Brito"/>
    <title>레드서버 회원가입</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="/css/Register.css?ver=20240813" />
  </head>
<script>
var checkStep1 = 0;
var checkStep2 = 0;
var checkStep3 = 0;
	
function doSubmit() {
	var userId = $("#username").val();
    var userNick = $("#nickname").val();
    var userEmail = $("#email").val();
    var rnumber = $("#mail-check-input").val();
    var userPw = $("#password").val();
    var userPw2 = $("#confirm_password").val();
    var pw = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&?_\-]).{4,15}$/;
    
    if(checkStep1 == 0){
		alert("아이디 중복을 체크해 주세요.");
        return;
	} else if(checkStep2 == 0){
		alert("닉네임을 중복을 체크해 주세요.");
        return;
	} else if(checkStep3 == 0){
		alert("이메일 인증을 해주세요.");
        return;
	}

    // 비밀번호 유효성 검사
    if (!pw.test(userPw) || userPw.length < 4 || userPw != userPw2) {
    	if(!validatePasswords()){
			alert('비밀번호를 확인해 주세요.');
			return;
		};
    }

    var param = {
        userId: userId,
        userNick: userNick,
        userEmail: userEmail,
        rnumber: rnumber,
        userPw: userPw,
        userPw2: userPw2,
    };

    $.ajax({
        type: "POST",
        url: "/Register/RegSignUp.do",
        data: param,
        dataType: 'json',
        success: function(response) {
            if (response == 1) {
                alert("회원가입에 성공하였습니다.");
                location.href = "/Login/LoginMain.do";
            } else {
                alert("회원가입에 실패하였습니다.");
            }
        },
        error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
    });
}


function validatePasswords() {
	var userPw = $("#password").val();
	var userPw2 = $("#confirm_password").val();
	var pw = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%^&*?_]).{4,15}$/;

	$("#error-message").text("");

	if (!pw.test(userPw) || userPw.length < 4) {
		$("#password").css("border-bottom", "1px solid red");
		$("#error-message").text("비밀번호는 영문 과 특수기호를 포함한 4자리 이상이어야 합니다.");
		
		return false;
	} else {
		$("#password").css("border-bottom", "1px solid green");
	}

	if (userPw !== userPw2) {
		$("#confirm_password").css("border-bottom", "1px solid red");
		$("#error-message").text("비밀번호가 일치하지 않습니다.");
		
		return false;
	} else if (userPw2.length > 0) {
		$("#confirm_password").css("border-bottom", "1px solid green");
	}
	
	return true;
}


function changeInput(e) {
	$(e).css("color", "white");
	$(e).css("border-bottom", "1px solid #ffffff");
}

function fn_IdCheck() {
	var userId = $("#username").val();
	
	checkStep1 = 0;
	
    var idPattern = /^[a-zA-Z0-9]+$/; 
    if (!idPattern.test(userId)) {
        alert("아이디는 영문과 숫자만 포함할 수 있습니다.");
        $("#username").css("border-bottom", "1px solid red");
        return;
    }
    
    if(userId == ""){
    	alert("아이디를 입력해 주세요.");
    	$("#username").focus();
    	return;
    }
    
	$.ajax({
			async: true,
			url: "/Register/RegduplChk.do",
			type: "POST",
			data: {userId : userId},
			success : function(response){
				if(response == 0) {
					alert("사용 가능한 아이디입니다.");
					$("#username").css("color", "grey");
					$("#username").css("border-bottom", "1px solid lime");
					
					checkStep1 = 1;
				} else {
					alert("중복된 아이디입니다.");
				}
			},
			error : function(xhr, status, error) {
				if(xhr.status == 500){
					location.href = "/";
				} else {
					alert('오류가 발생했습니다.');
					return;
				}
			},
			beforeSend: function(xhr){
				xhr.setRequestHeader("AJAX", "true");
			}
		});
	}

function fn_nickCheck() {
    var userNick = $("#nickname").val();
    var leg = /^(?=.*[ㄱ-ㅎ가-힣a-zA-Z0-9]).{3,15}$/; 
    var leg1 = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/g;
    
    checkStep2 = 0;
    
    if(!leg.test(userNick) || userNick.length < 2) {
        alert("닉네임은 한글, 영문, 숫자 2자리 이상이어야 합니다.");
        return;
    } else if(leg1.test(userNick)) {
        alert("특수문자는 사용 불가합니다.");
        return;
    } 
    
    $.ajax({
        async: true,
        url: "/Register/RegduplNick.do",
        type: "POST",
        data: {userNick : userNick},
        success: function(response) {
            if(response == 0) {
                alert("사용할 수 있는 닉네임입니다.");
                $("#nickname").css("border-bottom", "1px solid lime");
                
                checkStep2 = 1;
            } else {
                alert("중복된 닉네임 입니다.");
                $("#nickname").val("");
            }
        },
        error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
    });
}

function fn_mailCheck() {
	var userEmail = $("#email").val();
	
	if(!emailCheck(userEmail)){
    	alert("이메일 형식에 맞게 작성해 주세요.");
        $("#email").focus();
        return false;
    }
	
    $("#loading-bar").show();
    $("#send-mail").css('display', 'none');

    $(document).ajaxStart(function() {
        $('html').css("cursor", "wait");
    });

    $(document).ajaxStop(function() {
        $('html').css("cursor", "auto");
    });

    $.ajax({
        url: "/Register/RegEmailChk.do",
        type: "POST",
        data: { userEmail: userEmail },
        dataType: "json",
        success: function(response) {
            if (response.status === "error") {
            	alert(response.message);
            	$("#send-mail").css("display","none");
            	
                return;
            } else if (response.status === "success") {
                alert("발송되었습니다.");
                $("#send-mail").css("display","block");
            }
        },
        error : function(xhr, status, error) {
			if(xhr.status == 500){
				$("#loading-bar").hide();
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		},
        complete: function() {
            $("#loading-bar").hide();
        }
    });
}

function fn_checkCertify(){
	var rnumber = $("#mail-check-input").val();
	
	checkStep3 = 0;
	
	$.ajax({
		url:"/Register/RegMailNumber.do",
		type: "POST",
		data: {rnumber : rnumber},
		success: function(response){
			if(response == 1){
				alert("인증되었습니다.");
				$("#mail-check-input").css("border-bottom", "1px solid lime");
				$("#email").css("border-bottom", "1px solid lime");
				
				checkStep3 = 1;
			} else {
				alert("인증 실패하였습니다.");
			}
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
}

function changeCheck(val){
	if(val == 1){
		checkStep1 = 0;
		validationCheck("username");
	} else if(val == 2){
		checkStep2 = 0;
	} else {
		checkStep3 = 0;
		validationCheck("email");
	}
}

function validationCheck(val){
	var retVal = $("#"+val).val().replace(/ /g, '');
	$("#"+val).val(retVal);
}

function emailCheck(email_address){     
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	if(!email_regex.test(email_address)){ 
		return false; 
	} else {
		return true;
	}
}

</script>
  
<body class="body">
	<div id="loading-bar" style="display: none;">
		<div class="spinner"></div>
		<p>이메일 발송 중입니다...</p>
	</div>
	<div class="background-wrap">
    	<div class="background"></div>
	</div>

	<div class="form">
	
    	<h1>회원가입</h1>
    	<br>
    	
    	<hr class="hr-5">
    	<div class="regInfo">
    		<ul>
    			<li class="rinfo_list">회원가입은 신청 및 GM 승인 이후 가입됩니다.</li>
    			<li class="rinfo_list">디스코드 참여는 필수이며 카페, 디스코드 닉네임과 홈페이지 닉네임 동일하게 기입해 주세요.</li>
    			<li class="rinfo_list">유저들 간 불화는 용납하지 않습니다.</li>
    			<li class="rinfo_list">1인 5클라이언트 제한입니다.</li>
    			<li class="rinfo_list">다중 클라, 매크로 및 유사 프로그램 사용 시 해당 계정 및 관련 데이터 삭제입니다.</li>
    			<li class="rinfo_list">현금거래 관련 발언은 자제 부탁드리겠습니다. 후원 문의는 GM에게 해주세요.</li>
    			<li class="rinfo_list">레드 코인 현금 거래는 금지입니다. 또한, 유저들 간의 현금거래 목격 시 거래하셨던 분들의 계정 모두 벤 처리됩니다.</li>
    		</ul>
    	</div>
    	<hr class="hr-5">
    	<br>
    	<div class="container">
	    	<span style="color: red; margin-bottom: 10%;">※ 필수기입항목</span>
	        	<span class="insert_info">아이디 <span style="color: red;">※ 신청한 아이디 뒤에 1~30숫자가 붙어 게임계정이 생성됩니다. <br> 예시) 홈피ID/PW : red / 1234 -> 인게임ID/PW : red1~30 / 1234 (가급적 숫자를 제외하고 생성해주세요.)</span></span>
	            <input class="sec-label" type="text" name="username" id="username" class="username" placeholder="영문,숫자 포함 4자리 이상" autocomplete="off" oninput="changeInput(this);" onchange="changeCheck('1');">
	            <input type="button" value="중복" id="duplicationChk" class="duplicationChk" onclick="fn_IdCheck();">
				<br>
				
	    	    <span class="insert_info">비밀번호 <span style="color: red;">※</span></span>
	       	    <input class="sec-label" type="password" name="password" id="password" class="password" placeholder="특수기호 포함 4자리 이상" autocomplete="off">
	            <br>
	
	         	<span class="insert_info">비밀번호 확인 <span style="color: red;">※</span></span>
	            <input class="sec-label" type="password" name="confirm_password" id="confirm_password" class="confirm_password" placeholder="비밀번호 확인" autocomplete="off">
	            <div id="error-message" class="error-message" style="color:red; font-size:12px;"></div>
	            <br>
	            
	        	<span class="insert_info">닉네임 <span style="color: red;">※ 홈페이지 닉네임은 디스코드 & 게임캐릭명 과 모두 동일하도록 생성해주세요.</span></span>
	            <input class="sec-label" type="text" name="nickname" id="nickname" placeholder="한글,영문,숫자 3자리 이상" autocomplete="off" onchange="changeCheck('2');">
	            <input type="button" value="중복" id="duplNickNmChk" class="duplNickNmChk" onclick="fn_nickCheck();">
				<br>
				
	        	<span class="insert_info">이메일 <span style="color: red;">※가입 시 이메일 인증 필수</span></span>
	            <input class="sec-label" type="email" name="email" id="email" class="email" placeholder="이메일 정확하게 기입해 주세요" autocomplete="off" onchange="changeCheck('3');">
	            <input type="button" value="발송" id="emailCertifyNumber" class="emailCertifyNumber" onclick="fn_mailCheck();">
	            
	            <div class="send-mail" id="send-mail" style="display:none;">
	            	<input type="text" id="mail-check-input" class="mail-check-input" placeholder="인증번호 4자리를 입력해주세요" maxlength="4">
	            	<input type="button" value="인증확인" id="checkCertify" class="checkCertify" onclick="fn_checkCertify();">
	            </div>
	            <br>
	            <div id="error-message" class="error-message">
	            </div>
		</div>
    	<p class="p-container">
        	<input type="button" class="signup" name="signup" id="signup" value="가입하기" onclick="doSubmit();">
   		</p>
	</div>
</body>
</html>
