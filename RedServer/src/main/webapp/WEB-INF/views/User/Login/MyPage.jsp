<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="html">
  <head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>  
    <meta name="viewport" content="width=device-width",initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="ie-edge"/>
    <meta name="keywords" content="Yulian Brito, Yulian, Brito, Frontend Developer, Sign Up Sign In 1"/>
    <meta name="author" content="Yulian Brito"/>
    <title>레드서버</title>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="/css/MyPage.css?ver=20240813" />
  </head>
  
<script>
var checkStep1 = 0;
var checkStep2 = 0;
var checkStep3 = 0;

function f_findExistingUser(){
	
	var userId = $("#findId").val();
	var userPw = $("#findPw").val();
	
	if (userId === "") {
        alert("계정을 입력해주세요.");
        $("#findId").focus();
        return false;
	}
	
	if (userPw === "") {
	    alert("비밀번호를 입력해주세요.");
	    $("#findId").focus();
	    return false;
	}
	
	var param = {
    		userId : userId,
    		userPw : userPw
		}
	
	var mappingUrl = "/Register/FindExistingUser.do";
	
	var returnAjax = doAjax(param, mappingUrl);
    
	returnAjax = decodeURI(returnAjax)
	
	if (returnAjax === 'unknown') {
		alert("계정이 존재하지 않습니다.");
		return;
	} else if (returnAjax === 'alreadySignedUp') {
		alert("이미 홈페이지에 계정이 존재합니다.");
		return;
	} else if (returnAjax === 'denied') {
		alert("차단된 계정입니다. GM대장에게 문의하세요.");
		return;
	} else if (returnAjax === 'success') {
		alert("계정이 확인되었습니다. 추가 정보를 입력해 주세요.");
        $("#nickname").focus();
        checkStep1 = 1;
	} else {
		alert("오류가 발생하였습니다.");
    }
}


function f_userReg(){
	
	var userId = $("#findId").val();
	var userPw = $("#findPw").val();
	var userEmail = $("#email").val();
	var userNick = $("#nickname").val();
	var rnumber = $("#mail-check-input").val();
	
	if(checkStep1 == 0){
		alert("계정을 조회해 주세요.");
        return;
	} else if(checkStep2 == 0){
		alert("닉네임을 조회해 주세요.");
        return;
	} else if(checkStep3 == 0){
		alert("이메일 인증을 해주세요.");
        return;
	}
	
	if (userId === "") {
        alert("계정을 입력해주세요.");
        $("#findId").focus();
        return false;
    }
    
    if (userPw === "") {
        alert("비밀번호를 입력해주세요.");
        $("#findPw").focus();
        return false;
    }
    
    if (userNick === "") {
        alert("닉네임을 입력해주세요.");
        $("#nickname").focus();
        return false;
    }
    
    if (userEmail === "") {
        alert("이메일을 입력해주세요.");
        $("#email").focus();
        return false;
    }
    
    if (rnumber === "") {
        alert("인증번호를 입력해주세요.");
        $("#mail-check-input").focus();
        return false;
    }
	
	var param = {
    		userId : userId,
    		userPw : userPw,
    		userNick : userNick,
    		rnumber : rnumber,
    		userEmail : userEmail,
    		existingUserYn : "Y"
		}
	
	
	var regSignUpUrl = "/Register/RegSignUp.do";
    var signUpResult = doAjax(param, regSignUpUrl);
    
    if (parseInt(signUpResult) > 0) {
        alert("가입이 완료되었습니다.");
        doSubmit("/Login/LoginMain.do");
    } else {
        alert("가입에 실패하였습니다.");
    }
		
}

function fn_mailCheck() {
	var userEmail = $("#email").val();
	
	if(!emailCheck(userEmail)){
    	alert("이메일 형식에 맞게 작성해 주세요.");
        $("#email").focus();
        return false;
    }
	
    $("#loading-bar").show();
    $("#send-mail").css("display","none");

    $(document).ajaxStart(function() {
        $('html').css("cursor", "wait");
    });

    $(document).ajaxStop(function() {
        $('html').css("cursor", "auto");
    });
    
    checkStep3 = 0;

    $.ajax({
        url: "/Register/RegEmailChk.do",
        type: "POST",
        data: { userEmail: userEmail },
        dataType: "json",
        success: function(response) {
            if (response.status === "error") {
                alert(response.message);
                $("#send-mail").css("display","none");
                
                return;
            } else if (response.status === "success") {
                alert("발송되었습니다.");
                $("#send-mail").css("display","block");
            }
        },
        error : function(xhr, status, error) {
			if(xhr.status == 500){
				$("#loading-bar").hide();
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		},
        complete: function() {
            $("#loading-bar").hide();
        }
    });
}

function fn_checkCertify(){
	var rnumber = $("#mail-check-input").val();
	
	$.ajax({
		url:"/Register/RegMailNumber.do",
		type: "POST",
		data: {rnumber : rnumber},
		success: function(response){
			if(response == 1){
				alert("인증되었습니다.");
				$("#mail-check-input").css("border-bottom", "1px solid lime");
				$("#email").css("border-bottom", "1px solid lime");
				checkStep3 = 1;
			} else {
				alert("인증 실패하였습니다.");
				checkStep3 = 0;
			}
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				checkStep3 = 0;
				
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
}

function fn_nickCheck() {
	
	var userNick = $("#nickname").val();
	var leg = /^(?=.*[ㄱ-ㅎ가-힣a-zA-Z0-9]).{3,15}$/; 
	var leg1 = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/g;
	
	if(!leg.test(userNick) || userNick.length < 3 || userNick == ""){
		alert("사용 가능한 닉네임입니다.");
		checkStep2 = 1;
		return;
	} else if(leg1.test(userNick)){
		alert("특수문자는 사용 불가합니다.");
		checkStep2 = 0;
		return;
	} 
	
	// 닉네임 중복 체크 
	$.ajax({
		async: true,
		url: "/Register/RegduplNick.do",
		type: "POST",
		data: {userNick : userNick},
		success: function(response){
			if(response == 0){
				alert("사용할 수 있는 닉네임입니다.");
				$("#nickname").css("border-bottom", "1px solid lime");
				checkStep2 = 1;
				return;
			} else {
				alert("중복된 닉네임 입니다.");
				$("#nickname").val("");
				checkStep2 = 0;
				return;
			}
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				checkStep2 = 0;
				
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
}


function doSubmit(str) {
	var form = $("#userRegist");

	form.attr("action", str);
	form.attr("method", "POST");

	form.submit();
}


function doAjax(param, mappingUrl) {
	var returnVal;
	$.ajax({
		type : "POST",
		async : false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	return returnVal;
}

function changeCheck(val){
	if(val == 1){
		checkStep1 = 0;
		validationCheck("findId");
	} else if(val == 2){
		checkStep2 = 0;
	} else {
		checkStep3 = 0;
		validationCheck("email");
	}
}

function validationCheck(val){
	var retVal = $("#"+val).val().replace(/ /g, '');
	$("#"+val).val(retVal);
}

function emailCheck(email_address){     
	var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	if(!email_regex.test(email_address)){ 
		return false; 
	} else {
		return true;
	}
}

</script>
  
<body class="body">
	<div id="loading-bar" style="display: none;">
		<div class="spinner"></div>
		<p>이메일 발송 중입니다...</p>
	</div>
	<article id="top" class="wrapper style1">
		<div class="container1">
			<div class="row">
				<div class="col-4 col-5-large col-12-medium">
					<span class="image fit"><img src="/imgs/MyPage/king_mo.jpg" alt="" /></span>
				</div>
				<div class="col-8 col-7-large col-12-medium h_text">
					<header>
						<h1><strong style="color:#ffffff;">기존 유저 웹계정 신청</strong></h1>
					</header>
				</div>
			</div>
		</div>
	</article>
	
	<form id="userRegist">
		<div class="container profile">
			<div class="row row1">
				<div id="profile-modify" class="pm col">
	 				<div class = "mp_list">
						<span class="mp">게임 계정</span>
						<span style="color: red;">※게임 계정 뒤에 자동 생성된 1~30 숫자를 제외하고 입력해주세요.</span>
					    <input class="sec-label mp_input" type="text" id="findId" onchange="changeCheck('1');">
					    <span class="mp">비밀 번호</span>
					    <input class="sec-label mp_input" type="password" id="findPw">
					    <input type="button" value="조회" id="findExistingUser" class="emailCertifyNumber" onclick="f_findExistingUser();">
					    <br>
					    <span class="mp">닉네임</span>
					    <input class="sec-label mp_input" type="text" id="nickname" onchange="changeCheck('2');">
					    <input type="button" value="중복" id="duplNickNmChk" class="duplNickNmChk" onclick="fn_nickCheck();">
					    <br>
					    <span class="mp">이메일</span>
            			<span style="color: black;">이메일 수신 동의 </span>
            			<span style="color: red;">※가입 시 이메일 인증 필수</span>
            			<input class="sec-label mp_input" type="text" id="email" onchange="changeCheck('3');">
            			<br>
					    <input type="button" value="인증" id="emailCertifyNumber" class="emailCertifyNumber" onclick="fn_mailCheck();">
	            		<div class="send-mail" id="send-mail" style="display:none;">
	            			<input type="text" id="mail-check-input" class="mail-check-input" placeholder="인증번호 4자리를 입력해주세요" maxlength="4">
	            			<input type="button" value="인증확인" id="checkCertify" class="checkCertify" onclick="fn_checkCertify();">
	            		</div>
					</div>
					    <input type="button" onclick="f_userReg();" value="가입하기" id="goReg" class="goReg">	
				</div>
				<div id="openReg" class="pm col-6">
				
				</div>
			</div>
		</div>
	</form>
	
	<!-- Footer -->
	<%-- <%@ include file="../../../views/Section/Footer.jsp"%> --%>
	<!-- pageEncoding 충돌로 인해 일단 강제 작업 -->
	<footer>
		<div class="container">
			<p>&copy; RED SERVER. All rights reserved.</p>
		</div>
	</footer>
</body>
</html>
