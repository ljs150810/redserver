<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="/img/cross-gate__1.jpg"/>
    <meta charset="EUC-KR">
    <title>관리자 페이지</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <link rel="stylesheet" href="/css/Admin.css?ver=20240506" />
</head>
<script>
    $(document).ready(function() {
    	lineChart();
    });
    function lineChart(){

    	var data = {

    	        labels: ["월","화","수","목","금","토","일"],

    	        datasets: [

    	            {

    	                label: "",

    	                fillColor: "rgba(151,187,205,0.2)",

    	                strokeColor: "rgba(151,187,205,1)",

    	                pointColor: "rgba(151,187,205,1)",

    	                pointStrokeColor: "#fff",

    	                pointHighlightFill: "#fff",

    	                pointHighlightStroke: "rgba(151,187,205,1)",

    	                data: [1,2,3,7,2,1,6]

    	            }

    	        ]

    	    };

    	    var ctx = document.getElementById("lineCanvas").getContext("2d");

    	    var options = { };

    	    var lineChart = new Chart(ctx).Line(data, options);

    }


</script>
<body>
    <form id="applyAc">
        <div class="container mt-4">
            <h1>사용자 계정 신청 관리</h1>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>사용자 ID</th>
                        <th>닉네임</th>
                        <th>이메일 주소</th>
                        <th>신청 날짜</th>
                        <th>상태</th>
                        <th>작업</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user" items="${applyUserList}">
                        <tr>
                            <td>${user.userId}</td>
                            <td>${user.userNick}</td>    
                            <td>${user.userEmail}</td>
                            <td>${user.regDate}</td>
                            <c:if test="${user.userRole == 0}">
                                <td>신청대기</td>
                            </c:if>
                            <td>
                                <input type="hidden" name="userId" value="${user.userId}" />
                                <button type="submit" name="action" value="approve" class="btn btn-success">승인</button>
                                <button type="submit" name="action" value="reject" class="btn btn-danger">거부</button>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <h1>신규 사용자 등록 통계</h1>
            <c:choose>
                <c:when test="${fn:length(dailyUserStats) > 0}">
                    <canvas id="lineCanvas" width="1000" height="410"></canvas>
                </c:when>
                <c:otherwise>
                    <p>신규 사용자 등록 통계 데이터가 없습니다.</p>
                </c:otherwise>
            </c:choose>

            <c:if test="${not empty errorMessage}">
                <div class="alert alert-danger mt-4">
                    <strong>오류:</strong> ${errorMessage}
                </div>
            </c:if>
        </div>
    </form>

</body>
</html>