<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="EUC-KR">
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<title>레드서버</title>
	<link rel="stylesheet" href="/css/Main.css?ver=20240922" />
</head>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
$(document).ready(function() {
    	
	goBoardPage();
	
    if (getCookie('hidePopup') === 'true') {
        $('#popup').hide();
    } else {
        $('#popup').show();
    }

    
});

function goBoardPage(){
    
    $('.boardCtgs').click(function() {
        // 선택된 행의 데이터 페이지 가져오기
        
        var boardName = $(this).data('page')
        
        var param = {
        	boardName : boardName
        	, grpSeq : "3"
        };
        var mappingUrl = "/Board/BoardPageProc.do"
        
        var returnAjax = doAjaxObject(param, mappingUrl);
        
        var sendParam = "";
        
        if(returnAjax.boardCode != null){
        	sendParam = "boardCode=" + returnAjax.boardCode;
        	
        	doSubmit("/Board/GoBoardProc.do?" + sendParam);
        } else {
        	alert('선택된 정보가 없습니다.');
        }
		
    });
};

function closePopup() {
    var hidePopupCheckbox = $('#hidePopupCheckbox');
    if (hidePopupCheckbox.prop('checked')) {
        setCookie('hidePopup', 'true', 1);
    }
    $('#popup').hide();
}

function setCookie(name, value, days) {
    var expires = '';
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

function getCookie(name) {
    var nameEQ = name + '=';
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(nameEQ) === 0) {
            return cookie.substring(nameEQ.length, cookie.length);
        }
    }
    return null;
}

function doSubmit(str) {
	var form = $("#mainPage");

	form.attr("action", str);
	form.attr("method", "POST");

	form.submit();
}

function doAjax(param, mappingUrl) {
	var returnVal;
	$.ajax({
		type : "POST",
		async : false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	return returnVal;
}

function doAjaxObject(param, mappingUrl) {
	var returnVal;
	$.ajax({
		type : "POST",
		async : false,
		url : mappingUrl,
		data : param,
		contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		traditional : true,
		success : function(res) {
			returnVal = res;
		},
		error : function(xhr, status, error) {
			if(xhr.status == 500){
				location.href = "/";
			} else {
				alert('오류가 발생했습니다.');
				return;
			}
		},
		beforeSend: function(xhr){
			xhr.setRequestHeader("AJAX", "true");
		}
	});
	return returnVal;
}

</script>
<body>
	<!-- Header -->
    <%@ include file="../../Section/User/Header.jsp"%>
     <!-- Main Content -->
    <form id="mainPage">
	    <main>
	        <div class="container">
	            <section class="categoriesTable">
	            	<table>
	            		<thead>
	            			<tr>
								<th colspan="2">RED Notice</th>
							</tr>
						</thead>
						<tbody>
							<tr id="notice" class="boardCtgs" data-page="notice">
								<td style="width: 20%; position: relative;">
								    <div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">공지사항</span>
	    							</div>
									<img src="/img/Concerto%20Gate%20concept%209%20(1).jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 공지사항</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 1}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
										서버상태 및 유저들에게 공지사항을 제공 합니다.
								</td>
							</tr>
							<tr id="plan" class="boardCtgs" data-page="plan">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">패치예정</span>
	    							</div>
									<img src="/img/modify123.jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 패치예정</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 2}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버의 발전을 위한 패치의 예정일 과 계획을 제공 합니다.
								</td>
							</tr>
							<tr id="patch" class="boardCtgs" data-page="patch">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">패치노트</span>
	    							</div>
									<img src="/img/73fe7e18f74f402421991d1702a7b109.jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 패치노트</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 3}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버의 과거부터 현재까지 진행된 패치의 내역들을 제공 합니다.
								</td>
							</tr>
							<tr id="event" class="boardCtgs" data-page="event">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">이벤트</span>
	    							</div>
									<img src="/img/Concerto%20Gate%20concept%209%20(2).jpg?ref_type=heads">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 이벤트</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 4}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버의 이벤트를 확인하세요.
								</td>
							</tr>
						</tbody>
	            	</table>
	            </section>
	            <div id="sideNavigation">
		        	<button class="btn" onclick="window.open('https://discord.gg/kxTDkvzbmp', '_blank')">
						<img src="/img/discordImg.jpg" alt="디스코드">
					</button>
					<button class="btn" onclick="window.open('https://cafe.naver.com/redongam', '_blank')">
						<img src="/img/naverImg.png" alt="네이버카페">
					</button>
	        	</div>
	            <section class="categoriesTable">
	                <table>
	            		<thead>
	            			<tr>
								<th colspan="2">RED Communication</th>
							</tr>
						</thead>
						<tbody>
							<tr id="auction" class="boardCtgs" data-page="auction">
								<td class="boardCtgs" style="width: 20%; position: relative;">
								    <div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">경매장</span>
	    							</div>
									<img src="/img/cross-gate%20(33).jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 경매장</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 5}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버 유저들간의 판매품목을 경매를 통해 진행할 수 있습니다.
								</td>
							</tr>
							<tr id="trade" class="boardCtgs" data-page="trade">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">레드장터</span>
	    							</div>
									<img src="/img/cross-gate%20(30).jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 거래게시판</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 6}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버 유저들간의 거래를 위한 거래 게시판 입니다.
								</td>
							</tr>
							<tr id="communication" class="boardCtgs" data-page="communication">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">자유게시판</span>
	    							</div>
									<img src="/img/cross-gate%20(35).jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 자유게시판</span>
									<c:if test="${boardCount != null}">
										<c:forEach var="board" items="${boardCount}">
											<c:if test="${board.boardCode == 7}">
												<span class="boardCount">(${board.boardCount})</span>
											</c:if>
										</c:forEach>
									</c:if>
									<br>
									레드서버의 유저들간의 소통의 장을 위한 공간 입니다.
								</td>
							</tr>
							<tr id="information" class="boardCtgs" data-page="information">
								<td style="width: 20%; position: relative;">
									<div style="position: absolute; top: 35px; left: 15px; transform: translateY(-100%);">
	        							<span style="font-weight: bold;color: white; text-shadow: -1px -1px 0 black, 1px -1px 0 black, -1px 1px 0 black, 1px 1px 0 black;">정보게시판</span>
	    							</div>
									<img src="/img/ec5b59293d12c193525ee530b03cc9df.jpg">
								</td>
								<td>
									<span style="font-weight: bold;">레드서버 정보게시판</span>
									<c:if test="${boardCount != null}">
									<c:forEach var="board" items="${boardCount}">
										<c:if test="${board.boardCode == 8}">
											<span class="boardCount">(${board.boardCount})</span>
										</c:if>
									</c:forEach>
									</c:if>
									<br>
									레드서버 또는 크로스게이트의 정보를 업로드 할 수 있습니다.
								</td>
							</tr>
						</tbody>
	            	</table>
	            </section>
	
	
	            <!-- <section class="featured">
	                <h2>주요 콘텐츠</h2>
	            </section> -->
	        </div>
	        <div id="popup" class="popup">
    			<div class="popup-content">
        			<img src="/img/popUp.jpg?ref_type=heads">
        			<label><input type="checkbox" id="hidePopupCheckbox"> 하루동안 보지않기</label>
        			<span class="close" onclick="closePopup()">닫기</span>
    			</div>
			</div>
	    <!-- Footer -->
	        <%@ include file="../../Section/User/Footer.jsp"%>
	    </main>
    </form>
</body>

</html>