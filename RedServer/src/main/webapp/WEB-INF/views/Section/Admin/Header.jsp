<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="EUC-KR">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Red Server</title>
    
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="/css/bootStrap/sb-admin-2.min.css?ver=202407300001" rel="stylesheet">
    <link href="/css/bootStrap/all.min.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/Login/goMainPage.do">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Red Server</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="/Admin/Join.do">
                    <i class="fas fa-fw fa-table"></i>
                    <span>계정신청</span></a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" href="/Admin/UserInfo.do">
                    <i class="fas fa-fw fa-table"></i>
                    <span>유저 정보</span></a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" href="/Admin/UserMain.do">
                    <i class="fas fa-fw fa-table"></i>
                    <span>사용자 메인페이지</span>
				</a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">${userNick}</span>
                                <img class="img-profile rounded-circle"
                                    src="/img/bootStrap/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
		                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
		                        aria-labelledby="userDropdown">
		                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
		                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
		                            Logout
		                        </a>
		                    </div>
                        </li>
                    </ul>
                </nav>
                <!-- End of Topbar -->