<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="/img/cross-gate__1.jpg"/>
	<link rel="apple-touch-icon" href="/img/cross-gate__1.jpg"/>
	<meta charset="EUC-KR">
	<title>Insert title here</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css">
	<link rel="stylesheet" href="/css/Header.css?ver=20241110" />
</head>
<script>

window.onload = function() {
	updateKoreanDateTime();
	setInterval(updateKoreanDateTime, 1000);
	acSelect();
	goMain();
    slideUser();
    setInterval(slideUser, 3000);
};

window.onclick = function(event) {
    var modals = document.getElementsByClassName('modal');
    for (var i = 0; i < modals.length; i++) {
        if (event.target == modals[i]) {
            modals[i].style.display = "none";
        }
    }
}

function openMessagePopup() {
    window.open('/Login/MessageBoxProc.do', 'messagePopup', 'width=600,height=400,scrollbars=yes');
}

var newUserList = nullCheck(${getNewUserJson});

if (!newUserList || newUserList.length === 0) {
    newUserList = [{ userNick: "아쉽지만.. 오늘은 신규가입자가 없습니다" }];
}

var index = 0;
function slideUser() {
    $('#newUserSlide').fadeOut(400, function() {
        $(this).text("[" + newUserList[index].userNick + "]");
        $(this).fadeIn(400);
        index = (index + 1) % newUserList.length;
    });
}

function goMain(){
	$('#mainPart').click(function(){
		doSubmit("/Login/goMainPage.do");
	})
}

function acSelect(){
	$('#acSelectBox').change(function() {
		
		var accountId = $(this).val();
		
		$('#accountId').val(accountId);
		
		doSubmit("/Login/UserAcProc.do")
		
	}); 
} 

function updateKoreanDateTime() {
	var options = {
	        timeZone: "Asia/Seoul",
	        day: "numeric",
	        month: "numeric",
	        year: "numeric",
	        hour: "numeric",
	        minute: "numeric",
	        second: "numeric",
	        weekday: "short"
	    };
    var now = new Date();
    var koreanDateTime = now.toLocaleString("ko-KR", options);

    var hour = now.getHours();
    var minute = now.getMinutes();

    var gameTime;
    if ((hour % 2 === 0 && minute >= 48) || (hour % 2 !== 0 && minute < 2)) {
        gameTime = "새벽";
    } else if (hour % 2 !== 0 && minute >= 2 && minute < 47) {
        gameTime = "낮";
    } else if (hour % 2 !== 0 && minute >= 47) {
        gameTime = "저녁";
    } else if (hour % 2 === 0 && minute >= 2 && minute < 47) {
        gameTime = "밤";
    }

    document.getElementById("korean-date-time").innerText = "현재 시간: " + koreanDateTime + " [ 게임 시간: " + gameTime + " ]";
}

setInterval(updateKoreanDateTime, 1000);



function doSubmit(str) {
	var form = $("#header");

	form.attr("action", str);
	form.attr("method", "POST");
	
	form.submit();
}

function m_func(){
	var mappingUrl = "/Login/MyPageProc.do";
	doSubmit(mappingUrl);
}


function openModal(modalId) {
    document.getElementById(modalId).style.display = "block";
}

function closeModal(modalId) {
    document.getElementById(modalId).style.display = "none";
}

function nullCheck(value){
	var retVal = 0;
	if(value != ''){
		retVal = value;
	}
	
	return retVal;
}


</script>
<body>
<form id="header">
	<header>
		<div class="container">
			<nav class="headerNav">
				<div class="korean-date-time" id="korean-date-time"></div>
				<ul>
					<c:if test="${user == null}">
						<li><a href="/Login/LoginMain.do">로그인</a></li>
					</c:if>
					<c:if test="${user != null}">
						<!-- <li><img class="infoModifyImg" src="https://redmine.icared.co.kr/attachments/thumbnail/513"><a onclick="m_func()" href="#">내 정보 수정</a></li> -->
						<!-- <li><img class="infoModifyImg" src="/img/pngegg.png"><a href="#" onclick="openMessagePopup()">메시지</a></li> -->
						<li><img class="infoModifyImg" src="https://cdn-icons-png.flaticon.com/512/660/660350.png"><a href="/Login/LoginMain.do">로그아웃</a></li>
					</c:if>
				</ul>
			</nav>
		</div>
	</header>

	<!-- Main Content -->
	<main>
		<input type="hidden" id="accountId" name="accountId" />
		<div class="container">

			<section class="mainPart" id="mainPart">
				<img src="/img/W16304851253505376modify.jpg">
			</section>
			
			<section class="allCount">
				<select id="acSelectBox" class="select">
					<option disabled selected>게임 계정</option>
					<c:forEach var="userList" items="${user}" varStatus="stat">
						<option value="${userList.accountId}">${userList.accountId}</option>
					</c:forEach>
				</select>
				<span class="icoArrow"><img src="https://freepikpsd.com/media/2019/10/down-arrow-icon-png-7-Transparent-Images.png" alt=""></span>
				<span>총 가입자수 : ${userTotalCount}명 &nbsp;&nbsp; 신규 가입자 : &nbsp;<strong id="newUserSlide"></strong></span>
			</section>

			<section class="serverInfoList">
				<ul>
					<li><a href="#" onclick="openModal('gameIntroModal')">게임 소개</a></li>
	                <li><a href="#" onclick="openModal('downloadModal')">|&nbsp;&nbsp;레드서버 다운로드</a></li>
	                <li><a href="#" onclick="openModal('infoModal')">|&nbsp;&nbsp;정보 제공 퀵서치</a></li>
				</ul>
			</section>
			<!-- <section class="searchPart">
				<input type="text" class="search">
				<input type="button" id="headerSearch" class="searchBtn" value="검색" />
			</section> -->
				<div id="sideNavigation">
		        	<button class="btn" onclick="window.open('https://discord.gg/kxTDkvzbmp', '_blank')" title="레드서버 디스코드">
						<img src="/img/discordImg.jpg" alt="디스코드">
					</button>
					<button class="btn" onclick="window.open('https://cafe.naver.com/redongam', '_blank')" title="레드서버 카페">
						<img src="/img/naverImg.png" alt="네이버카페">
					</button>
					<button class="btn" onclick="window.open('https://cafe.naver.com/redongam/20', '_blank')" title="레드서버 사냥터">
						<img src="/img/battle.png" alt="사냥터정보">
					</button>
					<button class="btn" onclick="window.open('https://iita71737.github.io/BluecgPetCal/index.html', '_blank')" title="수마성장 게산기">
						<img src="/img/calImg.png" alt="펫성장계산기">
					</button>
	        	</div>
		</div>
	</main>
</form>

	<!-- MODAL START -->
	<div id="gameIntroModal" class="modal">
	    <div class="modal-content">
	    	<div class="modal-title">
	        	<span class="closeModal" onclick="closeModal('gameIntroModal')">&times;</span>
	        	<h1>레드서버 소개</h1>
	        </div>
	        <hr class="hr-5">
	        <p style="color: blue;">경험치 배율</p>
	        <p>레벨 1~40 : 5.3배</p>
	        <p>레벨 41~70 : 4.3배</p>
	        <p>레벨 71~114 : 3.3배</p>
	        <p>레벨 115~142 : 2.3배</p>
	        <p>레벨 142~150 : 2.0배</p>
	        <hr class="hr-5">
	        <p style="color: blue;">경험치 이벤트</p>
	        <p style="font-weight: bold;">이제 막 시작할 생각에 렙업이 막막하시다구요?</p>
	        <p>걱정 마세요! 쉽게 올릴 수 있도록 주기적인 경험치 이벤트</p>
	        <p>초보자 경험치 상승과 빵빵한 장비 지원! 상시 진행 중!</p>
	        <p>(친절한 고인물들 다수 존재!!)</p>
	        <hr class="hr-5">
	        <p style="color: blue;">통합된 직인 직업</p>
	        <p>중갑 무기 직인과 경갑 무기 직인</p>
	        <p>혼자서 할 수 있는 4차 랭크업까지</p>
	        <p>직인 경험치 대폭 하강 채취할땐 부상 방지 모자와 함께!</p>
	        <hr class="hr-5">
	        <p style="color: blue;">명성 시스템 철폐</p>
	        <p>레벨업 하는 데에 제약이 걸리는 명성 시스템을 없앴습니다!</p>
	        <p>자신이 크로스 게이트를 열심히 할 수 있다면</p>
	        <p>강제적인 제약 없이 폭풍 성장 가능합니다!</p>
	        <hr class="hr-5">
	        <p style="color: blue;">최신 컨테츠 개발 과 소통하는 GM 들</p>
	        <p>파티원들과 함께하는 레이드 보스전 과 듀얼대회</p>
	        <p>국내 섭 최고를 향한 최신 컨텐츠 및 기능구현</p>
	        <p>레드서버 GM들 일동 서버 운영 밸런스를 고려하여 여러분들의</p>
	        <p>요구 사항들을 적절히 반영하여 운영합니다.</p>
	    </div>
	</div>
	
	<div id="downloadModal" class="modal">
	    <div class="modal-content">
	    	<div class="modal-title">
	        	<span class="closeModal" onclick="closeModal('downloadModal')">&times;</span>
	        	<h1>레드서버 다운로드</h1>
	        </div>
	        <hr class="hr-5">
	        <p style="color: red;">※다운로드 시 주의사항</p>
	        <p>windows 보안 -> 바이러스 및 위협 방지 -> 설정관리 -> 제외 ( 제외추가 또는 제거)에서</p>
			<p>하나의 새폴더를 지정하신후 그폴더에 크로스게이트 클라를 압축풀어주세요!</p>
			<p>압축풀기 후 클라이언트 내 존재하는 gacg.exe파일을 관리자권한으로</p>
			<p>1회 실행한뒤 런쳐를 실행해주세요.</p>
			<hr class="hr-5">
			<p>클라이언트 <a href="https://drive.google.com/file/d/1rz1yh6ZKhvTnNjtkkgaf5lBQPf3it8l9/view?usp=sharing" target="_blank">다운로드</a></p>
	        <hr class="hr-5">
	        <p style="color: red;">※실패 시 해결방법</p>
	        <p>해결방법 <a href="https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=nidboss1&logNo=221204837909" target="_blank">이동하기</a></p>
	    </div>
	</div>
	
	<div id="infoModal" class="modal">
	    <div class="modal-content">
	        <div class="modal-title">
		        <span class="closeModal" onclick="closeModal('infoModal')">&times;</span>
		        <h2>웹사이트</h2>
	        </div>
	        <hr class="hr-5">
	        <p>레드서버 디스코드<a href="https://discord.gg/kxTDkvzbmp" target="_blank">이동하기</a></p>
	        <p>레드서버 카페 <a href="https://cafe.naver.com/redongam" target="_blank">이동하기</a></p>
	        <p>수마성장 계산기 <a href="https://iita71737.github.io/BluecgPetCal/index.html" target="_blank">이동하기</a></p>
	        <p>블루서버 카페 <a href="https://cafe.naver.com/bluecrossgate" target="_blank">이동하기</a></p>
	        <p>케렌시아 카페<a href="https://cafe.naver.com/silver80" target="_blank">이동하기</a></p>
	        <p>왕댜 <a href="http://cross.woobi.co.kr/index.htm" target="_blank">이동하기</a></p>
	        <p>아이블루 <a href="https://www.ibluecg.com/" target="_blank">이동하기</a></p>
	        <p>크로스게이트 온감 <a href="http://ongam.com/crossgate" target="_blank">이동하기</a></p>
	        <p>블루 영치기님 블로그 <a href="https://youngchigi588.blogspot.com/" target="_blank">이동하기</a></p>
	        <p>부리또님 블로그 <a href="https://m.blog.naver.com/PostList.naver?blogId=pqpqpq99&tab=1" target="_blank">이동하기</a></p>
	        <p>대만 정보 사이트① <a href="https://cg.skyey.tw/" target="_blank">이동하기</a></p>
	        <p>대만 정보 사이트② <a href="https://cgsword.com/" target="_blank">이동하기</a></p>
	        <p>대만 정보 사이트③ <a href="https://forum.gamer.com.tw/A.php?bsn=2577" target="_blank">이동하기</a></p>
	        
	        <h2 style="margin-top: 20px;">재료</h2>
	        <hr class="hr-5">
	        <p>광석 <a href="https://cafe.naver.com/guildan/413" target="_blank">이동하기</a></p>
	        <p>수렵 <a href="https://cafe.naver.com/guildan/416" target="_blank">이동하기</a></p>
	        <p>목재 <a href="https://cafe.naver.com/guildan/417" target="_blank">이동하기</a></p>
	        <p>허브 <a href="https://cafe.naver.com/guildan/419" target="_blank">이동하기</a></p>
	        <p>옷감 <a href="https://cafe.naver.com/guildan/420" target="_blank">이동하기</a></p>
	        <p>보석 <a href="https://cafe.naver.com/common/storyphoto/viewer.html?src=https%3A%2F%2Fcafefiles.pstatic.net%2FMjAxOTA1MTFfMjMy%2FMDAxNTU3NTE3NjEzNjUz.E8genDSOvonPcspsrhiQTLgS9ssC_TAvmyTcNT9-PoEg.m4bfvKTDdLtpgiKakm-rQ1p6Jrj6CDGMUUU8J1zfLyAg.JPEG.melo9999%2F%25EB%25B3%25B4%25EC%2584%259D.jpg" target="_blank">이동하기</a></p>
	        
	    	<h2 style="margin-top: 20px;">수마 정보</h2>
	        <hr class="hr-5">
	        <p>수마 정보 <a href="https://www.ibluecg.com/pet/?id=1" target="_blank">이동하기</a></p>
	        <p>수마 계산기 <a href="http://qo3op.asuscomm.com/bluecgpet/PetCalc.html" target="_blank">이동하기</a></p>
	        
	        <h2 style="margin-top: 20px;">세계 지도</h2>
	        <hr class="hr-5">
	        <p>프레이아 <a href="https://blog.naver.com/storyphoto/viewer.jsp?src=https%3A%2F%2Fblogfiles.pstatic.net%2F20130124_139%2Fooreu0000_135901193555765tGE_PNG%2F%25C7%25C1%25B7%25B9%25C0%25CC%25BE%25C6_%25C3%25A4%25C3%25EB.png" target="_blank">이동하기</a></p>
	        <p>세레네 <a href="https://blog.naver.com/storyphoto/viewer.jsp?src=https%3A%2F%2Fblogfiles.pstatic.net%2F20130124_127%2Fooreu0000_1359012537378ULzQM_PNG%2F%25BC%25BC%25B7%25B9%25B3%25D7.png" target="_blank">이동하기</a></p>
	        <p>미네갈 <a href="https://blog.naver.com/storyphoto/viewer.jsp?src=https%3A%2F%2Fblogfiles.pstatic.net%2F20130124_230%2Fooreu0000_1359012857421KyhRs_PNG%2F%25B9%25CC%25B3%25D7%25B0%25A5.png" target="_blank">이동하기</a></p>
	        <p>솔키아 <a href="https://blog.naver.com/storyphoto/viewer.jsp?src=https%3A%2F%2Fblogfiles.pstatic.net%2F20130124_219%2Fooreu0000_1359012349693ciJUa_PNG%2F%25BC%25D6%25C5%25B0%25BE%25C6.png" target="_blank">이동하기</a></p>
	        <p>쿠르크스 <a href="https://blog.naver.com/storyphoto/viewer.jsp?src=https%3A%2F%2Fblogfiles.pstatic.net%2F20130124_8%2Fooreu0000_13590127977439stzU_PNG%2F%25C5%25A9%25B7%25E7%25C5%25A9%25BD%25BA_%25C3%25A4%25C3%25EB.png" target="_blank">이동하기</a></p>
	    </div>
	</div>
	<!-- MODAL END -->

</body>

</html>