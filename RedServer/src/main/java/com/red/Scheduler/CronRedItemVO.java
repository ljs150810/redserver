package com.red.Scheduler;

public class CronRedItemVO {
	
	private int itemId;
	private String itemTrueName;
	private String itemSaveIspoolItem;
	private int itemBaseImageNumber;
	private int itemCost;
	private int itemType;
	private int itemRemain;
	private int itemDurability;
	private int itemMaxdurability;
	private int itemModifyAttack;
	private int itemModifyDefence;
	private int itemModifyAgility;
	private int itemModifyRecovery;
	private int itemModifyMagic;
	private int itemModifyCritical;
	private int itemModifyCounter;
	private int itemModifyHitrate;
	private int itemModifyAvoid;
	private int itemModifyHp;
	private int itemModifyForcePoint;
	private int itemPoison;
	private int itemSleep;
	private int itemStone;
	private int itemDrunk;
	private int itemConfusion;
	private int itemAmnesia;
	private int itemLevel;
	private String cdKey;
	private int registNumber;
	private int itemdataplacenumber;
	private String itemFirstname;
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemTrueName() {
		return itemTrueName;
	}
	public void setItemTrueName(String itemTrueName) {
		this.itemTrueName = itemTrueName;
	}
	public int getItemBaseImageNumber() {
		return itemBaseImageNumber;
	}
	public void setItemBaseImageNumber(int itemBaseImageNumber) {
		this.itemBaseImageNumber = itemBaseImageNumber;
	}
	public int getItemCost() {
		return itemCost;
	}
	public void setItemCost(int itemCost) {
		this.itemCost = itemCost;
	}
	public int getItemType() {
		return itemType;
	}
	public void setItemType(int itemType) {
		this.itemType = itemType;
	}
	public int getItemRemain() {
		return itemRemain;
	}
	public void setItemRemain(int itemRemain) {
		this.itemRemain = itemRemain;
	}
	public int getItemDurability() {
		return itemDurability;
	}
	public void setItemDurability(int itemDurability) {
		this.itemDurability = itemDurability;
	}
	public int getItemMaxdurability() {
		return itemMaxdurability;
	}
	public void setItemMaxdurability(int itemMaxdurability) {
		this.itemMaxdurability = itemMaxdurability;
	}
	public int getItemModifyAttack() {
		return itemModifyAttack;
	}
	public void setItemModifyAttack(int itemModifyAttack) {
		this.itemModifyAttack = itemModifyAttack;
	}
	public int getItemModifyDefence() {
		return itemModifyDefence;
	}
	public void setItemModifyDefence(int itemModifyDefence) {
		this.itemModifyDefence = itemModifyDefence;
	}
	public int getItemModifyAgility() {
		return itemModifyAgility;
	}
	public void setItemModifyAgility(int itemModifyAgility) {
		this.itemModifyAgility = itemModifyAgility;
	}
	public int getItemModifyRecovery() {
		return itemModifyRecovery;
	}
	public void setItemModifyRecovery(int itemModifyRecovery) {
		this.itemModifyRecovery = itemModifyRecovery;
	}
	public int getItemModifyMagic() {
		return itemModifyMagic;
	}
	public void setItemModifyMagic(int itemModifyMagic) {
		this.itemModifyMagic = itemModifyMagic;
	}
	public int getItemModifyCritical() {
		return itemModifyCritical;
	}
	public void setItemModifyCritical(int itemModifyCritical) {
		this.itemModifyCritical = itemModifyCritical;
	}
	public int getItemModifyCounter() {
		return itemModifyCounter;
	}
	public void setItemModifyCounter(int itemModifyCounter) {
		this.itemModifyCounter = itemModifyCounter;
	}
	public int getItemModifyHitrate() {
		return itemModifyHitrate;
	}
	public void setItemModifyHitrate(int itemModifyHitrate) {
		this.itemModifyHitrate = itemModifyHitrate;
	}
	public int getItemModifyAvoid() {
		return itemModifyAvoid;
	}
	public void setItemModifyAvoid(int itemModifyAvoid) {
		this.itemModifyAvoid = itemModifyAvoid;
	}
	public int getItemModifyHp() {
		return itemModifyHp;
	}
	public void setItemModifyHp(int itemModifyHp) {
		this.itemModifyHp = itemModifyHp;
	}
	public int getItemModifyForcePoint() {
		return itemModifyForcePoint;
	}
	public void setItemModifyForcePoint(int itemModifyForcePoint) {
		this.itemModifyForcePoint = itemModifyForcePoint;
	}
	public int getItemPoison() {
		return itemPoison;
	}
	public void setItemPoison(int itemPoison) {
		this.itemPoison = itemPoison;
	}
	public int getItemSleep() {
		return itemSleep;
	}
	public void setItemSleep(int itemSleep) {
		this.itemSleep = itemSleep;
	}
	public int getItemStone() {
		return itemStone;
	}
	public void setItemStone(int itemStone) {
		this.itemStone = itemStone;
	}
	public int getItemDrunk() {
		return itemDrunk;
	}
	public void setItemDrunk(int itemDrunk) {
		this.itemDrunk = itemDrunk;
	}
	public int getItemConfusion() {
		return itemConfusion;
	}
	public void setItemConfusion(int itemConfusion) {
		this.itemConfusion = itemConfusion;
	}
	public int getItemAmnesia() {
		return itemAmnesia;
	}
	public void setItemAmnesia(int itemAmnesia) {
		this.itemAmnesia = itemAmnesia;
	}
	public int getItemLevel() {
		return itemLevel;
	}
	public void setItemLevel(int itemLevel) {
		this.itemLevel = itemLevel;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	public int getRegistNumber() {
		return registNumber;
	}
	public void setRegistNumber(int registNumber) {
		this.registNumber = registNumber;
	}
	public String getItemSaveIspoolItem() {
		return itemSaveIspoolItem;
	}
	public void setItemSaveIspoolItem(String itemSaveIspoolItem) {
		this.itemSaveIspoolItem = itemSaveIspoolItem;
	}
	public int getItemdataplacenumber() {
		return itemdataplacenumber;
	}
	public void setItemdataplacenumber(int itemdataplacenumber) {
		this.itemdataplacenumber = itemdataplacenumber;
	}
	public String getItemFirstname() {
		return itemFirstname;
	}
	public void setItemFirstname(String itemFirstname) {
		this.itemFirstname = itemFirstname;
	}

}
