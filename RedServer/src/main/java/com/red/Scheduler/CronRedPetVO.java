package com.red.Scheduler;

public class CronRedPetVO {
	
	private int unk1;
	private int baseImgnum;
	private int basebaseimgnum;
	private int lEVEL;
	private int hP;
	private int mP;
	private int vital;
	private int str;
	private int tgh;
	private int quick;
	private int migic;
	private int race;
	private int attribFire;
	private int attribWind;
	private int attribEarth;
	private int attribWater;
	private int eXP;
	private int bounsPoint;
	private int putPetTime;
	private String petName;
	private String userPetName;
	private String unk95;
	private String cdKey;
	private String ownerName;
	private int unk98;
	private String ownerCdKey;
	private int registNumber;
	private int position;
	private String uuid;
	
	public int getUnk1() {
		return unk1;
	}
	public void setUnk1(int unk1) {
		this.unk1 = unk1;
	}
	public int getBaseImgnum() {
		return baseImgnum;
	}
	public void setBaseImgnum(int baseImgnum) {
		this.baseImgnum = baseImgnum;
	}
	public int getBasebaseimgnum() {
		return basebaseimgnum;
	}
	public void setBasebaseimgnum(int basebaseimgnum) {
		this.basebaseimgnum = basebaseimgnum;
	}
	public int getlEVEL() {
		return lEVEL;
	}
	public void setlEVEL(int lEVEL) {
		this.lEVEL = lEVEL;
	}
	public int gethP() {
		return hP;
	}
	public void sethP(int hP) {
		this.hP = hP;
	}
	public int getmP() {
		return mP;
	}
	public void setmP(int mP) {
		this.mP = mP;
	}
	public int getVital() {
		return vital;
	}
	public void setVital(int vital) {
		this.vital = vital;
	}
	public int getStr() {
		return str;
	}
	public void setStr(int str) {
		this.str = str;
	}
	public int getTgh() {
		return tgh;
	}
	public void setTgh(int tgh) {
		this.tgh = tgh;
	}
	public int getQuick() {
		return quick;
	}
	public void setQuick(int quick) {
		this.quick = quick;
	}
	public int getMigic() {
		return migic;
	}
	public void setMigic(int migic) {
		this.migic = migic;
	}
	public int getRace() {
		return race;
	}
	public void setRace(int race) {
		this.race = race;
	}
	public int getAttribFire() {
		return attribFire;
	}
	public void setAttribFire(int attribFire) {
		this.attribFire = attribFire;
	}
	public int getAttribWind() {
		return attribWind;
	}
	public void setAttribWind(int attribWind) {
		this.attribWind = attribWind;
	}
	public int getAttribEarth() {
		return attribEarth;
	}
	public void setAttribEarth(int attribEarth) {
		this.attribEarth = attribEarth;
	}
	public int getAttribWater() {
		return attribWater;
	}
	public void setAttribWater(int attribWater) {
		this.attribWater = attribWater;
	}
	public int geteXP() {
		return eXP;
	}
	public void seteXP(int eXP) {
		this.eXP = eXP;
	}
	public int getBounsPoint() {
		return bounsPoint;
	}
	public void setBounsPoint(int bounsPoint) {
		this.bounsPoint = bounsPoint;
	}
	public int getPutPetTime() {
		return putPetTime;
	}
	public void setPutPetTime(int putPetTime) {
		this.putPetTime = putPetTime;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public String getUserPetName() {
		return userPetName;
	}
	public void setUserPetName(String userPetName) {
		this.userPetName = userPetName;
	}
	public String getUnk95() {
		return unk95;
	}
	public void setUnk95(String unk95) {
		this.unk95 = unk95;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public int getUnk98() {
		return unk98;
	}
	public void setUnk98(int unk98) {
		this.unk98 = unk98;
	}
	public String getOwnerCdKey() {
		return ownerCdKey;
	}
	public void setOwnerCdKey(String ownerCdKey) {
		this.ownerCdKey = ownerCdKey;
	}
	public int getRegistNumber() {
		return registNumber;
	}
	public void setRegistNumber(int registNumber) {
		this.registNumber = registNumber;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
