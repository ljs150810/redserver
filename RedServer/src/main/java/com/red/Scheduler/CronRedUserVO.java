package com.red.Scheduler;

public class CronRedUserVO {
	
	private String accountID;
	private String accountPassword;
	private int enableFlg;
	private int trialFlg;
	private int downFlg;
	private int expFlg;
	private int sequenceNumber;
	private int useFlg;
	private int badMsg;
	private String cdKey;
	
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public String getAccountPassword() {
		return accountPassword;
	}
	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}
	public int getEnableFlg() {
		return enableFlg;
	}
	public void setEnableFlg(int enableFlg) {
		this.enableFlg = enableFlg;
	}
	public int getTrialFlg() {
		return trialFlg;
	}
	public void setTrialFlg(int trialFlg) {
		this.trialFlg = trialFlg;
	}
	public int getDownFlg() {
		return downFlg;
	}
	public void setDownFlg(int downFlg) {
		this.downFlg = downFlg;
	}
	public int getExpFlg() {
		return expFlg;
	}
	public void setExpFlg(int expFlg) {
		this.expFlg = expFlg;
	}
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public int getUseFlg() {
		return useFlg;
	}
	public void setUseFlg(int useFlg) {
		this.useFlg = useFlg;
	}
	public int getBadMsg() {
		return badMsg;
	}
	public void setBadMsg(int badMsg) {
		this.badMsg = badMsg;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	
	

}
