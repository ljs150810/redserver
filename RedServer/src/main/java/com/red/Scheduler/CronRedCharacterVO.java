package com.red.Scheduler;

public class CronRedCharacterVO {
	
	private int whichType;
	private int registNumber;
	private int baseImageNumber;
	private int originalImageNumber;
	private int mapId;
	private String name;
	private String mainJob;					
	private int nameColor;
	private int jobRank;
	private int jobAncesry;
	private int gold;
	private int loginCount;
	private int lv;
	private int hp;
	private int forcePoint;
	private int vital;
	private int str;
	private int tough;
	private int quick;
	private int magic;
	private int duelPoint;
	private String cdkey;
	
	public int getWhichType() {
		return whichType;
	}
	public void setWhichType(int whichType) {
		this.whichType = whichType;
	}
	public int getRegistNumber() {
		return registNumber;
	}
	public void setRegistNumber(int registNumber) {
		this.registNumber = registNumber;
	}
	public int getBaseImageNumber() {
		return baseImageNumber;
	}
	public void setBaseImageNumber(int baseImageNumber) {
		this.baseImageNumber = baseImageNumber;
	}
	public int getOriginalImageNumber() {
		return originalImageNumber;
	}
	public void setOriginalImageNumber(int originalImageNumber) {
		this.originalImageNumber = originalImageNumber;
	}
	public int getMapId() {
		return mapId;
	}
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNameColor() {
		return nameColor;
	}
	public void setNameColor(int nameColor) {
		this.nameColor = nameColor;
	}
	public int getJobRank() {
		return jobRank;
	}
	public void setJobRank(int jobRank) {
		this.jobRank = jobRank;
	}
	public int getJobAncesry() {
		return jobAncesry;
	}
	public void setJobAncesry(int jobAncesry) {
		this.jobAncesry = jobAncesry;
	}
	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getForcePoint() {
		return forcePoint;
	}
	public void setForcePoint(int forcePoint) {
		this.forcePoint = forcePoint;
	}
	public int getVital() {
		return vital;
	}
	public void setVital(int vital) {
		this.vital = vital;
	}
	public int getStr() {
		return str;
	}
	public void setStr(int str) {
		this.str = str;
	}
	public int getTough() {
		return tough;
	}
	public void setTough(int tough) {
		this.tough = tough;
	}
	public int getQuick() {
		return quick;
	}
	public void setQuick(int quick) {
		this.quick = quick;
	}
	public int getMagic() {
		return magic;
	}
	public void setMagic(int magic) {
		this.magic = magic;
	}
	public int getDuelPoint() {
		return duelPoint;
	}
	public void setDuelPoint(int duelPoint) {
		this.duelPoint = duelPoint;
	}
	public String getCdkey() {
		return cdkey;
	}
	public void setCdkey(String cdkey) {
		this.cdkey = cdkey;
	}
	public String getMainJob() {
		return mainJob;
	}
	public void setMainJob(String mainJob) {
		this.mainJob = mainJob;
	}
	
}
