package com.red.Scheduler;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CronFileDelete {
	
	@Value("#{redInfo['file.logsPath']}")
	private String filePath;
	
	public void cronfiledelete1() throws Exception {
		
		System.out.println("-----------------Log File Delete-----------------");
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		
		long todayMil = cal.getTimeInMillis();
		long oneDayMil = 24*60*60*1000;
		
		Calendar fileCal = Calendar.getInstance();
		Date fileDate = null;
		
		File path = new File(filePath);
		File[] list = path.listFiles();
		
		for(int i=0; i<list.length; i++) {
			
			fileDate = new Date(list[i].lastModified());
			
			fileCal.setTime(fileDate);
			long diffMil = todayMil - fileCal.getTimeInMillis();
			
			int diffDay = (int)(diffMil/oneDayMil);
			
			if(diffDay > 1 && list[i].exists()) {
				list[i].delete();
			}
			
		}
		
	}

}
