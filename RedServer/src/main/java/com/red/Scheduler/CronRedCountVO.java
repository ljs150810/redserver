package com.red.Scheduler;

public class CronRedCountVO {
	
	private int selectRow = 50000;
	//private int selectRow = 3;
	private int startRow;
	private int endRow;
	
	public int getSelectRow() {
		return selectRow;
	}
	public void setSelectRow(int selectRow) {
		this.selectRow = selectRow;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

}
