package com.red.Scheduler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CronRed {
	
	@Autowired
	@Qualifier("sqlSessionTemplateDev")
	private SqlSessionTemplate sqlSessionDev;	//개발
	
	@Value("#{dbInfo['dbCafe.url']}")
	private String url;
	
	@Value("#{dbInfo['dbCafe.username']}")
	private String username;
	
	@Value("#{dbInfo['dbCafe.password']}")
	private String password;
	
	private static final String namespace = "com.red.Mapper.Scheduler";
	
	/* 데이터 이관 */
	public void cronred1() throws Exception {
		System.out.println("--------------------------------------Cron Start!!--------------------------------------");
		
		int cnt = 0;
		int forCnt = 0;
		int startRow = 0;
		int endRow = 0;
		String sql = "";
		CronRedCountVO CRCountVo = null;
		
		/*
		 * 1. 기존 데이터 삭제
		 */
		Connection conn = null;
		PreparedStatement stmt = null;
		conn = DriverManager.getConnection(url, username, password);
		conn.setAutoCommit(false);
		
		sql = "DELETE FROM tbl_character";
		stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		
		sql = "DELETE FROM tbl_pet";
		stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		
		sql = "DELETE FROM tbl_item";
		stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		
		sql = "DELETE FROM tbl_user";
		stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		
		conn.commit();
		conn.close();
		
		/*
		 * 2. 데이터 이관
		 */
		
		//2-1. 캐릭터 정보
		CRCountVo = new CronRedCountVO();
		
		cnt = sqlSessionDev.selectOne(namespace+".selectCharacterTotal");
		forCnt = cnt / CRCountVo.getSelectRow();
		startRow = 0;
		endRow = CRCountVo.getSelectRow();
		
		if(cnt % CRCountVo.getSelectRow() != 0) {
			forCnt = forCnt + 1;
		}
		
		sql = "INSERT INTO tbl_character(WHICHTYPE, REGISTNUMBER, BASEIMAGENUMBER, ORIGINALIMAGENUMBER, MAPID, NAME, NAMECOLOR, JOBRANK, JOBANCESTRY, GOLD, LOGINCOUNT, LV, HP, FORCEPOINT, VITAL, STR, TOUGH, QUICK, MAGIC, DUELPOINT, CDKEY, MAINJOB) ";
		sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		for(int i=0; i<forCnt; i++) {
			
			if(startRow != 0) {
				endRow = endRow + CRCountVo.getSelectRow();
			}
			
			CRCountVo.setStartRow(startRow);
			CRCountVo.setEndRow(endRow);
			
			List<CronRedCharacterVO> CRCharacterVo = sqlSessionDev.selectList(namespace+".selectCharacter", CRCountVo);
			
			conn = null;
			stmt = null;
			conn = DriverManager.getConnection(url, username, password);
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			
			for(CronRedCharacterVO tempVo : CRCharacterVo) {
				stmt.setInt(1, tempVo.getWhichType());
				stmt.setInt(2, tempVo.getRegistNumber());
				stmt.setInt(3, tempVo.getBaseImageNumber());
				stmt.setInt(4, tempVo.getOriginalImageNumber());
				stmt.setInt(5, tempVo.getMapId());
				stmt.setString(6, tempVo.getName());
				stmt.setInt(7, tempVo.getNameColor());
				stmt.setInt(8, tempVo.getJobRank());
				stmt.setInt(9, tempVo.getJobAncesry());
				stmt.setInt(10, tempVo.getGold());
				stmt.setInt(11, tempVo.getLoginCount());
				stmt.setInt(12, tempVo.getLv());
				stmt.setInt(13, tempVo.getHp());
				stmt.setInt(14, tempVo.getForcePoint());
				stmt.setInt(15, tempVo.getVital());
				stmt.setInt(16, tempVo.getStr());
				stmt.setInt(17, tempVo.getTough());
				stmt.setInt(18, tempVo.getQuick());
				stmt.setInt(19, tempVo.getMagic());
				stmt.setInt(20, tempVo.getDuelPoint());
				stmt.setString(21, tempVo.getCdkey());
				stmt.setString(22, tempVo.getMainJob());
				
				stmt.addBatch();
				
				stmt.clearParameters();
			}
			
			stmt.executeBatch();
			stmt.clearBatch();
			
			conn.commit();
			conn.close();
			
			startRow = endRow;
		}
		
		/* 초기화 시작 */
		cnt = 0;
		forCnt = 0;
		startRow = 0;
		endRow = 0;
		sql = "";
		/* 초기화 끝 */
		
		//2-2. 펫 정보
		CRCountVo = new CronRedCountVO();
		
		cnt = sqlSessionDev.selectOne(namespace+".selectPetTotal");
		forCnt = cnt / CRCountVo.getSelectRow();
		startRow = 0;
		endRow = CRCountVo.getSelectRow();
		
		if(cnt % CRCountVo.getSelectRow() != 0) {
			forCnt = forCnt + 1;
		}
		
		sql = "INSERT INTO tbl_pet(unk1, baseImgnum, basebaseimgnum, lEVEL, hP, mP, vital, str, tgh, quick, migic, race, attrib_Fire, attrib_Wind, attrib_Earth, attrib_Water, eXP, bounsPoint, putPetTime, petName, userPetName, unk95, cdKey, ownerName, unk98, ownerCdKey, registNumber, position, uuid) ";
		sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		for(int i=0; i<forCnt; i++) {
			
			if(startRow != 0) {
				endRow = endRow + CRCountVo.getSelectRow();
			}
			
			CRCountVo.setStartRow(startRow);
			CRCountVo.setEndRow(endRow);
			
			List<CronRedPetVO> CRPetVo = sqlSessionDev.selectList(namespace+".selectPet", CRCountVo);
			
			conn = null;
			stmt = null;
			conn = DriverManager.getConnection(url, username, password);
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			
			for(CronRedPetVO tempVo : CRPetVo) {
				stmt.setInt(1, tempVo.getUnk1());
				stmt.setInt(2, tempVo.getBaseImgnum());
				stmt.setInt(3, tempVo.getBasebaseimgnum());
				stmt.setInt(4, tempVo.getlEVEL());
				stmt.setInt(5, tempVo.gethP());
				stmt.setInt(6, tempVo.getmP());
				stmt.setInt(7, tempVo.getVital());
				stmt.setInt(8, tempVo.getStr());
				stmt.setInt(9, tempVo.getTgh());
				stmt.setInt(10, tempVo.getQuick());
				stmt.setInt(11, tempVo.getMigic());
				stmt.setInt(12, tempVo.getRace());
				stmt.setInt(13, tempVo.getAttribFire());
				stmt.setInt(14, tempVo.getAttribWind());
				stmt.setInt(15, tempVo.getAttribEarth());
				stmt.setInt(16, tempVo.getAttribWater());
				stmt.setInt(17, tempVo.geteXP());
				stmt.setInt(18, tempVo.getBounsPoint());
				stmt.setInt(19, tempVo.getPutPetTime());
				stmt.setString(20, tempVo.getPetName());
				stmt.setString(21, tempVo.getUserPetName());
				stmt.setString(22, tempVo.getUnk95());
				stmt.setString(23, tempVo.getCdKey());
				stmt.setString(24, tempVo.getOwnerName());
				stmt.setInt(25, tempVo.getUnk98());
				stmt.setString(26, tempVo.getOwnerCdKey());
				stmt.setInt(27, tempVo.getRegistNumber());
				stmt.setInt(28, tempVo.getPosition());
				stmt.setString(29, tempVo.getUuid());
				
				stmt.addBatch();
				
				stmt.clearParameters();
			}
			
			stmt.executeBatch();
			stmt.clearBatch();
			
			conn.commit();
			conn.close();
			
			startRow = endRow;
		}
		
		/* 초기화 시작 */
		cnt = 0;
		forCnt = 0;
		startRow = 0;
		endRow = 0;
		sql = "";
		/* 초기화 끝 */
		
		//2-3. 아이템 정보
		CRCountVo = new CronRedCountVO();
		
		cnt = sqlSessionDev.selectOne(namespace+".selectItemTotal");
		forCnt = cnt / CRCountVo.getSelectRow();
		startRow = 0;
		endRow = CRCountVo.getSelectRow();
		
		if(cnt % CRCountVo.getSelectRow() != 0) {
			forCnt = forCnt + 1;
		}
		
		sql = "INSERT INTO tbl_item(ITEM_ID, ITEM_TRUENAME, ITEM_BASEIMAGENUMBER, ITEM_COST, ITEM_TYPE, ITEM_REMAIN, ITEM_DURABILITY, ITEM_MAXDURABILITY, ITEM_MODIFYATTACK, ITEM_MODIFYDEFENCE, ITEM_MODIFYAGILITY, ITEM_MODIFYRECOVERY, ITEM_MODIFYMAGIC, ITEM_MODIFYCRITICAL, ITEM_MODIFYCOUNTER, ITEM_MODIFYHITRATE, ITEM_MODIFYAVOID, ITEM_MODIFYHP, ITEM_MODIFYFORCEPOINT, ITEM_POISON, ITEM_SLEEP, ITEM_STONE, ITEM_DRUNK, ITEM_CONFUSION, ITEM_AMNESIA, ITEM_LEVEL, CDKEY, RegistNumber, ITEM_SAVE_ISPOOLITEM, ITEMDATAPLACENUMBER, ITEM_FIRSTNAME) ";
		sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		for(int i=0; i<forCnt; i++) {
			
			if(startRow != 0) {
				endRow = endRow + CRCountVo.getSelectRow();
			}
			
			CRCountVo.setStartRow(startRow);
			CRCountVo.setEndRow(endRow);
			
			List<CronRedItemVO> CRItemrVo = sqlSessionDev.selectList(namespace+".selectItem", CRCountVo);
			
			conn = null;
			stmt = null;
			conn = DriverManager.getConnection(url, username, password);
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			
			for(CronRedItemVO tempVo : CRItemrVo) {
				stmt.setInt(1, tempVo.getItemId());
				stmt.setString(2, tempVo.getItemTrueName());
				stmt.setInt(3, tempVo.getItemBaseImageNumber());
				stmt.setInt(4, tempVo.getItemCost());
				stmt.setInt(5, tempVo.getItemType());
				stmt.setInt(6, tempVo.getItemRemain());
				stmt.setInt(7, tempVo.getItemDurability());
				stmt.setInt(8, tempVo.getItemMaxdurability());
				stmt.setInt(9, tempVo.getItemModifyAttack());
				stmt.setInt(10, tempVo.getItemModifyDefence());
				stmt.setInt(11, tempVo.getItemModifyAgility());
				stmt.setInt(12, tempVo.getItemModifyRecovery());
				stmt.setInt(13, tempVo.getItemModifyMagic());
				stmt.setInt(14, tempVo.getItemModifyCritical());
				stmt.setInt(15, tempVo.getItemModifyCounter());
				stmt.setInt(16, tempVo.getItemModifyHitrate());
				stmt.setInt(17, tempVo.getItemModifyAvoid());
				stmt.setInt(18, tempVo.getItemModifyHp());
				stmt.setInt(19, tempVo.getItemModifyForcePoint());
				stmt.setInt(20, tempVo.getItemPoison());
				stmt.setInt(21, tempVo.getItemSleep());
				stmt.setInt(22, tempVo.getItemStone());
				stmt.setInt(23, tempVo.getItemDrunk());
				stmt.setInt(24, tempVo.getItemConfusion());
				stmt.setInt(25, tempVo.getItemAmnesia());
				stmt.setInt(26, tempVo.getItemLevel());
				stmt.setString(27, tempVo.getCdKey());
				stmt.setInt(28, tempVo.getRegistNumber());
				stmt.setString(29, tempVo.getItemSaveIspoolItem());
				stmt.setInt(30, tempVo.getItemdataplacenumber());
				stmt.setString(31, tempVo.getItemFirstname());
				
				stmt.addBatch();
				
				stmt.clearParameters();
			}
			
			stmt.executeBatch();
			stmt.clearBatch();
			
			conn.commit();
			conn.close();
			
			startRow = endRow;
		}
		
		/* 초기화 시작 */
		cnt = 0;
		forCnt = 0;
		startRow = 0;
		endRow = 0;
		sql = "";
		/* 초기화 끝 */
		
		//2-4. 유저 정보
		CRCountVo = new CronRedCountVO();
				
		cnt = sqlSessionDev.selectOne(namespace+".selectUserTotal");
		forCnt = cnt / CRCountVo.getSelectRow();
		startRow = 0;
		endRow = CRCountVo.getSelectRow();
		
		if(cnt % CRCountVo.getSelectRow() != 0) {
			forCnt = forCnt + 1;
		}
		
		sql = "INSERT INTO tbl_user(AccountID, AccountPassword, EnableFlg, TrialFlg, DownFlg, ExpFlg, SequenceNumber, UseFlg, BadMsg, CdKey) ";
		sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		for(int i=0; i<forCnt; i++) {
			
			if(startRow != 0) {
				endRow = endRow + CRCountVo.getSelectRow();
			}
			
			CRCountVo.setStartRow(startRow);
			CRCountVo.setEndRow(endRow);
			
			List<CronRedUserVO> CRUserVo = sqlSessionDev.selectList(namespace+".selectUser", CRCountVo);
			
			conn = null;
			stmt = null;
			conn = DriverManager.getConnection(url, username, password);
			conn.setAutoCommit(false);
			stmt = conn.prepareStatement(sql);
			
			for(CronRedUserVO tempVo : CRUserVo) {
				stmt.setString(1, tempVo.getAccountID());
				stmt.setString(2, tempVo.getAccountPassword());
				stmt.setInt(3, tempVo.getEnableFlg());
				stmt.setInt(4, tempVo.getTrialFlg());
				stmt.setInt(5, tempVo.getDownFlg());
				stmt.setInt(6, tempVo.getExpFlg());
				stmt.setInt(7, tempVo.getSequenceNumber());
				stmt.setInt(8, tempVo.getUseFlg());
				stmt.setInt(9, tempVo.getBadMsg());
				stmt.setString(10, tempVo.getCdKey());
				
				stmt.addBatch();
				
				stmt.clearParameters();
			}
			
			stmt.executeBatch();
			stmt.clearBatch();
			
			conn.commit();
			conn.close();
			
			startRow = endRow;
		}
		
		System.out.println("--------------------------------------Cron End!!--------------------------------------");
	}

}
