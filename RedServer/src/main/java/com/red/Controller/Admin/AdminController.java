package com.red.Controller.Admin;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.red.Service.Admin.AdminService;
import com.red.Service.User.LoginService;
import com.red.Util.PagingUtil;
import com.red.Vo.Admin.AdminBoardVo;
import com.red.Vo.Admin.AdminVo;
import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.LoginVo;

@Controller
@RequestMapping("/Admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private LoginService loginService;
	
	
	@RequestMapping("/Main.do")
	public ModelAndView goMain(ModelAndView mv) {
		
		mv.setViewName("Admin/Join/JoinPage");
		
		return mv;
	}
	
	
	@ResponseBody
	@RequestMapping("/Join.do")
	public ModelAndView AdminJoinProc(ModelAndView mv, AdminVo vo) throws Exception {
		
		List<AdminVo> applyUserList = new ArrayList<AdminVo>();
		String resultMsg = "";
		
		try {
			
			int totalRecord = adminService.TotalRecordCount(vo);
			
			vo.setTotalRecord(totalRecord);
			
			vo = PagingUtil.getPagingData1(vo);
			
			applyUserList = adminService.GetUserApplications(vo);
			
			mv.addObject("applyUserList", applyUserList);
			mv.addObject("pagingVo", vo);
			mv.setViewName("Admin/Join/JoinPage");
		} catch (Exception e) {
			resultMsg = "오류가 발생하였습니다.";
			mv.addObject("errorMessage", resultMsg);
		}
		
		return mv;
	}
	
	@RequestMapping("/UserMain.do")
	public ModelAndView goUserMain(ModelAndView mv,HttpSession session) throws Exception {
		
		List<AdminBoardVo> boardCount = adminService.GetBoardCount();
		LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
		
		String cdKey = loginSession.getUserId();
		List<String> cdKeyList = new ArrayList<String>();
		
		for (int i = 1; i <= 30; i++) {
			cdKeyList.add(cdKey + i);
		}
		
		DecimalFormat formatter = new DecimalFormat("###,###");
		
		CharacterInfoVo totalGolds = new CharacterInfoVo();
		totalGolds = loginService.GetTotalGold(cdKeyList);
		
		totalGolds.setFormattedTotalGold(formatter.format(totalGolds.getTotalGold()));
		totalGolds.setFormattedTotalPoolGold(formatter.format(totalGolds.getTotalPoolGold()));
		totalGolds.setTotalFireRemain((int)totalGolds.getFire());
		totalGolds.setTotalWindRemain((int)totalGolds.getWind());
		totalGolds.setTotalWaterRemain((int)totalGolds.getWater());
		totalGolds.setTotalEarthRemain((int)totalGolds.getEarth());
		
		if(totalGolds != null) {
			session.setAttribute("totalGolds", totalGolds);
		} 
		
		List<CharacterInfoVo> totalAcInfo = loginService.getTotalAc(cdKeyList);
		
		if(totalAcInfo != null) {
			session.setAttribute("totAc", totalAcInfo);
		}
		
        mv.addObject("boardCount", boardCount);
        
        AdminBoardVo bvo = new AdminBoardVo();
        bvo.setGrpSeq("5");
        List<AdminBoardVo> dtlNm = new ArrayList<AdminBoardVo>();
        
        dtlNm = adminService.getEasyCodeDetail(bvo);
        
        if(dtlNm != null && !dtlNm.isEmpty()) {
        	session.setAttribute("downLoadLink", dtlNm);
        }
        
        bvo.setGrpSeq("6");
        dtlNm = adminService.getEasyCodeDetail(bvo);
        if(dtlNm != null && !dtlNm.isEmpty()) {
        	session.setAttribute("profileImgs", dtlNm);
        }
        
		mv.setViewName("User/Main/MainPage");
		
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/UserConfirm.do")
	public String LoginProc(AdminVo vo) throws Exception{

		String retVal = "";

	    try {

	        int result = adminService.UserConfirm(vo);
	        
	        if(result != 0) {
	        	int res = adminService.UserAccountInsert(vo);
	        	if(res > 0) {
	        		retVal = "0";
	        	} else {
	        		retVal = "1";
	        	}
	        } else {
	        	retVal = "1";
	        }
	        
	    } catch(Exception e) {
	        retVal = "오류가 발생하였습니다.";
	        e.printStackTrace();
	    }
	    
        return retVal;
	}

	@RequestMapping("/UserInfo.do")
	public ModelAndView goUserInfo(ModelAndView mv, AdminVo vo) throws Exception {
		
		List<AdminVo> getUserInfoList = new ArrayList<AdminVo>();
		
		try {
			
			int totalRecord = adminService.TotalUserRecordCount(vo);
			
			vo.setTotalRecord(totalRecord);
			
			vo = PagingUtil.getPagingData1(vo);
			
			getUserInfoList = adminService.GetUserInfo(vo);
			
			if (getUserInfoList == null || getUserInfoList.isEmpty()) {
				mv.addObject("emptyUserInfo", "가입된 계정 목록이 없습니다.");
			} else {
				mv.addObject("getUserInfoList", getUserInfoList);
				mv.addObject("pagingVo", vo);
				mv.setViewName("Admin/UserList/UserListPage");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mv;
	}
}
