package com.red.Controller.User;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.red.Service.User.RegisterService;
import com.red.Vo.User.RegisterVo;
import com.red.Vo.User.UserVo;

@Controller
@RequestMapping("/Register")
public class RegisterController {

	@Autowired
	private RegisterService registerService;

	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@ResponseBody
	@RequestMapping("/RegduplChk.do")
	public int RegisterduplCheck(String userId) throws Exception {
		
		int result = 0;
		
		try {
			
			result = registerService.duplicationChk(userId);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@ResponseBody
	@RequestMapping("/RegduplNick.do")
	public int duplNickNmChk(String userNick) throws Exception {
		
		int result = 0;
		
		try {
			
			result = registerService.duplNickNmChk(userNick);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/RegEmailChk.do")
	public Map<String, Object> RegEmailChk(RegisterVo vo) throws Exception {
	    Random r = new Random();
	    int rnumber = r.nextInt(8888) + 1111;
	    Map<String, Object> response = new HashMap<String, Object>();
	    
	    vo.setRnumber(rnumber);

	    MimeMessage message = mailSender.createMimeMessage();
	    
	    try {
	        // 이메일 중복 체크
	        boolean emailExists = registerService.isEmailExists(vo);
	        
	        if (emailExists) {
	            response.put("status", "error");
	            response.put("message", "중복된 이메일이 있습니다.");
	            return response;
	        }
	        
	        // 이메일 발송
	        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
	        helper.setFrom("redongam1216@gmail.com");
	        helper.setTo(vo.getUserEmail());
	        helper.setSubject("인증번호 발송 메일");
	        helper.setText("요청하신 RedServer의 인증번호는 " + Integer.toString(rnumber) + "입니다.");
	        mailSender.send(message);
	        
	        // 인증번호 저장
	        int result = registerService.emailCertifyNumber(vo);
	        
	        response.put("status", "success");
	        response.put("result", result);
	    } catch (Exception e) {
	        e.printStackTrace();
	        response.put("status", "error");
	        response.put("message", "오류가 발생하였습니다.");
	    }
	    
	    return response;
	}
	
	@ResponseBody
	@RequestMapping("/RegMailNumber.do")
	public int RegMailNumber(RegisterVo vo) throws Exception {
		
		int result = registerService.checkCertify(vo);
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/RegSignUp.do")
	public int RegSignUp(UserVo vo, HttpServletRequest request) throws Exception {
		
		int result = 0;
		
		try {
			
			vo.setDltYn("N");
			
			if ("Y".equals(vo.getExistingUserYn())) {
				// 기존 유저라면 userRole을 1로 설정
	            vo.setUserRole(1);  
	        } else {
	        	// 신규 유저라면 userRole을 0으로 설정
	            vo.setUserRole(0);
	        }
			
			String clientIp = getClientIp(request);
			
	        vo.setClientIp(clientIp);
	        
			result = registerService.signup(vo);
			
		} catch(Exception e) {
			e.printStackTrace();
			result = 0;
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("/FindExistingUser.do")
	public String FindExistingUser(UserVo vo) throws Exception {
		
		String retVal = "";
	   
	    try {
	    	// tbl_user에서 게임 계정 평문 비밀번호와 아이디를 조회
	    	List<UserVo> findGameUser = registerService.findExistingUser(vo);
	    	if (findGameUser == null || findGameUser.isEmpty()) {
	            retVal = "unknown";
	        } else if (findGameUser.get(0).getEnableFlg() == 0) {
	        	retVal = "denied";
	        } else {
	        	// tbl_user에 계정이 있으면 login_info 웹계정이 있는지 확인
	        	// 없으면 기존유저 회원가입 success
	        	List<UserVo> findWebUser = registerService.findWebUserIdPw(vo);
		        if (findWebUser == null || findWebUser.isEmpty()) {
		            retVal ="success";
				} else {
					// login_info 웹계정이 있고 서로 비밀번호 암호화하여 매칭후 맞으면 이미 가입 alert
					String accountPassword = findGameUser.get(0).getAccountPassword();
					String userPw = findWebUser.get(0).getUserPw();

					if (accountPassword.equals(userPw)) {
						retVal = "alreadySignedUp";
					} 
				}
	        }
	    } catch(Exception e) {
	    	e.printStackTrace();
	    	retVal = "error";
	    }
	    
	    return retVal;
	}
	
	
	private String getClientIp(HttpServletRequest request) {
		
		String ip = "";
		
		try {
			
			ip = request.getHeader("X-Forwarded-For");
			
		    if(ip != null && !ip.isEmpty() && !"unknown".equalsIgnoreCase(ip)) {
		        String[] ips = ip.split(",");
		        ip = ips[0];
		    } else {
		        ip = request.getHeader("Proxy-Client-IP");
		        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
		            ip = request.getHeader("WL-Proxy-Client-IP");
		            if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
		                ip = request.getRemoteAddr();
		            }
		        }
		    }
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	    return ip;
    }
	
}
