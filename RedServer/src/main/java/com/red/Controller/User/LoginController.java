package com.red.Controller.User;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.red.Service.User.BoardService;
import com.red.Service.User.CodeService;
import com.red.Service.User.LoginService;
import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.CodeVo;
import com.red.Vo.User.LoginVo;
import com.red.Vo.User.UserVo;

@Controller
@RequestMapping("/Login")
public class LoginController {
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private CodeService codeService;
	
	@Autowired
	private BoardService boardService;
	
	@RequestMapping("/goMainPage.do")
	public ModelAndView goMainPage(ModelAndView mv) throws Exception {

		try {
			mv.setViewName("User/Main/MainPage");
			List<BoardVo> boardCount = boardService.GetBoardCount();
			mv.addObject("boardCount", boardCount);
		} catch (Exception e) {
			mv.addObject("error", "오류가 발생하였습니다.");
			mv.setViewName("User/Board/ErrorPage");
		}
		return mv;
		
	}
	
	@RequestMapping("/LoginMain.do")
	public ModelAndView LoginMain(ModelAndView mv) throws Exception{
		
		mv.setViewName("User/Login/Login");
		
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/LoginProc.do")
	public String LoginProc(LoginVo vo, HttpSession session) throws Exception{

		String retVal = "";
		boolean retFlag = false;

	    LoginVo listVo = new LoginVo();

	    try {

	        listVo = loginService.LoginList(vo);

	        if(listVo == null) {
	            retVal = "계정정보가 없습니다.";
	        } else if (!vo.getUserPw().equals(listVo.getUserPw())) {
	            retVal = "비밀번호가 일치하지 않습니다.";
	        } else if (listVo.getUserRole() <= 0) {
	            retVal = "계정신청 승인이 아직 되지 않았습니다.";
	        } else {
	        	retFlag = true;
	        	retVal = "success";
	        }
	        
	        if (retFlag) {
	            session.setAttribute("userInfo", listVo);
	            session.setAttribute("userNick", listVo.getUserNick());
	        }
	        
	        retVal = URLEncoder.encode(retVal, "UTF-8").replace("+", "%20");
	        
	    } catch(Exception e) {
	        retVal = "오류가 발생하였습니다.";
	        e.printStackTrace();
	    }

        return retVal;
	}
	
	@RequestMapping("/RegPageProc.do")
	public ModelAndView RegPage(ModelAndView mv) throws Exception{
		
		mv.setViewName("User/Login/Register");
		
		return mv;
	}
	
	@RequestMapping("/MessageBoxProc.do")
	public ModelAndView MessageBox(ModelAndView mv) {
		
		mv.setViewName("User/Board/MessagePage");
		
		return mv;
	}
	
	@RequestMapping("/MyPageProc.do")
	public ModelAndView MyPageProc(ModelAndView mv, LoginVo vo) throws Exception{
		
		try {
			
            //List<LoginVo> listVo = loginService.selectMyPage(vo);		//사용안함
            
            mv.addObject("listVo", vo.getUserId());
            mv.addObject("listVo", vo.getUserNick());
            mv.setViewName("User/Login/MyPage");
            
        } catch (Exception e) {
            mv.addObject("error", "오류가 발생하였습니다.");
            mv.setViewName("User/Board/ErrorPage");
            
            e.printStackTrace();
        }
		
        return mv;
	}
	
	@RequestMapping("/MainProc.do")
	public ModelAndView MainProc(HttpSession session, ModelAndView mv, LoginVo vo) throws Exception{
		
		try {
			
			LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
			
			String cdKey = loginSession.getUserId();
			List<String> cdKeyList = new ArrayList<String>();
			
			for (int i = 1; i <= 30; i++) {
				cdKeyList.add(cdKey + i);
			}
			
			DecimalFormat formatter = new DecimalFormat("###,###");
			
			CharacterInfoVo totalGolds = new CharacterInfoVo();
			totalGolds = loginService.GetTotalGold(cdKeyList);
			
			totalGolds.setFormattedTotalGold(formatter.format(totalGolds.getTotalGold()));
			totalGolds.setFormattedTotalPoolGold(formatter.format(totalGolds.getTotalPoolGold()));
			totalGolds.setTotalFireRemain((int)totalGolds.getFire());
			totalGolds.setTotalWindRemain((int)totalGolds.getWind());
			totalGolds.setTotalWaterRemain((int)totalGolds.getWater());
			totalGolds.setTotalEarthRemain((int)totalGolds.getEarth());
			
			List<CharacterInfoVo> totalAcInfo = loginService.getTotalAc(cdKeyList);
			
			if(totalAcInfo != null) {
				session.setAttribute("totAc", totalAcInfo);
			}
			
			if(totalGolds != null) {
				session.setAttribute("totalGolds", totalGolds);
			}
			
            List<String> stringList = new ArrayList<String>();
            int userTotalCount = loginService.GetUserCount() / 30; 
            List<UserVo> getNewUser = loginService.GetNewUser();

            List<BoardVo> boardCount = boardService.GetBoardCount();
            
            BoardVo bvo = new BoardVo();
            
            bvo.setGrpSeq("5");
            List<BoardVo> dtlNm = new ArrayList<BoardVo>();
            
            dtlNm = boardService.getEasyCodeDetail(bvo);
            
            if(dtlNm != null && !dtlNm.isEmpty()) {
            	session.setAttribute("downLoadLink", dtlNm);
            }
            
            bvo.setGrpSeq("6");
            dtlNm = boardService.getEasyCodeDetail(bvo);
            if(dtlNm != null && !dtlNm.isEmpty()) {
            	session.setAttribute("profileImgs", dtlNm);
            }
            
            mv.addObject("boardCount", boardCount);
            int accountCnt = 30;
            String userId = vo.getUserId();
            
            for (int i = 0; i < accountCnt; i++) {
                stringList.add(userId + (i + 1));
            }
            
            List<LoginVo> userList = loginService.UserList(stringList);
            session.setAttribute("user", userList);

            ObjectMapper mapper = new ObjectMapper();
            String getNewUserJson = mapper.writeValueAsString(getNewUser);
            session.setAttribute("getNewUserJson", getNewUserJson);
            session.setAttribute("userTotalCount", userTotalCount);
            
            if(loginSession.getUserRole() == 999 && "Y".equals(loginSession.getGmYn())) {
//            	mv.setViewName("User/Main/MainPage");
            	mv.addObject("userNick", loginSession.getUserNick());
				mv.setViewName("Admin/Main/MainPage");
            } else {
            	mv.setViewName("User/Main/MainPage");
            }
            
        } catch (Exception e) {
            mv.addObject("error", "오류가 발생하였습니다.");
            mv.setViewName("User/Board/ErrorPage");
            
            e.printStackTrace();
        }
		
        return mv;
    }
	
	
	
	@RequestMapping("/TotalItemViewProc.do")
	public ModelAndView TotalItemViewProc(HttpSession session, 
	                                      @RequestParam(value = "sin", required = false) String searchItemName, 
	                                      ModelAndView mv) throws Exception {
	    try {
	        LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
	        String cdKey = loginSession.getUserId();

	        List<String> cdKeyList = new ArrayList<String>();
	        for (int i = 1; i <= 30; i++) {
	            cdKeyList.add(cdKey + i);
	        }

	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("cdKeyList", cdKeyList); 
	        params.put("searchItemName", searchItemName);

	        List<CharacterInfoVo> totalItemList = loginService.GetTotalItemView(params);

	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	        if (totalItemList == null || totalItemList.isEmpty()) {
	            mv.addObject("emptyItemList", "검색된 아이템이 없습니다.");
	            mv.addObject("totalItemList", null); // 빈 리스트를 전달
	        } else {
	            for (CharacterInfoVo item : totalItemList) {
	                long unixTime = item.getItemCreateTime();
	                LocalDateTime createTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(unixTime), ZoneId.systemDefault());
	                item.setFormattedCreateTime(createTime.format(formatter));
	            }
	            mv.addObject("totalItemList", totalItemList);
	        }

	        mv.setViewName("User/Board/TotalInfoView");
	    } catch (Exception e) {
	    	e.printStackTrace();
	        mv.addObject("error", "오류가 발생하였습니다.");
	        mv.setViewName("User/Board/ErrorPage");
	    }
	    return mv;
	}




	
	@RequestMapping("/TotalPetViewProc.do")
	public ModelAndView TotalPetViewProc(HttpSession session, @RequestParam(value = "sin", required = false) String searchPetName, ModelAndView mv) throws Exception {
		
		try {
			LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
			String cdKey = loginSession.getUserId();
			List<String> cdKeyList = new ArrayList<String>();
			
			for (int i = 1; i <= 30; i++) {
				cdKeyList.add(cdKey + i);
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("cdKeyList", cdKeyList); 
	        params.put("searchPetName", searchPetName);
	        
			List<CharacterInfoVo> totalPetList = loginService.GetTotalPetView(params);
			
			if(!totalPetList.isEmpty()) {
				mv.addObject("totalPetList", totalPetList);
				mv.setViewName("User/Board/TotalInfoView");
			} else {
				mv.addObject("emptyPetList", "목록이 없습니다.");
				mv.setViewName("User/Board/TotalInfoView");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "오류가 발생하였습니다.");
            mv.setViewName("User/Board/ErrorPage");
		}
		
		return mv;
	}
	
	@RequestMapping("/TotalCurrencyViewProc.do")
	public ModelAndView TotalCurrencyViewProc(HttpSession session, ModelAndView mv) throws Exception {
		
		try {
			LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
			String cdKey = loginSession.getUserId();
			List<String> cdKeyList = new ArrayList<String>();
			
			for (int i = 1; i <= 30; i++) {
				cdKeyList.add(cdKey + i);
			}
			
			DecimalFormat formatter = new DecimalFormat("###,###");
			
			
			List<CharacterInfoVo> totalCurrencyList = new ArrayList<CharacterInfoVo>();
			totalCurrencyList = loginService.GetTotalCurrencyView(cdKeyList);
			
			for(int i = 0; i < totalCurrencyList.size(); i++) {
				totalCurrencyList.get(i).setFormattedGold(formatter.format(totalCurrencyList.get(i).getGold()));
				totalCurrencyList.get(i).setFormattedPoolGold(formatter.format(totalCurrencyList.get(i).getPoolGold()));
			}
			
			if(!totalCurrencyList.isEmpty()) {
				mv.addObject("totalCurrencyList", totalCurrencyList);
				mv.setViewName("User/Board/TotalInfoView");
			} else {
				mv.addObject("emptyGoldList", "목록이 없습니다.");
				mv.setViewName("User/Board/TotalInfoView");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			mv.addObject("error", "오류가 발생하였습니다.");
			mv.setViewName("User/Board/ErrorPage");
		}
		
		return mv;
	}
	
	
//	@PostMapping("/UserAcProc.do")
//	public ModelAndView UserAcProc(LoginVo vo, ModelAndView mv) throws Exception {
//		
//		CharacterInfoVo civo = new CharacterInfoVo();
//		
//		try {
//			
//			// 좌측캐릭터 우선 쿼리
//			if(vo.getRegistNo() == 0) {
//				civo.setRegistNumber(14);
//				civo.setCdKey(vo.getAccountId());
//			} else {
//				civo.setRegistNumber(vo.getRegistNo());
//				civo.setCdKey(vo.getAccountId_1());
//			}
//			
//            mv.addObject("acId", civo.getCdKey());
//			
//			// Item List
//			List<CharacterInfoVo> getCharacterItemInfo = loginService.GetCharacterItemInfo(civo);
//			// Pet List
//			List<CharacterInfoVo> getCharacterPetInfo = loginService.GetCharacterPetInfo(civo);
//			// Code List
//			List<CodeVo> codeList = codeService.GetCodeDtlList();
//			
//			if (vo.getRegistNo() == 14) {
//	            for (int i = 0; i < getCharacterItemInfo.size(); i++) {
//	                if (getCharacterItemInfo.get(i).getItemSaveIspoolItem().equals("0")) {
//	                    int itemDataPlaceNumber = getCharacterItemInfo.get(i).getItemDataPlaceNumber();
//	                    if (itemDataPlaceNumber >= 0 && itemDataPlaceNumber <= 7) {
//	                        mv.addObject("Equipment" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                        mv.addObject("EquipName" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemTrueName());
//	                        mv.addObject("EquipLevel" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemLevel());
//	                        mv.addObject("EquipDurability" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemDurability());
//	                        mv.addObject("EquipMaxDurability" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemMaxDurability());
//	                        mv.addObject("EquipAtk" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAttack());
//	                        mv.addObject("EquipDef" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyDefence());
//	                        mv.addObject("EquipAgil" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAgility());
//	                        mv.addObject("EquipMagic" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyMagic());
//	                        mv.addObject("EquipCri" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyCritical());
//	                        mv.addObject("EquipCounter" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyCounter());
//	                        mv.addObject("EquipHit" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyHitRate());
//	                        mv.addObject("EquipAvoid" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAvoid());
//	                        mv.addObject("EquipRcv" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyRecovery());
//	                        mv.addObject("EquipLP" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyHp());
//	                        mv.addObject("EquipFP" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyForcePoint());
//	                        mv.addObject("EquipPoison" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getPoison());
//	                        mv.addObject("EquipSleep" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getSleep());
//	                        mv.addObject("EquipStone" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getStone());
//	                        mv.addObject("EquipDrunk" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getDrunk());
//	                        mv.addObject("EquipConf" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getConfusion());
//	                        mv.addObject("EquipAmne" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getAmnesia());
//	                    }else {
//	                    	String attributeName = "Inventory";
//	                    	mv.addObject(attributeName, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                    }
//	                }else {
//	                	String attributeName = "Storage";
//	                	mv.addObject(attributeName, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                }
//	            }
//	        } else if (vo.getRegistNo() == 15) {
//	        	civo.setRegistNumber(15);
//	        	getCharacterItemInfo = loginService.GetCharacterItemInfo(civo);
//	        	getCharacterPetInfo = loginService.GetCharacterPetInfo(civo);
//	            for(int i = 0; i < getCharacterItemInfo.size(); i++) {
//	                if(getCharacterItemInfo.get(i).getItemSaveIspoolItem().equals("0")) {
//	                    int itemDataPlaceNumber = getCharacterItemInfo.get(i).getItemDataPlaceNumber();
//	                    if (itemDataPlaceNumber >= 0 && itemDataPlaceNumber <= 7) {
//	                    	mv.addObject("Equipment" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                        mv.addObject("EuipName" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemTrueName());
//	                        mv.addObject("EuipLevel" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemLevel());
//	                        mv.addObject("EuipDurability" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemDurability());
//	                        mv.addObject("EuipMaxDurability" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemMaxDurability());
//	                        mv.addObject("EuipAtk" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAttack());
//	                        mv.addObject("EuipDef" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyDefence());
//	                        mv.addObject("EuipAgil" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAgility());
//	                        mv.addObject("EuipMagic" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyMagic());
//	                        mv.addObject("EuipCri" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyCritical());
//	                        mv.addObject("EuipCounter" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyCounter());
//	                        mv.addObject("EuipHit" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyHitRate());
//	                        mv.addObject("EuipAvoid" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyAvoid());
//	                        mv.addObject("EuipRcv" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyRecovery());
//	                        mv.addObject("EuipLP" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyHp());
//	                        mv.addObject("EuipFP" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getItemModifyForcePoint());
//	                        mv.addObject("EuipPoison" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getPosition());
//	                        mv.addObject("EuipSleep" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getSleep());
//	                        mv.addObject("EuipStone" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getStone());
//	                        mv.addObject("EuipDrunk" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getDrunk());
//	                        mv.addObject("EuipConf" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getConfusion());
//	                        mv.addObject("EuipAmne" + itemDataPlaceNumber, getCharacterItemInfo.get(i).getAmnesia());
//	                    } else {
//	                    	String attributeName = "Inventory";
//	                    	mv.addObject(attributeName, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                    	
//	                    }
//	                } else {
//	                	String attributeName = "Storage";
//	                	mv.addObject(attributeName, getCharacterItemInfo.get(i).getItemBaseImageNumber());
//	                }
//	            }
//	        }
//
//			
//			if((getCharacterItemInfo == null || getCharacterItemInfo.isEmpty()) && (getCharacterPetInfo == null || getCharacterPetInfo.isEmpty())) {
//				String infoEmpty = "캐릭터 정보가 없습니다.";
//	            mv.addObject("InfoEmpty", infoEmpty);
//	        } else {
//	            mv.addObject("CharacterItemInfo", getCharacterItemInfo);
//	            mv.addObject("CharacterPetInfo", getCharacterPetInfo);
//				DecimalFormat formatter = new DecimalFormat("###,###");
//				String formattedGold = formatter.format(getCharacterItemInfo.get(0).getGold());
//				mv.addObject("FormattedGold", formattedGold);
//				mv.addObject("CodeList", codeList);
//	            mv.setViewName("User/Board/CharacterView");
//	        }
//			
//		}catch(Exception e) {
//			String error = "오류가 발생하였습니다.";
//			mv.addObject("ErrorMessage", error);
//			
//			e.printStackTrace();
//		}
//		
//		return mv;
//	}
	
	@PostMapping("/UserAcProc.do")
	public ModelAndView UserAcProc(LoginVo vo, ModelAndView mv, HttpSession session) throws Exception {

	    CharacterInfoVo civo = new CharacterInfoVo();
	    List<CharacterInfoVo> getCharacterItemInfo = null;
	    List<CharacterInfoVo> getCharacterPetInfo = null;
	    List<CodeVo> codeList = null;

	    try {
	        // 좌측캐릭터 우선 쿼리
	        if (vo.getRegistNo() == 0) {
	            civo.setRegistNumber(14);
	            civo.setCdKey(vo.getAccountId());
	        } else {
	            civo.setRegistNumber(vo.getRegistNo());
	            civo.setCdKey(vo.getAccountId_1());
	        }

	        mv.addObject("acId", civo.getCdKey());

	        getCharacterItemInfo = loginService.GetCharacterItemInfo(civo);
	        getCharacterPetInfo = loginService.GetCharacterPetInfo(civo);
	        codeList = codeService.GetCodeDtlList();

	        if (civo.getRegistNumber() == 14) {
	        	mv.addObject("regNo", 14);
	            if (getCharacterItemInfo != null && !getCharacterItemInfo.isEmpty()) {
	                processCharacterInfo(getCharacterItemInfo, mv);
	            } else {
	                mv.addObject("InfoEmpty", "캐릭터 정보가 없습니다.");
	            }
	        } else if (civo.getRegistNumber() == 15) {
	        	mv.addObject("regNo", 15);
	            if (getCharacterItemInfo != null && !getCharacterItemInfo.isEmpty()) {
	                processCharacterInfo(getCharacterItemInfo, mv);
	            } else {
	            	mv.addObject("InfoEmpty", "캐릭터 정보가 없습니다.");
	            }
	        }
	        
	        if (getCharacterItemInfo != null && !getCharacterItemInfo.isEmpty()) {
	            int feverHaveTime = getCharacterItemInfo.get(0).getFeverHaveTime(); 
	            String formattedTime = formatSecondsToTime(feverHaveTime);
	            mv.addObject("FormattedFeverHaveTime", formattedTime);
	        }
	        
	        mv.addObject("CharacterItemInfo", getCharacterItemInfo);
	        mv.addObject("CharacterPetInfo", getCharacterPetInfo);
	        DecimalFormat formatter = new DecimalFormat("###,###");
	        String formattedGold = getCharacterItemInfo != null && !getCharacterItemInfo.isEmpty()
	                ? formatter.format(getCharacterItemInfo.get(0).getGold())
	                : "0";
	        mv.addObject("FormattedGold", formattedGold);
	        mv.addObject("CodeList", codeList);
	        mv.addObject("selectAccountId", vo.getAccountId());
	        
	        mv.setViewName("User/Board/CharacterView");

	    } catch (Exception e) {
	        mv.addObject("ErrorMessage", "오류가 발생하였습니다.");
	        e.printStackTrace();
	        mv.setViewName("User/Board/ErrorPage");
	    }

	    return mv;
	}
	
	private String formatSecondsToTime(int totalSeconds) {
	    int hours = totalSeconds / 3600;
	    int minutes = (totalSeconds % 3600) / 60;
	    if (minutes == 0) {
	        return hours + "시간";
	    }
	    return hours + "시간 " + minutes + "분";
	}

	private void processCharacterInfo(List<CharacterInfoVo> getCharacterItemInfo, ModelAndView mv) {
	    if (getCharacterItemInfo != null && !getCharacterItemInfo.isEmpty()) {
	        for (int i = 0; i < getCharacterItemInfo.size(); i++) {
	            CharacterInfoVo item = getCharacterItemInfo.get(i);
	            if (item.getItemSaveIspoolItem().equals("0")) {
	                int itemDataPlaceNumber = item.getItemDataPlaceNumber();
	                if (itemDataPlaceNumber >= 0 && itemDataPlaceNumber <= 7) {
	                    mv.addObject("Equipment" + itemDataPlaceNumber, item.getItemBaseImageNumber());
	                    mv.addObject("EquipName" + itemDataPlaceNumber, item.getItemTrueName());
	                    mv.addObject("EquipLevel" + itemDataPlaceNumber, item.getItemLevel());
	                    mv.addObject("EquipDurability" + itemDataPlaceNumber, item.getItemDurability());
	                    mv.addObject("EquipMaxDurability" + itemDataPlaceNumber, item.getItemMaxDurability());
	                    mv.addObject("EquipAtk" + itemDataPlaceNumber, item.getItemModifyAttack());
	                    mv.addObject("EquipDef" + itemDataPlaceNumber, item.getItemModifyDefence());
	                    mv.addObject("EquipAgil" + itemDataPlaceNumber, item.getItemModifyAgility());
	                    mv.addObject("EquipMagic" + itemDataPlaceNumber, item.getItemModifyMagic());
	                    mv.addObject("EquipCri" + itemDataPlaceNumber, item.getItemModifyCritical());
	                    mv.addObject("EquipCounter" + itemDataPlaceNumber, item.getItemModifyCounter());
	                    mv.addObject("EquipHit" + itemDataPlaceNumber, item.getItemModifyHitRate());
	                    mv.addObject("EquipAvoid" + itemDataPlaceNumber, item.getItemModifyAvoid());
	                    mv.addObject("EquipRcv" + itemDataPlaceNumber, item.getItemModifyRecovery());
	                    mv.addObject("EquipLP" + itemDataPlaceNumber, item.getItemModifyHp());
	                    mv.addObject("EquipFP" + itemDataPlaceNumber, item.getItemModifyForcePoint());
	                    mv.addObject("EquipPoison" + itemDataPlaceNumber, item.getPoison());
	                    mv.addObject("EquipSleep" + itemDataPlaceNumber, item.getSleep());
	                    mv.addObject("EquipStone" + itemDataPlaceNumber, item.getStone());
	                    mv.addObject("EquipDrunk" + itemDataPlaceNumber, item.getDrunk());
	                    mv.addObject("EquipConf" + itemDataPlaceNumber, item.getConfusion());
	                    mv.addObject("EquipAmne" + itemDataPlaceNumber, item.getAmnesia());
	                } else {
	                    mv.addObject("Inventory", item.getItemBaseImageNumber());
	                }
	            } else {
	                mv.addObject("Storage", item.getItemBaseImageNumber());
	            }
	        }
	    }
	}

	
	@RequestMapping("/LogoutProc.do")
	public String doLogout(HttpSession session) throws Exception {
		
		session.invalidate();
		return "redirect:/Login/LoginMain.do/";
		
	}
	
	@RequestMapping("/AdminProc.do")
	public ModelAndView AdminProc(LoginVo vo, ModelAndView mv, HttpSession session) throws Exception {
		
		LoginVo listVo = new LoginVo();
		
		String resultMsg = "";
		String userNick = "";
		
		try {
			
			listVo = loginService.LoginList(vo);
			
			if (listVo == null) {
				resultMsg = "계정이 없습니다.";
				mv.addObject("errorMessage", resultMsg);
			} else {
				userNick = listVo.getUserNick();
				session.setAttribute("user", listVo);
				mv.addObject("userNick", userNick);
				mv.setViewName("Admin/Main/MainPage");
			}
			
	    } catch (Exception e) {
	        mv.addObject("errorMessage", "오류가 발생하였습니다.");
	        mv.setViewName("User/Board/ErrorPage");
	    }
		
	    return mv;
	}
	
	@ResponseBody
	@RequestMapping("ProfileImgReg.do")
	public String ProfileImgReg(HttpSession session,String profileImgNo) throws Exception {

		String rtVal = "";
		LoginVo loginSession = (LoginVo) session.getAttribute("userInfo");
		try {
			if (profileImgNo != null && profileImgNo != "") {
				loginSession.setUserImgname(profileImgNo);
				loginService.ProfileImgReg(loginSession);
				rtVal = "success";
			} else {
				rtVal = "fail";
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtVal ="error";
		}
		
		rtVal = URLEncoder.encode(rtVal, "UTF-8").replace("+", "%20");
		
		return rtVal;
	}
	
}
