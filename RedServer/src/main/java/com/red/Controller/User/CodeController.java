package com.red.Controller.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.red.Service.User.CodeService;
import com.red.Vo.User.CodeVo;


@RequestMapping("/Code")
@Controller
public class CodeController {
	
	@Autowired
	private CodeService codeService;
	
	
	List<CodeVo> CodeDtlList() throws Exception{
		
		List<CodeVo> codeList = codeService.GetCodeDtlList();
		
		return codeList;
		
	}

}
