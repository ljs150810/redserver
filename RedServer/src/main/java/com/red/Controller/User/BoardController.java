package com.red.Controller.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.red.Service.User.BoardService;
import com.red.Service.User.CodeService;
import com.red.Util.PagingUtil;
import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.ReplyVo;


@RequestMapping("/Board")
@Controller
public class BoardController {
	
	@Autowired
	CodeService codeService;
	
	@Autowired
	BoardService boardService;
	
	@Value("#{redInfo['file.path']}")
	private String filePath;
	
	@ResponseBody
	@RequestMapping("/GoBoardProc.do")
	public ModelAndView goBoard(ModelAndView mv, BoardVo bvo, @RequestParam(value = "sin", required = false) String boardSearch) throws Exception {
	    try {
	        
	        int boardCode = 0;
			if(!"".equals(bvo.getBoardCode())) {
				boardCode = bvo.getBoardCode();
			}
			

	        if (boardCode == 0) {
	            mv.addObject("boardCtgError", "해당 게시물의 카테고리가 존재하지 않습니다.");
	            mv.setViewName("User/Board/BoardPage");
	            return mv;
	        }

	        bvo.setBoardCode(boardCode);
	        
	        if ("all".equals(bvo.getTradeType())) {
	            bvo.setTradeType(null);
	        }
	        Map<String, Object> params = new HashMap<String, Object>();
	        
	        params.put("boardCode", boardCode); 
	        params.put("boardSearch", boardSearch);
	        params.put("tradeType", bvo.getTradeType());
	        params.put("bvo", bvo);
	        
	        int totalRecord = boardService.GetTotalRecordCount(params);
	        bvo.setTotalRecord(totalRecord);

	        BoardVo pagingVo = PagingUtil.getPagingData(bvo);
	        
	        params.put("pagingVo", pagingVo);
	        
	        
	        List<BoardVo> boardList = boardService.GetBoardList(params);
	        
	        if (boardList.size() == 0) {
	            mv.addObject("emptyList", "게시물이 없습니다.");
	        }
	        
	        mv.addObject("boardCode", boardCode);
	        mv.addObject("boardList", boardList);
	        mv.addObject("pagingVo", pagingVo);
	    } catch (Exception e) {
	        e.printStackTrace();
	        mv.addObject("error", "오류가 발생하였습니다.");
	        mv.setViewName("User/Board/ErrorPage");
	    }

	    mv.setViewName("User/Board/BoardPage");
	    return mv;
	}
	
	@RequestMapping("/DownLoadProc.do")
	public ModelAndView goDownLoadPage(ModelAndView mv) throws Exception{
		
		mv.setViewName("User/Board/DownLoadPage");
		
		return mv;
		
	}
	
	@RequestMapping("/GoWriteBoardProc.do")
	public ModelAndView goWriteBoard(ModelAndView mv) throws Exception{
		
		mv.setViewName("User/Board/BoardWritePage");
		
		return mv;
		
	}
	
	@RequestMapping("/GoNoticeBoardProc.do")
	public ModelAndView goNoticeBoard(ModelAndView mv, BoardVo vo, CharacterInfoVo cvo) throws Exception{
		
		try {
			
			int boardCode = 0;
			
			if(!"".equals(vo.getBoardCode())){
				boardCode = vo.getBoardCode();
			}
			
			if(boardCode > 0) {
				BoardVo boardDetail = boardService.BoardDetail(vo);
				if (boardDetail != null && boardDetail.getContent() != null) {
			        String content = boardDetail.getContent()
			            .replaceAll("[\\u200B-\\u200D\\uFEFF]", "") // Zero-width space 및 비정상 문자를 제거
			            .replaceAll("\\uFFFD", ""); // 대체 문자 제거

			        boardDetail.setContent(content);
			    }
				mv.addObject("boardDetail", boardDetail);
			}
			
			mv.addObject("boardCode", boardCode);
			
			if(boardCode == 6) {
				mv.setViewName("User/Board/AuctionPage");
				System.out.println("경매장 글쓰기 페이지 입니다");
			} else {
				mv.setViewName("User/Board/NoticePage");
				System.out.println("일반 글쓰기 페이지 입니다.");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			mv.setViewName("User/Board/ErrorPage");
		}
		
		return mv;
	}
	
	@ResponseBody
	@RequestMapping("/AuctionProc.do")
	public ModelAndView goAuctionBoard(ModelAndView mv, CharacterInfoVo cvo, HttpSession session) throws Exception{
	    try {

	        if (cvo.getCdKey() != null) {
	        	if(cvo.getAuctionType().equals("pet")) {
	        		List<CharacterInfoVo> auctionPetList = boardService.GetAuctionPetList(cvo);
	        		mv.addObject("auctionPetList", auctionPetList);
	        		mv.addObject("pet", "pet");
	        	}else {
	        		List<CharacterInfoVo> auctionItemList = boardService.GetAuctionItemList(cvo);
	        		mv.addObject("auctionItemList", auctionItemList);
	        		mv.addObject("item", "item");
	        	}	
	        		mv.addObject("acid", cvo.getCdKey());
	        } else {
	            mv.addObject("acEmpty", "경매 정보가 없습니다.");
	        }
	        
	        mv.addObject("boardCode", cvo.getBoardCode());
	        
	    } catch (Exception e) {
	        mv.addObject("error", "에러가 발생하였습니다: " + e.getMessage());
	        mv.setViewName("User/Board/ErrorPage");
	        e.printStackTrace(); // 에러 로그 출력
	    }
	    session.setAttribute("aucId", cvo.getCdKey());
	    mv.setViewName("User/Board/AuctionPage");
	    return mv;
	}
	
	@ResponseBody
	@RequestMapping("/BoardDetailViewProc.do")
	public BoardVo doBoardDetailView(BoardVo bvo, ReplyVo rvo) throws Exception{
		
		BoardVo boardDetail = new BoardVo();
		
		try {
			
			if(bvo.getBoardCode() == 6) {
				
				boardDetail = boardService.AuctionDetail(bvo);
				
				if(boardDetail != null) {
					List<ReplyVo> replyList = boardService.GetReplyList(rvo);
					
					boardDetail.setReplyList(replyList);
					DecimalFormat formatter = new DecimalFormat("###,###");
					boardDetail.setFormatAuctionPrice(formatter.format(boardDetail.getAuctionPrice()));
					List<ReplyVo> bidTopList = boardService.GetBidTopList(rvo);
		            boardDetail.setBidTopList(bidTopList);
		            
					if(boardDetail.getAuctionType().equals("item")) {
						boardDetail.setAuctionType("아이템");
					} else {
						boardDetail.setAuctionType("수마");
					}
				}
				
			} else {
				boardDetail = boardService.BoardDetail(bvo);
				
				if(boardDetail != null) {
					List<ReplyVo> replyList = boardService.GetReplyList(rvo);
		            boardDetail.setReplyList(replyList);
		            System.out.println(bvo.getBoardCode());
		            System.out.println(bvo.getBoardNo());
		            BoardVo prevPost = boardService.getPreviousPost(bvo);
	                BoardVo nextPost = boardService.getNextPost(bvo);
	                
	                boardDetail.setPreviousPost(prevPost);
	                boardDetail.setNextPost(nextPost);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return boardDetail;
	}
	
	@ResponseBody
	@RequestMapping("/ReplyDeleteProc.do")
	public String doDeleteReply(ReplyVo rvo) throws Exception {
		String returnVal ="";
		
		try {
			if(0 > rvo.getBoardNo() || ("").equals(rvo.getRegister()) || 0 > rvo.getReplySeq() || 0 > rvo.getBidSeq()) {
				returnVal="댓글 수정에 실패하였습니다.";
			}
			else {
				boardService.ReplyDelete(rvo);
				returnVal="성공적으로 삭제되었습니다.";
			}
		}catch (Exception e) {
			e.printStackTrace();
			returnVal="오류가 발생하였습니다.";
		}
		
		return returnVal;
	}
	
	@ResponseBody
	@RequestMapping("/DeleteBoardProc.do")
	public String doDeleterBoard(BoardVo bvo) throws Exception {
		
		String returnVal ="";
		
		try {
			if(bvo.getBoardNo() != 0) {
				if(bvo.getBoardCode() == 6) {
					boardService.BoardDelete(bvo);
					boardService.AuctionDelete(bvo);
					boardService.deleteBidInfo(bvo.getBoardNo());
					returnVal = "경매게시물이 정상적으로 삭제 되었습니다.";
				}else {
					boardService.BoardDelete(bvo);
					boardService.DeleteRepliesByBoardNo(bvo);	
					returnVal = "게시물이 정상적으로 삭제 되었습니다.";
				}
			}else {
				returnVal = "선택된 게시물이 없습니다.";
			}
		}catch (Exception e) {
			e.printStackTrace();
			returnVal= "오류가 발생하였습니다.";
		}
		
		returnVal = URLEncoder.encode(returnVal, "UTF-8").replace("+", "%20");
		return returnVal;
	}
	
	@ResponseBody
	@RequestMapping("/BoardPageProc.do")
	public BoardVo doBoardPage(BoardVo bvo) throws Exception {
		
		String boardCtg = "";
		int boardCode = 0;
		String boardCtgName ="";
		
		BoardVo codeDetail = new BoardVo();
		
		try {

			String boardName = bvo.getBoardName();
			
			if ("notice".equals(boardName)) {
				boardCode = 1;
				boardCtgName ="공지사항";
				boardCtg = "notice";
		    } else if ("patch".equals(boardName)) {
		    	boardCode = 2;
		    	boardCtgName ="패치노트";
		    	boardCtg = "patch";
		    } else if ("function".equals(boardName)) {
		    	boardCode = 3;
		    	boardCtgName ="레드서버 기능";
		    	boardCtg = "function";
		    } else if ("guide".equals(boardName)) {
		    	boardCode = 4;
		    	boardCtgName ="레드서버 가이드";
		    	boardCtg = "guide";
		    } else if ("event".equals(boardName)) {
		    	boardCode = 5;
		    	boardCtgName ="이벤트";
		    	boardCtg = "event";
		    } else if ("auction".equals(boardName)) {
		    	boardCode = 6;
		    	boardCtgName ="경매장";
		    	boardCtg = "auction";
		    } else if ("trade".equals(boardName)) {
		    	boardCode = 7;
		    	boardCtgName ="레드장터";
		    	boardCtg = "trade";
		    } else if ("communication".equals(boardName)) {
		    	boardCode = 8;
		    	boardCtgName ="자유게시판";
		    	boardCtg = "communication";
		    } else if ("information".equals(boardName)) {
		    	boardCode = 9;
		    	boardCtgName ="정보게시판";
		    	boardCtg = "information";
		    } else if ("knowHow".equals(boardName)) {
		    	boardCode = 10;
		    	boardCtgName ="공략게시판";
		    	boardCtg = "knowHow";
		    } else if ("quest".equals(boardName)) {
		    	boardCode = 11;
		    	boardCtgName ="퀘스트게시판";
		    	boardCtg = "quest";
		    } else if ("suggestion".equals(boardName)) {
		    	boardCode = 12;
		    	boardCtgName ="건의게시판";
		    	boardCtg = "suggestion";
		    } else if ("qna".equals(boardName)) {
		    	boardCode = 13;
		    	boardCtgName ="QnA게시판";
		    	boardCtg = "qna";
		    } else {
		        throw new IllegalArgumentException("ERROR : " + boardName);
		    }
			
			bvo.setDtlSeq(boardCode);
			bvo.setBoardCtg(boardCtg);
			bvo.setBoardCtgName(boardCtgName);
			
			codeDetail = boardService.getCodeDetail(bvo);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return codeDetail;
	}
	
	@ResponseBody
	@RequestMapping("/BoardRegistProc.do")
	public String doBoardReg(BoardVo bvo, HttpSession session) throws Exception{
		
		
		String returnVal = "";
		String userNick = "BoardRP";
		int boardCode = 0;
		
		try {
			
			if(!"".equals(bvo.getBoardCode())) {
				boardCode = bvo.getBoardCode();
			}
	
			bvo.setBoardCode(boardCode);
		
			if (bvo.getTitle() != null && !bvo.getTitle().isEmpty()) {
		           bvo.setTitle(bvo.getTitle()); // Removed unescape utility
		       }
		    if (bvo.getContent() != null && !bvo.getContent().isEmpty()) {
		        bvo.setContent(bvo.getContent());
		    }
			
			if(!"".equals((String) session.getAttribute("userNick"))) {
				userNick = (String) session.getAttribute("userNick");
			}

			bvo.setRegister(userNick);

			if (bvo.getTitle() == null || bvo.getTitle().isEmpty() || bvo.getContent() == null
					|| bvo.getContent().isEmpty()) {
				returnVal = "제목 또는 내용을 입력해주세요.";
			} else {
				if (bvo.getBoardNo() > 0) {
					boardService.BoardUpdate(bvo);
					returnVal = "게시글이 정상적으로 수정되었습니다.";
				} else {
					boardService.BoardInsert(bvo);
					if(boardCode == 6) {
						boardService.AuctionInsert(bvo);
					}
					returnVal = "게시글이 정상적으로 등록되었습니다.";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnVal = "오류가 발생하였습니다.";
		}

		try {
			returnVal = URLEncoder.encode(returnVal, "UTF-8").replace("+", "%20");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnVal;
	}
	
	@ResponseBody
	@RequestMapping("/ReplyRegistProc.do")
	public String doRegistReply(ReplyVo vo) throws Exception {
		String returnVal = "";
		System.out.println("게시물 번호 : " + vo.getBoardNo());
		System.out.println("게시물 내용 : " + vo.getReplyContent());
		System.out.println("등록자 : " + vo.getRegister());
		System.out.println("게시물 코드 : " + vo.getBoardCode());
		
		try {
			
			if(vo.getRegister() == null || vo.getBoardNo() == 0) {
				returnVal = "댓글 등록에 실패하였습니다.";
			}
			
			if (6 == vo.getBoardCode()) {
			    vo.setBidder(vo.getRegister());

			    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    List<ReplyVo> bidTopList = boardService.GetBidTopList(vo);

			    boolean canPost = true;
			    Date now = new Date();

			    for (ReplyVo bid : bidTopList) {
			        if (vo.getRegister().equals(bid.getBidder())) {
			            Date bidRegistDate;
			            try {
			                bidRegistDate = formatter.parse(bid.getBidRegistDtm());
			                System.out.println(bidRegistDate);
			            } catch (Exception e) {
			                e.printStackTrace(); 
			                continue; 
			            }

			            Calendar calendar = Calendar.getInstance();
			            calendar.setTime(now);
			            calendar.add(Calendar.MINUTE, -3);
			            Date fiveMinutesAgo = calendar.getTime();

			            if (bidRegistDate.after(fiveMinutesAgo)) {
			                canPost = false;
			                break;
			            }
			        }
			    }

			    System.out.println(canPost);

			    int maxBidAmount = 0;
			    for (ReplyVo bid : bidTopList) {
			        maxBidAmount = Math.max(maxBidAmount, bid.getBidAmount());
			    }
			    System.out.println(maxBidAmount);

			    if (!canPost) {
			        returnVal = "3분 후에 다시 시도해주세요.";
			    } else if (vo.getBidAmount() <= maxBidAmount) {
			        returnVal = "입찰 금액이 현재 최고 입찰 금액보다 낮습니다.";
			    } else {
			        boardService.BidInsert(vo);
			        returnVal = "정상적으로 등록되었습니다.";
			    }
			} else {
				boardService.ReplyInsert(vo);
				returnVal = "댓글이 정상적으로 등록되었습니다.";
			}
				
			returnVal = URLEncoder.encode(returnVal, "UTF-8").replace("+", "%20");
			
		}catch(Exception e){
			e.printStackTrace();
			returnVal = "오류가 발생하였습니다.";
		}
		
		return returnVal;
	}
	
	@ResponseBody
	@RequestMapping("/GetTopBidProc.do")
	public String getTopBid(ReplyVo rvo) throws Exception{
		
		String returnVal ="";
		
		try {
			
			if(5 == rvo.getBoardCode()) {
				boardService.GetBidTopList(rvo);
			} else {
				returnVal ="경매게시물이 아닙니다.";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			returnVal ="오류가 발생하였습니다.";
		}
		
		return returnVal;
	}
	
	@PostMapping("/ImageUpload.do")
	public void imageUpload(HttpServletRequest req, HttpServletResponse res, MultipartFile upload, HttpServletRequest request) {
	 
	    OutputStream out = null;
	    PrintWriter printWriter = null;
	 
	    res.setCharacterEncoding("EUC-KR");
	    res.setContentType("text/html; charset=EUC-KR");
	    
	    try {
	        String originalFileName = upload.getOriginalFilename(); 
	        String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
	        
	        // 고유한 파일명 생성 (UUID 사용)
	        String uniqueFileName = UUID.randomUUID().toString() + fileExtension;
	        
	        byte[] bytes = upload.getBytes();    
	 
	        // 서버의 실제 업로드 경로 (절대 경로 사용)
	        String uploadDir = request.getRealPath("/") + filePath;
	        String uploadPath = uploadDir + uniqueFileName;
	        
	        String fileUrl = "/resources/uploadImage/" + uniqueFileName;

	        // 디렉토리가 존재하지 않으면 생성
	        File dir = new File(uploadDir);
	        if (!dir.exists()) {
	            dir.mkdirs();
	        }
	 
	        // 출력스트림을 이용하여, 업로드 작업을 진행함.
	        out = new FileOutputStream(new File(uploadPath));
	        out.write(bytes);
	        out.flush();
	 
	        // JSON 포맷으로 응답
	        printWriter = res.getWriter();
	        String json = String.format("{\"uploaded\" : 1, \"filename\" : \"%s\", \"url\" : \"%s\"}", uniqueFileName, fileUrl);
	        printWriter.println(json);
	        printWriter.flush();
	        
	    } catch(Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        if(out != null) {
	            try {
	                out.close();
	            } catch(Exception ex) {
	                ex.printStackTrace();
	            }
	        }
	        if(printWriter != null) {
	            printWriter.close();
	        }
	    }
	}
	
}
