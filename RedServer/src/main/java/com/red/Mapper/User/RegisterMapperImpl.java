package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.red.Vo.User.RegisterVo;
import com.red.Vo.User.UserVo;


@Repository("RegisterMapper")
public class RegisterMapperImpl implements RegisterMapper {
	
private static final String namespace = "com.red.Mapper.User.RegisterMapper";
	
	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
	
	@Override
	public int duplicationChk(String userId) throws Exception{
		return sqlSession.selectOne(namespace+".duplicationChk", userId);
	}

	@Override
	public int duplNickNmChk(String userNick) throws Exception {
		return sqlSession.selectOne(namespace+".duplNickNmChk", userNick);
	}
	
	@Override
	public int emailCertifyNumber(RegisterVo vo) throws Exception {
		return sqlSession.insert(namespace+".emailCertifyNumber", vo);
	}
	
	@Override
	public int checkCertify(RegisterVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".checkCertify", vo);
	}
	
	@Override
	public int signup(UserVo vo) throws Exception {
		return sqlSession.insert(namespace+".signup", vo);
	}
	
	@Override
	public int passWordChk(UserVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".passWordChk", vo);
	}
	
	@Override
	public int RegEmailChk(RegisterVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".RegEmailChk", vo);
	}

	@Override
	public List<UserVo> findExistingUser(UserVo vo) throws Exception {
	    return sqlSession.selectList(namespace + ".findExistingUser", vo);
	}

	@Override
	public List<UserVo> findWebUserIdPw(UserVo vo) throws Exception {
	    return sqlSession.selectList(namespace + ".findWebUserIdPw", vo);
	}

	@Override
	public int isEmailExists(RegisterVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".countEmail", vo);
	}
	
}
