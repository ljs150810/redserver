package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.LoginVo;
import com.red.Vo.User.UserVo;

@Repository("LoginMapper")
public class LoginImpl implements LoginMapper {
	
private static final String namespace = "com.red.Mapper.User.LoginMapper";
	
	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
	
	@Override
	public LoginVo LoginList(LoginVo vo) throws Exception{
		
		return sqlSession.selectOne(namespace+".loginList", vo);
	}
	
	@Override
	public List<LoginVo> UserList(List<String> stringList) throws Exception{
		
		return sqlSession.selectList(namespace+".userList", stringList);
	}

	@Override
	public List<CharacterInfoVo> GetCharacterItemInfo(CharacterInfoVo civo) throws Exception {
		
		return sqlSession.selectList(namespace+".getCharacterItemInfo", civo);
	}
	
	@Override
	public List<CharacterInfoVo> GetCharacterPetInfo(CharacterInfoVo civo) throws Exception {
		
		return sqlSession.selectList(namespace+".getCharacterPetInfo", civo);
	}

	@Override
	public int GetUserCount() throws Exception {
		return sqlSession.selectOne(namespace+".getUserCount");
	}

	@Override
	public List<UserVo> GetNewUser() throws Exception {
		return sqlSession.selectList(namespace+".getNewUser");
	}
	
	@Override
	public List<LoginVo> selectMyPage(LoginVo vo) throws Exception {
		return sqlSession.selectList(namespace+".selectMyPage", vo);
	}

	@Override
	public List<UserVo> GetDailyUserStats() throws Exception {
		return sqlSession.selectList(namespace+".getDailyUserStats");
	}

	@Override
	public List<UserVo> GetUserApplications() throws Exception {
		return sqlSession.selectList(namespace+".getUserApplications");
	}

	@Override
	public List<CharacterInfoVo> GetTotalItemView(Map<String, Object> params) throws Exception {
		return sqlSession.selectList(namespace+".getTotalItemView", params);
	}

	@Override
	public List<CharacterInfoVo> GetTotalPetView(Map<String, Object> params) throws Exception {
		return sqlSession.selectList(namespace+".getTotalPetView", params);
	}

	@Override
	public List<CharacterInfoVo> GetTotalCurrencyView(List<String> cdKey) throws Exception {
		return sqlSession.selectList(namespace+".getTotalCurrencyView", cdKey);
	}

	@Override
	public CharacterInfoVo GetTotalGold(List<String> cdKey) throws Exception {
		return sqlSession.selectOne(namespace+".getTotalGold", cdKey);
	}

	@Override
	public void ProfileImgReg(LoginVo vo) throws Exception {
		sqlSession.update(namespace+".ProfileImgReg", vo);
	}

	@Override
	public List<CharacterInfoVo> getTotalAc(List<String> cdKey) throws Exception {
		return sqlSession.selectList(namespace+".getTotalAc", cdKey);
	}
	

}
