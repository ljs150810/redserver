package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.ReplyVo;

public interface BoardMapper {
	
	public void BoardInsert(BoardVo vo) throws Exception;
	
	public void AuctionInsert(BoardVo vo) throws Exception;
	
	public List<BoardVo> GetBoardList(Map<String, Object> params) throws Exception;
	
	public void BoardDelete(BoardVo vo) throws Exception;
	
	public void AuctionDelete(BoardVo vo) throws Exception;
	
	public BoardVo BoardDetail(BoardVo vo) throws Exception;
	
	public BoardVo AuctionDetail(BoardVo vo) throws Exception;
	
	public void BoardUpdate(BoardVo vo) throws Exception;
	
	public int GetTotalRecordCount(Map<String, Object> params)throws Exception;
	
	public List<CharacterInfoVo> GetAuctionPetList(CharacterInfoVo vo)throws Exception;
	
	public List<CharacterInfoVo> GetAuctionItemList(CharacterInfoVo vo)throws Exception;
	
	public void ReplyInsert(ReplyVo vo) throws Exception;
	
	public void ReReplyInsert(ReplyVo vo) throws Exception;
	
	public List<ReplyVo> GetReplyList(ReplyVo vo) throws Exception;
	
	public void ReplyModify(ReplyVo rvo) throws Exception;
	
	public void ReplyDelete(ReplyVo rvo) throws Exception;
	
	public void BidInsert(ReplyVo rvo) throws Exception;
	
	public List<ReplyVo> GetBidTopList(ReplyVo rvo) throws Exception;
	
	public List<BoardVo> GetBoardCount() throws Exception;
	
	public BoardVo getCodeDetail(BoardVo vo) throws Exception;
	
	public int getBoardReplyCount(BoardVo vo) throws Exception;
	
	public void DeleteRepliesByBoardNo(BoardVo vo) throws Exception;
	
	public void deleteBidInfo(int boardNo) throws Exception;
	
	public List<BoardVo> GetRecentBoardInfo() throws Exception; 
	
	public BoardVo getPreviousPost(BoardVo bvo) throws Exception;
	
	public BoardVo getNextPost(BoardVo bvo) throws Exception;
	
	public List<BoardVo> getEasyCodeDetail(BoardVo bvo) throws Exception;
}


