package com.red.Mapper.User;

import java.util.List;

import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.CodeVo;
import com.red.Vo.User.LoginVo;

public interface CodeMapper {
	
	public List<CodeVo> GetCodeDtlList() throws Exception;

}
