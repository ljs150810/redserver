package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.ReplyVo;

@Repository("BoardMapper")
public class BoardMapperImpl implements BoardMapper{
	
private static final String namespace = "com.red.Mapper.User.BoardMapper";
	
	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
	
	@Override
	public void BoardInsert(BoardVo vo) throws Exception{
		sqlSession.insert(namespace+".boardInsert", vo);
	}

	@Override
	public List<BoardVo> GetBoardList(Map<String, Object> params) throws Exception {
		return sqlSession.selectList(namespace+".getBoardList", params);
	}

	@Override
	public void BoardDelete(BoardVo vo) throws Exception {
		sqlSession.delete(namespace+".boardDelete", vo);
	}
	
	@Override
	public void AuctionDelete(BoardVo vo) throws Exception {
		sqlSession.delete(namespace+".auctionDelete", vo);
	}

	@Override
	public BoardVo BoardDetail(BoardVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".getBoardDetail", vo);
	} 	

	@Override
	public void BoardUpdate(BoardVo vo) throws Exception {
		sqlSession.update(namespace+".boardUpdate", vo);
	}

	@Override
	public int GetTotalRecordCount(Map<String, Object> params) throws Exception {
		return sqlSession.selectOne(namespace+".getTotalRecordCount", params);
	}

	@Override
	public List<CharacterInfoVo> GetAuctionPetList(CharacterInfoVo vo) throws Exception {
		return sqlSession.selectList(namespace+".getAuctionPetList", vo);
	}

	@Override
	public List<CharacterInfoVo> GetAuctionItemList(CharacterInfoVo vo) throws Exception {
		return sqlSession.selectList(namespace+".getAuctionItemList", vo);
	}

	@Override
	public void AuctionInsert(BoardVo vo) throws Exception {
		sqlSession.insert(namespace+".auctionInsert", vo);
	}

	@Override
	public BoardVo AuctionDetail(BoardVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".getAuctionDetail", vo);
	}

	@Override
	public void ReplyInsert(ReplyVo vo) throws Exception {
		sqlSession.insert(namespace+".replyInsert", vo);
	}

	@Override
	public void ReReplyInsert(ReplyVo vo) throws Exception {
		sqlSession.insert(namespace+".reReplyInsert", vo);
	}

	@Override
	public List<ReplyVo> GetReplyList(ReplyVo vo) throws Exception {
		return sqlSession.selectList(namespace+".getReplyList", vo);
	}

	@Override
	public void ReplyModify(ReplyVo rvo) throws Exception {
		sqlSession.update(namespace+".updateReply", rvo);
	}
	
	@Override
	public void ReplyDelete(ReplyVo rvo) throws Exception {
		sqlSession.delete(namespace+".deleteReply", rvo);
	}

	@Override
	public void BidInsert(ReplyVo rvo) throws Exception {
		sqlSession.insert(namespace+".bidInsert", rvo);
	}

	@Override
	public List<ReplyVo> GetBidTopList(ReplyVo rvo) throws Exception {
		return sqlSession.selectList(namespace+".getBidTopList", rvo);
	}

	@Override
	public List<BoardVo> GetBoardCount() throws Exception {
		return sqlSession.selectList(namespace+".getBoardCount");
	}
	
	@Override
	public BoardVo getCodeDetail(BoardVo bvo) throws Exception {
		return sqlSession.selectOne(namespace+".getCodeDetail", bvo);
	}

	@Override
	public int getBoardReplyCount(BoardVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".getBoardReplyCount", vo);
	}

	@Override
	public void DeleteRepliesByBoardNo(BoardVo vo) throws Exception {
		sqlSession.delete(namespace+".deleteRepliesByBoardNo", vo);
	}

	@Override
	public void deleteBidInfo(int boardNo) throws Exception {
		sqlSession.delete(namespace+".bidDelete", boardNo);
	}

	@Override
	public List<BoardVo> GetRecentBoardInfo() throws Exception {
		return sqlSession.selectList(namespace+".getRecentBoardInfo");
	}

	@Override
	public BoardVo getPreviousPost(BoardVo bvo) throws Exception {
		return sqlSession.selectOne(namespace+".selectPreviousPost", bvo);
	}

	@Override
	public BoardVo getNextPost(BoardVo bvo) throws Exception {
		return sqlSession.selectOne(namespace+".selectNextPost", bvo);
	}

	@Override
	public List<BoardVo> getEasyCodeDetail(BoardVo bvo) throws Exception {
		return sqlSession.selectList(namespace+".getEasyCodeDetail", bvo);
	}
	
	

}
