package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import com.red.Vo.User.RegisterVo;
import com.red.Vo.User.UserVo;

public interface RegisterMapper {
	
	public int duplicationChk(String userId) throws Exception;
	public int duplNickNmChk(String userNick) throws Exception;
	public int emailCertifyNumber(RegisterVo vo) throws Exception;
	public int checkCertify(RegisterVo vo) throws Exception;
	public int signup(UserVo vo) throws Exception;
	public int passWordChk(UserVo vo) throws Exception;
	public int RegEmailChk(RegisterVo vo) throws Exception;
	public List<UserVo> findExistingUser(UserVo vo) throws Exception;
	public List<UserVo> findWebUserIdPw(UserVo vo) throws Exception;
	public int isEmailExists(RegisterVo vo) throws Exception;
	
}
