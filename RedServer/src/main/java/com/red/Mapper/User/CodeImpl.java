package com.red.Mapper.User;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.red.Vo.User.CodeVo;

@Repository("CodeMapper")
public class CodeImpl implements CodeMapper {
	
private static final String namespace = "com.red.Mapper.Code.CodeMapper";
	
	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
	
	@Override
	public List<CodeVo> GetCodeDtlList() throws Exception {
		
		return sqlSession.selectList(namespace+".getCodeDtl");
	}

}
