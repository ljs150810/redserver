package com.red.Mapper.User;

import java.util.List;
import java.util.Map;

import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.LoginVo;
import com.red.Vo.User.UserVo;

public interface LoginMapper {
	
	public LoginVo LoginList(LoginVo vo) throws Exception;
	public List<LoginVo> UserList(List<String> stringList) throws Exception;
	
	public List<CharacterInfoVo> GetCharacterItemInfo(CharacterInfoVo civo) throws Exception;
	public List<CharacterInfoVo> GetCharacterPetInfo(CharacterInfoVo civo) throws Exception;
	
	public int GetUserCount() throws Exception;
	
	public List<UserVo> GetNewUser() throws Exception;
	public List<LoginVo> selectMyPage(LoginVo vo) throws Exception;
	
	public List<UserVo> GetDailyUserStats() throws Exception;
	
	public List<UserVo> GetUserApplications() throws Exception;
	
	public List<CharacterInfoVo> GetTotalItemView(Map<String, Object> params) throws Exception;
	public List<CharacterInfoVo> GetTotalPetView(Map<String, Object> params) throws Exception;
	public List<CharacterInfoVo> GetTotalCurrencyView(List<String> cdKey) throws Exception;
	public List<CharacterInfoVo> getTotalAc(List<String> cdKey) throws Exception;
	public CharacterInfoVo GetTotalGold(List<String> cdKey) throws Exception;

	public void ProfileImgReg(LoginVo vo) throws Exception;
}
