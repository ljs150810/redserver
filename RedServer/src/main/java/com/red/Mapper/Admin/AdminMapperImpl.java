package com.red.Mapper.Admin;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.red.Vo.Admin.AdminBoardVo;
import com.red.Vo.Admin.AdminVo;
import com.red.Vo.User.BoardVo;

@Repository("AdminMapper")
public class AdminMapperImpl implements AdminMapper {
	
	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
	
	private static final String namespace = "com.red.Mapper.Admin.AdminMapper";
	
	@Override
	public List<AdminVo> GetUserApplications(AdminVo vo) throws Exception {
		return sqlSession.selectList(namespace+".getUserApplications", vo);
	}
	
	@Override
	public List<AdminBoardVo> GetBoardCount() throws Exception {
		return sqlSession.selectList(namespace+".getBoardCount");
	}
	
	@Override
	public int UserConfirm(AdminVo vo) throws Exception {
		return sqlSession.update(namespace+".userConfirm", vo);
	}
	
	@Override
	public int TotalRecordCount(AdminVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".totalRecordCount", vo);
	}
	
	@Override
	public int TotalUserRecordCount(AdminVo vo) throws Exception {
		return sqlSession.selectOne(namespace+".totalUserRecordCount", vo);
	}

	@Override
	public int UserAccountInsert(AdminVo vo) throws Exception {
		return sqlSession.insert(namespace+ ".userAccountInsert", vo);
	}

	@Override
	public String FindUserPasswordByUserId(AdminVo vo) throws Exception {
		return sqlSession.selectOne(namespace+ ".findUserPasswordByUserId", vo);
	}

	@Override
	public List<AdminVo> GetUserInfo(AdminVo vo) throws Exception {
		return sqlSession.selectList(namespace+ ".getUserInfo", vo);
	}

	@Override
	public List<AdminBoardVo> getEasyCodeDetail(AdminBoardVo bvo) throws Exception {
		return sqlSession.selectList(namespace+ ".getEasyCodeDetail", bvo);
	}

}
