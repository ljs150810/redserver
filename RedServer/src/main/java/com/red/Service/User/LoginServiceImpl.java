package com.red.Service.User;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.red.Mapper.User.LoginMapper;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.LoginVo;
import com.red.Vo.User.UserVo;

@Service("LoginService")
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	private LoginMapper userMapper;
	
	@Override
	public LoginVo LoginList(LoginVo vo) throws Exception{
		
		LoginVo loginList = userMapper.LoginList(vo);
		
		return loginList;
	}
	
	@Override
	public List<LoginVo> UserList(List<String> stringList) throws Exception{
		
		List<LoginVo> userList = userMapper.UserList(stringList);
		
		return userList;
	}

	@Override
	public List<CharacterInfoVo> GetCharacterItemInfo(CharacterInfoVo civo) throws Exception {
		
		List<CharacterInfoVo> getCharacterInfo = userMapper.GetCharacterItemInfo(civo);
		
		return getCharacterInfo;
	}
	
	@Override
	public List<CharacterInfoVo> GetCharacterPetInfo(CharacterInfoVo civo) throws Exception {
		
		List<CharacterInfoVo> getCharacterPetInfo = userMapper.GetCharacterPetInfo(civo);
		
		return getCharacterPetInfo;
	}

	@Override
	public int GetUserCount() throws Exception {
		
		int userCount = userMapper.GetUserCount();
		
		return userCount;
	}

	@Override
	public List<UserVo> GetNewUser() throws Exception {
		
		List<UserVo> getNewUser = userMapper.GetNewUser();
		
		return getNewUser;
	}
	
	@Override
	public List<LoginVo> selectMyPage(LoginVo vo) throws Exception {
		
		List<LoginVo> loginVo = userMapper.selectMyPage(vo);
		
		return loginVo;
	}

	@Override
	public List<UserVo> GetDailyUserStats() throws Exception {
		return userMapper.GetDailyUserStats();
	}

	@Override
	public List<UserVo> GetUserApplications() throws Exception {
		return userMapper.GetUserApplications();
	}

	@Override
	public List<CharacterInfoVo> GetTotalItemView(Map<String, Object> params) throws Exception {
		List<CharacterInfoVo> itemTotalList = userMapper.GetTotalItemView(params);
		return itemTotalList;
	}

	@Override
	public List<CharacterInfoVo> GetTotalPetView(Map<String, Object> params) throws Exception {
		List<CharacterInfoVo> petTotalList = userMapper.GetTotalPetView(params);
		return petTotalList;
	}

	@Override
	public List<CharacterInfoVo> GetTotalCurrencyView(List<String> cdKey) throws Exception {
		List<CharacterInfoVo> currencyTotalList = userMapper.GetTotalCurrencyView(cdKey);
		return currencyTotalList;
	}

	@Override
	public CharacterInfoVo GetTotalGold(List<String> cdKey) throws Exception {
		CharacterInfoVo totalGolds = userMapper.GetTotalGold(cdKey);
		return totalGolds;
	}

	@Override
	public void ProfileImgReg(LoginVo vo) throws Exception {
		userMapper.ProfileImgReg(vo);
	}

	@Override
	public List<CharacterInfoVo> getTotalAc(List<String> cdKey) throws Exception {
		List<CharacterInfoVo> getTotalAc = userMapper.getTotalAc(cdKey);
		return getTotalAc;
	}

}
