package com.red.Service.User;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.red.Mapper.User.BoardMapper;
import com.red.Vo.User.BoardVo;
import com.red.Vo.User.CharacterInfoVo;
import com.red.Vo.User.ReplyVo;

@Service("BoardService")
public class BoardServiceImpl implements BoardService{
	
	@Autowired
	private BoardMapper boardMapper;
	
	@Override
	public void BoardInsert(BoardVo vo) throws Exception{
		boardMapper.BoardInsert(vo);
	}

	@Override
	public List<BoardVo> GetBoardList(Map<String, Object> params) throws Exception {
		return boardMapper.GetBoardList(params);
	}

	@Override
	public void BoardDelete(BoardVo vo) throws Exception {
		boardMapper.BoardDelete(vo);
	}
	
	@Override
	public void AuctionDelete(BoardVo vo) throws Exception {
		boardMapper.AuctionDelete(vo);
	}

	@Override
	public BoardVo BoardDetail(BoardVo vo) throws Exception {
		return boardMapper.BoardDetail(vo);
	}

	@Override
	public void BoardUpdate(BoardVo vo) throws Exception {
		boardMapper.BoardUpdate(vo);
	}

	@Override
	public int GetTotalRecordCount(Map<String, Object> params) throws Exception {
		return boardMapper.GetTotalRecordCount(params);
	}

	@Override
	public List<CharacterInfoVo> GetAuctionPetList(CharacterInfoVo vo) throws Exception {
		return boardMapper.GetAuctionPetList(vo);
	}

	@Override
	public List<CharacterInfoVo> GetAuctionItemList(CharacterInfoVo vo) throws Exception {
		return boardMapper.GetAuctionItemList(vo);
	}

	@Override
	public void AuctionInsert(BoardVo vo) throws Exception {
		boardMapper.AuctionInsert(vo);
	}

	@Override
	public BoardVo AuctionDetail(BoardVo vo) throws Exception {
		return boardMapper.AuctionDetail(vo);
	}

	@Override
	public void ReplyInsert(ReplyVo vo) throws Exception {
		boardMapper.ReplyInsert(vo);
	}

	@Override
	public void ReReplyInsert(ReplyVo vo) throws Exception {
		boardMapper.ReReplyInsert(vo);
	}

	@Override
	public List<ReplyVo> GetReplyList(ReplyVo vo) throws Exception {
		return boardMapper.GetReplyList(vo);
	}

	@Override
	public void ReplyModify(ReplyVo rvo) throws Exception {
		boardMapper.ReplyModify(rvo);
	}
	
	@Override
	public void ReplyDelete(ReplyVo rvo) throws Exception {
		boardMapper.ReplyDelete(rvo);
	}

	@Override
	public void BidInsert(ReplyVo rvo) throws Exception {
		boardMapper.BidInsert(rvo);
	}

	@Override
	public List<ReplyVo> GetBidTopList(ReplyVo rvo) throws Exception {
		return boardMapper.GetBidTopList(rvo);
	}

	@Override
	public List<BoardVo> GetBoardCount() throws Exception {
		return boardMapper.GetBoardCount();
	}
	
	@Override
	public BoardVo getCodeDetail(BoardVo bvo) throws Exception {
		return boardMapper.getCodeDetail(bvo);
	}

	@Override
	public int getBoardReplyCount(BoardVo vo) throws Exception {
		int replyCnt = 0;
		
	    try {
	    	
	        replyCnt = boardMapper.getBoardReplyCount(vo);
	        
	    } catch (Exception e) {
	    	
	        System.err.println("에러발생: " + e.getMessage());
	        e.printStackTrace();

	        throw new Exception("댓글 수 조회 중 오류가 발생했습니다.", e);
	    }
	    return replyCnt;
	}

	@Override
	public void DeleteRepliesByBoardNo(BoardVo vo) throws Exception {
		boardMapper.DeleteRepliesByBoardNo(vo);
	}

	@Override
	public void deleteBidInfo(int boardNo) throws Exception {
		boardMapper.deleteBidInfo(boardNo);
	}

	@Override
	public List<BoardVo> GetRecentBoardInfo() throws Exception {
		return boardMapper.GetRecentBoardInfo();
	}

	@Override
	public BoardVo getPreviousPost(BoardVo bvo) throws Exception {
		return boardMapper.getPreviousPost(bvo);
	}

	@Override
	public BoardVo getNextPost(BoardVo bvo) throws Exception {
		return boardMapper.getNextPost(bvo);
	}

	@Override
	public List<BoardVo> getEasyCodeDetail(BoardVo bvo) throws Exception {
		return boardMapper.getEasyCodeDetail(bvo);
	}
	
}
