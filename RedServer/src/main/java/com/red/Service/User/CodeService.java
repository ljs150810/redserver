package com.red.Service.User;

import java.util.List;
import com.red.Vo.User.CodeVo;

public interface CodeService {

	public List<CodeVo> GetCodeDtlList() throws Exception;
	
}
