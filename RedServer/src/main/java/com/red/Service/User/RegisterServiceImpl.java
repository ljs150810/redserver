package com.red.Service.User;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.red.Mapper.User.RegisterMapper;
import com.red.Vo.User.RegisterVo;
import com.red.Vo.User.UserVo;

@Service("RegisterService")
public class RegisterServiceImpl implements RegisterService{
	
	@Autowired
	private RegisterMapper registerMapper;
	
	@Override
	public int duplicationChk(String userId) throws Exception {
		int result = registerMapper.duplicationChk(userId);
		return result;
	}

	@Override
	public int duplNickNmChk(String userNick) throws Exception {
		int result = registerMapper.duplNickNmChk(userNick);
		return result;
	}
	
	@Override
	public int emailCertifyNumber(RegisterVo vo) throws Exception {
		return registerMapper.emailCertifyNumber(vo);
	}
	
	@Override
	public int checkCertify(RegisterVo vo) throws Exception {
		int result = registerMapper.checkCertify(vo);
		return result;
	}
	
	@Override
	public int signup(UserVo vo) throws Exception {
		int result = registerMapper.signup(vo);
		return result;
	}
	
	@Override
	public int passWordChk(UserVo vo) throws Exception {
		int result = registerMapper.passWordChk(vo);
		return result;
	}
	
	@Override
	public int RegEmailChk(RegisterVo vo) throws Exception {
		int result = registerMapper.RegEmailChk(vo);
		return result;
	}

	@Override
	public List<UserVo> findExistingUser(UserVo vo) throws Exception {
		return registerMapper.findExistingUser(vo);
	}

	@Override
	public List<UserVo> findWebUserIdPw(UserVo vo) throws Exception {
		return registerMapper.findWebUserIdPw(vo);
	}

	@Override
	public boolean isEmailExists(RegisterVo vo) throws Exception {
        int count = registerMapper.isEmailExists(vo); // 이메일 중복 여부 확인
        return count > 0;
	}
	
}
