package com.red.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.red.Mapper.User.CodeMapper;
import com.red.Vo.User.CodeVo;

@Service("CodeService")
public class CodeServiceImpl implements CodeService{
	
	@Autowired
	private CodeMapper codeMapper;

	@Override
	public List<CodeVo> GetCodeDtlList() throws Exception {
		
		List<CodeVo> codeDtlList = codeMapper.GetCodeDtlList();
		
		return codeDtlList;
	}
	

}
