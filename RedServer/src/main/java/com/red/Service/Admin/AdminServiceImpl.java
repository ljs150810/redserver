package com.red.Service.Admin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.red.Mapper.Admin.AdminMapper;
import com.red.Vo.Admin.AdminBoardVo;
import com.red.Vo.Admin.AdminVo;
import com.red.Vo.User.BoardVo;

@Service("AdminService")
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	private AdminMapper adminMapper;
	
	@Override
	public List<AdminVo> GetUserApplications(AdminVo vo) throws Exception {
		return adminMapper.GetUserApplications(vo);
	}
	
	@Override
	public List<AdminBoardVo> GetBoardCount() throws Exception {
		return adminMapper.GetBoardCount();
	}
	
	@Override
	public int UserConfirm(AdminVo vo) throws Exception {
		
		String confirmType = vo.getConfirmType();
		
		if("0".equals(confirmType)) {
			vo.setUserRole(1);
			vo.setDltYn("N");
		} else {
			vo.setUserRole(0);
			vo.setDltYn("Y");
		}
		
		return adminMapper.UserConfirm(vo);
	}
	
	@Override
	public int TotalRecordCount(AdminVo vo) throws Exception {
		return adminMapper.TotalRecordCount(vo);
	}
	
	@Override
	public int TotalUserRecordCount(AdminVo vo) throws Exception {
		return adminMapper.TotalUserRecordCount(vo);
	}

	@Override
	public int UserAccountInsert(AdminVo vo) throws Exception {
		
		String accountPassword = FindUserPasswordByUserId(vo);
		
		for(int i = 1; i <= 30; i++) {
			
			AdminVo avo = new AdminVo();
			
			String accountId = vo.getUserId() + i;
			
			avo.setAccountId(accountId);
			avo.setAccountPassword(accountPassword);
			avo.setEnableFlg(1);
			avo.setTrialFlg(8);
			avo.setDownFlg(0);
			avo.setExpFlg(0);
			avo.setSequenceNumber(13);
			avo.setUseFlg(1);
			avo.setBadMsg(0);
			avo.setCdKey(accountId);
			
			adminMapper.UserAccountInsert(avo);
		}
		return 30;
	}

	@Override
	public String FindUserPasswordByUserId(AdminVo vo) throws Exception {
		String userPw = adminMapper.FindUserPasswordByUserId(vo);
		return userPw;
	}

	@Override
	public List<AdminVo> GetUserInfo(AdminVo vo) throws Exception {
		List<AdminVo> userInfoList = new ArrayList<AdminVo>();
		userInfoList = adminMapper.GetUserInfo(vo);
		return userInfoList;
	}

	@Override
	public List<AdminBoardVo> getEasyCodeDetail(AdminBoardVo bvo) throws Exception {
		return adminMapper.getEasyCodeDetail(bvo);
	}
	
	

}
