package com.red.Service.Admin;

import java.util.List;

import com.red.Vo.Admin.AdminBoardVo;
import com.red.Vo.Admin.AdminVo;
import com.red.Vo.User.BoardVo;

public interface AdminService {
	
	public List<AdminVo> GetUserApplications(AdminVo vo) throws Exception;
	
	public List<AdminBoardVo> GetBoardCount() throws Exception;
	
	public int UserConfirm(AdminVo vo) throws Exception;
	
	public int TotalRecordCount(AdminVo vo) throws Exception;
	
	public int TotalUserRecordCount(AdminVo vo) throws Exception;
	
	public int UserAccountInsert(AdminVo vo) throws Exception;
	
	public String FindUserPasswordByUserId(AdminVo vo) throws Exception;
	
	public List<AdminVo> GetUserInfo(AdminVo vo) throws Exception;
	
	public List<AdminBoardVo> getEasyCodeDetail(AdminBoardVo bvo) throws Exception;

}
