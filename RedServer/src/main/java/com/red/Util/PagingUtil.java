package com.red.Util;


import com.red.Vo.Admin.AdminVo;
import com.red.Vo.User.BoardVo;

public class PagingUtil {
	
	public static int pageSize = 20;
	public static int totalRecord = 0;
	public static int totalPage = 1;
	
	public static BoardVo getPagingData(BoardVo vo) throws Exception {
		
	    totalRecord = vo.getTotalRecord();
	    totalPage = totalRecord / pageSize;

	    if (totalRecord % pageSize != 0) {
	        totalPage++;
	    }

	    int pageNum = intZeroCheck(vo.getPageNum());
	    
	    int start = (pageNum - 1) * pageSize;
	    int count = pageSize;

	    vo.setStartRecord(start);
	    vo.setEndRecord(count);
	    vo.setTotalPage(totalPage);

	    return vo;
	}
	
	public static AdminVo getPagingData1(AdminVo vo) throws Exception {
		
	    totalRecord = vo.getTotalRecord();
	    totalPage = totalRecord / pageSize;

	    if (totalRecord % pageSize != 0) {
	        totalPage++;
	    }

	    int pageNum = intZeroCheck(vo.getPageNum());
	    
	    int start = (pageNum - 1) * pageSize;
	    int count = pageSize;

	    vo.setStartRecord(start);
	    vo.setEndRecord(count); 
	    vo.setTotalPage(totalPage);

	    return vo;
	}
	
	
	public static int intZeroCheck(int pageNum) throws Exception {
		if(pageNum == 0) {
			pageNum = 1;
		}
		
		return pageNum;
	}

}
