package com.red.Util;

public class StringUtil {
	
	public static String nullCheck (String strValue, String strDefault) {
		if(strValue == null || strValue.trim().length() == 0 || strValue.toLowerCase().equals("null") || strValue.toLowerCase().equals("NULL"))
			return strDefault;
		else
			return strValue;
	}
	
	public static int zeroCheck (String strValue, int strDefault) {
		int intValue = Integer.parseInt(strValue);
		if(intValue == 0)
			return strDefault;
		else
			return intValue;
	}

}
