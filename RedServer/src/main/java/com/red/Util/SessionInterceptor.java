package com.red.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.red.Vo.User.LoginVo;

public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		HttpSession session = request.getSession(false);
		String ajaxCall = (String) request.getHeader("AJAX");
		String requestUri = request.getRequestURI();
		String boardCheck = request.getParameter("boardCheck");
		
		if(session == null) {
			
			if(ajaxCall != null) {
				response.sendError(500);
			} else {
				response.sendRedirect("/");
			}
			
			return false;
		} else {
			session.setMaxInactiveInterval(7200);
			LoginVo userSession = (LoginVo) session.getAttribute("userInfo");
			
			if(userSession == null) {
				if(ajaxCall != null) {
					response.sendError(500);
				} else {
					response.sendRedirect("/");
				}
				
				return false;
			}
			
			if (requestUri.startsWith("/Admin") && 
					(userSession.getUserRole() != 999 || !"Y".equals(userSession.getGmYn()))) {
					
				if (ajaxCall != null) {
					response.sendError(500);
				} else {
					response.sendRedirect("/");
				}
				return false;
			}
		}
		
		
		
		if(requestUri.contains("GoNoticeBoardProc.do") && boardCheck == null) {
			if(ajaxCall != null) {
				response.sendError(500);
			} else {
				response.sendRedirect("/");
			}
			
			return false;
		}
		
		return true;
	}
 
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mv) throws Exception {
		
	}

}
