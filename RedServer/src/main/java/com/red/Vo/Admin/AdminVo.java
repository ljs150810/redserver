package com.red.Vo.Admin;

public class AdminVo {
	
	private String userId;
	private String userPw;
	private String userName;
	private String userNick;
	private String userEmail;
	private int userRole;
	private String regDate;
	private String retVal;
    private String salt;
    private String dltYn;
    private String clientIp;
	
    private String accountId;
	private String accountPassword;
	private int enableFlg;
	private int trialFlg;
	private int downFlg;
	private int ExpFlg;
	private int sequenceNumber;
	private int useFlg;
	private int badMsg;
	private String cdKey;
	
	private String ip;
	private String cash;
	private String credit;
	private String mlhy;
	private String time;
	private String petMoney;
	private String petMB;
	private String petMoneyType;
	private String s1;
	private String s2;
	private String s3;
	
	private String confirmType;
	
	/* ����¡ */
	private int pageNum;
	private int startRecord;
	private int endRecord;
	private int totalRecord;
	private int totalPage;
	
	private int boardCode;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRetVal() {
		return retVal;
	}
	public void setRetVal(String retVal) {
		this.retVal = retVal;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountPassword() {
		return accountPassword;
	}
	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}
	public int getEnableFlg() {
		return enableFlg;
	}
	public void setEnableFlg(int enableFlg) {
		this.enableFlg = enableFlg;
	}
	public int getTrialFlg() {
		return trialFlg;
	}
	public void setTrialFlg(int trialFlg) {
		this.trialFlg = trialFlg;
	}
	public int getDownFlg() {
		return downFlg;
	}
	public void setDownFlg(int downFlg) {
		this.downFlg = downFlg;
	}
	public int getExpFlg() {
		return ExpFlg;
	}
	public void setExpFlg(int expFlg) {
		ExpFlg = expFlg;
	}
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public int getUseFlg() {
		return useFlg;
	}
	public void setUseFlg(int useFlg) {
		this.useFlg = useFlg;
	}
	public int getBadMsg() {
		return badMsg;
	}
	public void setBadMsg(int badMsg) {
		this.badMsg = badMsg;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getCash() {
		return cash;
	}
	public void setCash(String cash) {
		this.cash = cash;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getMlhy() {
		return mlhy;
	}
	public void setMlhy(String mlhy) {
		this.mlhy = mlhy;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPetMoney() {
		return petMoney;
	}
	public void setPetMoney(String petMoney) {
		this.petMoney = petMoney;
	}
	public String getPetMB() {
		return petMB;
	}
	public void setPetMB(String petMB) {
		this.petMB = petMB;
	}
	public String getPetMoneyType() {
		return petMoneyType;
	}
	public void setPetMoneyType(String petMoneyType) {
		this.petMoneyType = petMoneyType;
	}
	public String getS1() {
		return s1;
	}
	public void setS1(String s1) {
		this.s1 = s1;
	}
	public String getS2() {
		return s2;
	}
	public void setS2(String s2) {
		this.s2 = s2;
	}
	public String getS3() {
		return s3;
	}
	public void setS3(String s3) {
		this.s3 = s3;
	}
	public String getUserNick() {
		return userNick;
	}
	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
	public int getBoardCode() {
		return boardCode;
	}
	public void setBoardCode(int boardCode) {
		this.boardCode = boardCode;
	}
	public String getDltYn() {
		return dltYn;
	}
	public void setDltYn(String dltYn) {
		this.dltYn = dltYn;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getConfirmType() {
		return confirmType;
	}
	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}
	public int getEndRecord() {
		return endRecord;
	}
	public void setEndRecord(int endRecord) {
		this.endRecord = endRecord;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

}
