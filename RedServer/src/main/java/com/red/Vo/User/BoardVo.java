package com.red.Vo.User;

import java.util.List;

public class BoardVo {
	
	private String userId;
	private int boardNo;
	private int boardCode;
	private String title;
	private String content;
	private String boardImg;
	private String register;
	private String boardName;
	private String sysRegDtm;
	private String boardCtg;
	private String boardCtgName;
	private String grpSeq;
	private String dtlName;
	private List<String> boardList;
	private int boardCount;
	private int pageNum;
	private int startRecord;
	private int endRecord;
	private int totalRecord;
	private int totalPage;
	private int dtlSeq;
	private String tradeType;
	private int replyCnt;
	private String timeAgo;
	private String userImgname;
	//��� �ʵ�
	private String auctionType;
	private String petName;
	private String itemName;
	private String auctionAccount;
	private String petImgNum;
	private String itemImgNum;
	private String startPrice;
	private int auctionPrice;
	private int bidUnit;
	private String customBidUnit;
	private String formatBidUnit;
	private String formatCustomBidUnit;
	private String formatAuctionPrice;
	private String auctionEndDate;
	
	private List<ReplyVo> replyList;
	private List<ReplyVo> bidTopList;
	
	private BoardVo previousPost;
	private BoardVo nextPost; 
	
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public int getBoardCode() {
		return boardCode;
	}
	public void setBoardCode(int boardCode) {
		this.boardCode = boardCode;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBoardImg() {
		return boardImg;
	}
	public void setBoardImg(String boardImg) {
		this.boardImg = boardImg;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public String getBoardName() {
		return boardName;
	}
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}
	public String getSysRegDtm() {
		return sysRegDtm;
	}
	public void setSysRegDtm(String sysRegDtm) {
		this.sysRegDtm = sysRegDtm;
	}
	public String getBoardCtg() {
		return boardCtg;
	}
	public void setBoardCtg(String boardCtg) {
		this.boardCtg = boardCtg;
	}
	public String getBoardCtgName() {
		return boardCtgName;
	}
	public void setBoardCtgName(String boardCtgName) {
		this.boardCtgName = boardCtgName;
	}
	public String getGrpSeq() {
		return grpSeq;
	}
	public void setGrpSeq(String grpSeq) {
		this.grpSeq = grpSeq;
	}
	public List<String> getBoardList() {
		return boardList;
	}
	public void setBoardList(List<String> boardList) {
		this.boardList = boardList;
	}
	public int getBoardCount() {
		return boardCount;
	}
	public void setBoardCount(int boardCount) {
		this.boardCount = boardCount;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(int startRecord) {
		this.startRecord = startRecord;
	}
	public int getEndRecord() {
		return endRecord;
	}
	public void setEndRecord(int endRecord) {
		this.endRecord = endRecord;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getDtlSeq() {
		return dtlSeq;
	}
	public void setDtlSeq(int dtlSeq) {
		this.dtlSeq = dtlSeq;
	}
	public String getAuctionType() {
		return auctionType;
	}
	public void setAuctionType(String auctionType) {
		this.auctionType = auctionType;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getAuctionAccount() {
		return auctionAccount;
	}
	public void setAuctionAccount(String auctionAccount) {
		this.auctionAccount = auctionAccount;
	}
	public String getPetImgNum() {
		return petImgNum;
	}
	public void setPetImgNum(String petImgNum) {
		this.petImgNum = petImgNum;
	}
	public String getItemImgNum() {
		return itemImgNum;
	}
	public void setItemImgNum(String itemImgNum) {
		this.itemImgNum = itemImgNum;
	}
	public String getStartPrice() {
		return startPrice;
	}
	public void setStartPrice(String startPrice) {
		this.startPrice = startPrice;
	}
	public int getAuctionPrice() {
		return auctionPrice;
	}
	public void setAuctionPrice(int auctionPrice) {
		this.auctionPrice = auctionPrice;
	}
	public int getBidUnit() {
		return bidUnit;
	}
	public void setBidUnit(int bidUnit) {
		this.bidUnit = bidUnit;
	}
	public String getCustomBidUnit() {
		return customBidUnit;
	}
	public void setCustomBidUnit(String customBidUnit) {
		this.customBidUnit = customBidUnit;
	}
	public String getFormatBidUnit() {
		return formatBidUnit;
	}
	public void setFormatBidUnit(String formatBidUnit) {
		this.formatBidUnit = formatBidUnit;
	}
	public String getFormatCustomBidUnit() {
		return formatCustomBidUnit;
	}
	public void setFormatCustomBidUnit(String formatCustomBidUnit) {
		this.formatCustomBidUnit = formatCustomBidUnit;
	}
	public String getFormatAuctionPrice() {
		return formatAuctionPrice;
	}
	public void setFormatAuctionPrice(String formatAuctionPrice) {
		this.formatAuctionPrice = formatAuctionPrice;
	}
	public String getAuctionEndDate() {
		return auctionEndDate;
	}
	public void setAuctionEndDate(String auctionEndDate) {
		this.auctionEndDate = auctionEndDate;
	}
	public List<ReplyVo> getReplyList() {
		return replyList;
	}
	public void setReplyList(List<ReplyVo> replyList) {
		this.replyList = replyList;
	}
	public List<ReplyVo> getBidTopList() {
		return bidTopList;
	}
	public void setBidTopList(List<ReplyVo> bidTopList) {
		this.bidTopList = bidTopList;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public int getReplyCnt() {
		return replyCnt;
	}
	public void setReplyCnt(int replyCnt) {
		this.replyCnt = replyCnt;
	}
	public String getTimeAgo() {
		return timeAgo;
	}
	public void setTimeAgo(String timeAgo) {
		this.timeAgo = timeAgo;
	}
	public BoardVo getPreviousPost() {
		return previousPost;
	}
	public void setPreviousPost(BoardVo previousPost) {
		this.previousPost = previousPost;
	}
	public BoardVo getNextPost() {
		return nextPost;
	}
	public void setNextPost(BoardVo nextPost) {
		this.nextPost = nextPost;
	}
	public String getDtlName() {
		return dtlName;
	}
	public void setDtlName(String dtlName) {
		this.dtlName = dtlName;
	}
	public String getUserImgname() {
		return userImgname;
	}
	public void setUserImgname(String userImgname) {
		this.userImgname = userImgname;
	}
	
}