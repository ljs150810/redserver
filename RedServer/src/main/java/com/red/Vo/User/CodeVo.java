package com.red.Vo.User;

/**
 * @author ���λ�
 *
 */
public class CodeVo {

	private int grpSeq;
	private int dtlSeq;
	private String grpName;
	private String dtlName;
	private String useYn;
	
	public int getGrpSeq() {
		return grpSeq;
	}
	public void setGrpSeq(int grpSeq) {
		this.grpSeq = grpSeq;
	}
	public int getDtlSeq() {
		return dtlSeq;
	}
	public void setDtlSeq(int dtlSeq) {
		this.dtlSeq = dtlSeq;
	}
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getDtlName() {
		return dtlName;
	}
	public void setDtlName(String dtlName) {
		this.dtlName = dtlName;
	}
	
}
