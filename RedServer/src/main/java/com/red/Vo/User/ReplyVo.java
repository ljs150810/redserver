package com.red.Vo.User;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class ReplyVo extends BoardVo{
	
	private int replySeq;
	private int boardNo;
	private int replyParentSeq;
	private int userRole;
	private String replyContent;
	private String replyDate;
	private String register;
	private String userImgname;
	
	
	public String getUserImgname() {
		return userImgname;
	}
	public void setUserImgname(String userImgname) {
		this.userImgname = userImgname;
	}
	//��� ���� �ʵ�
	private int bidSeq;
	private int bidAmount;
	private String bidder;
	private String bidRegistDtm;
	
	
	
	public int getBidAmount() {
		return bidAmount;
	}
	public void setBidAmount(int bidAmount) {
		this.bidAmount = bidAmount;
	}
	public String getBidder() {
		return bidder;
	}
	public void setBidder(String bidder) {
		this.bidder = bidder;
	}
	public String getBidRegistDtm() {
		return bidRegistDtm;
	}
	public void setBidRegistDtm(String bidRegistDtm) {
		this.bidRegistDtm = bidRegistDtm;
	}
	public int getReplySeq() {
		return replySeq;
	}
	public void setReplySeq(int replySeq) {
		this.replySeq = replySeq;
	}
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public int getReplyParentSeq() {
		return replyParentSeq;
	}
	public void setReplyParentSeq(int replyParentSeq) {
		this.replyParentSeq = replyParentSeq;
	}
	public String getReplyDate() {
		return replyDate;
	}
	public void setReplyDate(String replyDate) {
		this.replyDate = replyDate;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
	public int getBidSeq() {
		return bidSeq;
	}
	public void setBidSeq(int bidSeq) {
		this.bidSeq = bidSeq;
	}
	
	
	
}
