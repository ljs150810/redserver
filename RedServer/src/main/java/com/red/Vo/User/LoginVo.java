package com.red.Vo.User;

import java.util.Date;

public class LoginVo {
	
	
	private String salt;
	private String userNick;
	private int userRole;
	private String regDate;
	private String dltYn;
    private String clientIp;
	private String retVal;
	private String userId;
	private String userPw;
	private String userEmail;
	private String accountId;
	private String accountId_1;
	private String accountPassword;
	private String enableFlg;
	private String trialFlg;
	private String downFlg;
	private String ExpFlg;
	private String sequenceNumber;
	private String useFlg;
	private String badMsg;
	private String cdKey;
	private String ip;
	private String cash;
	private String credit;
	private String mlhy;
	private String time;
	private String petMoney;
	private String petMB;
	private String petMoneyType;
	private String s1;
	private String s2;
	private String s3;
	private String gmYn;
	private String userImgname;
	
	
	private int registNo;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getUserNick() {
		return userNick;
	}
	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}
	public String getRetVal() {
		return retVal;
	}
	public void setRetVal(String retVal) {
		this.retVal = retVal;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountPassword() {
		return accountPassword;
	}
	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}
	public String getEnableFlg() {
		return enableFlg;
	}
	public void setEnableFlg(String enableFlg) {
		this.enableFlg = enableFlg;
	}
	public String getTrialFlg() {
		return trialFlg;
	}
	public void setTrialFlg(String trialFlg) {
		this.trialFlg = trialFlg;
	}
	public String getDownFlg() {
		return downFlg;
	}
	public void setDownFlg(String downFlg) {
		this.downFlg = downFlg;
	}
	public String getExpFlg() {
		return ExpFlg;
	}
	public void setExpFlg(String expFlg) {
		ExpFlg = expFlg;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getUseFlg() {
		return useFlg;
	}
	public void setUseFlg(String useFlg) {
		this.useFlg = useFlg;
	}
	public String getBadMsg() {
		return badMsg;
	}
	public void setBadMsg(String badMsg) {
		this.badMsg = badMsg;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getCash() {
		return cash;
	}
	public void setCash(String cash) {
		this.cash = cash;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getMlhy() {
		return mlhy;
	}
	public void setMlhy(String mlhy) {
		this.mlhy = mlhy;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPetMoney() {
		return petMoney;
	}
	public void setPetMoney(String petMoney) {
		this.petMoney = petMoney;
	}
	public String getPetMB() {
		return petMB;
	}
	public void setPetMB(String petMB) {
		this.petMB = petMB;
	}
	public String getPetMoneyType() {
		return petMoneyType;
	}
	public void setPetMoneyType(String petMoneyType) {
		this.petMoneyType = petMoneyType;
	}
	public String getS1() {
		return s1;
	}
	public void setS1(String s1) {
		this.s1 = s1;
	}
	public String getS2() {
		return s2;
	}
	public void setS2(String s2) {
		this.s2 = s2;
	}
	public String getS3() {
		return s3;
	}
	public void setS3(String s3) {
		this.s3 = s3;
	}
	public String getAccountId_1() {
		return accountId_1;
	}
	public void setAccountId_1(String accountId_1) {
		this.accountId_1 = accountId_1;
	}
	public int getRegistNo() {
		return registNo;
	}
	public void setRegistNo(int registNo) {
		this.registNo = registNo;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getDltYn() {
		return dltYn;
	}
	public void setDltYn(String dltYn) {
		this.dltYn = dltYn;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getGmYn() {
		return gmYn;
	}
	public void setGmYn(String gmYn) {
		this.gmYn = gmYn;
	}
	public String getUserImgname() {
		return userImgname;
	}
	public void setUserImgname(String userImgname) {
		this.userImgname = userImgname;
	}
	
	
	
}
