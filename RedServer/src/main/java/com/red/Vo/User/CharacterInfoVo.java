package com.red.Vo.User;

/**
 * @author 조민상
 *
 */
public class CharacterInfoVo {
	
	/* 공통 파라미터 VO */
	private String cdKey;					// 아이디
	private int registNumber;				// 캐릭위치 14:좌측 / 15:우측
	
	/* TYPE */
	private String auctionType;
	
	/* 캐릭터 VO */
	private int whichType;					// 타입
	private int baseImageNumber;			// 이미지 번호
	private int originalImageNumber;		// 원 이미지 번호	
	private int mapId;						//
	private String name;					// 캐릭명
	private int nameColor;					// 환생 회수
	private String mainJob;					// 직업
	private int jobRank;					// 랭크업
	private int jobAncestry;				//
	private int gold;						// 골드
	private int loginCount;					// 로그인 횟수
	private int lv;							// 레벨	
	private int hp;							// HP
	private int forcePoint;					// FP
	private int vital;						// 체력
	private int str;						// 근력
	private int tough;						// 방어력
	private int quick;						// 순발력
	private int magic;						// 마력
	private int duelPoint;					// 듀얼포인트
	private int feverHaveTime;				// 작업시간
	
	/* 아이템 VO */
	private int itemId;						// 아이템 아이디
	private int itemBaseImageNumber;		// 이미지 번호
	private int itemCost;					// 가격
	private int itemType;					// 타입
	private int itemRemain;					// 수량
	private int itemDurability;				// 현재 내구도 수치
	private int itemMaxDurability;			// 최대 내구도 수치
	private int itemModifyAttack;			// 현재 공격력 수치
	private int itemModifyDefence;			// 현재 방어력 수치
	private int itemModifyAgility;			// 현재 민첩성 수치
	private int itemModifyRecovery;			// 현재 회복율 수치
	private int itemModifyMagic;			// 현재 정신 수치
	private int itemModifyCounter;			// 현재 반격 수치
	private int itemModifyCritical;			// 현재 크리티컬 수치
	private int itemModifyAvoid;			// 현재 회피 수치
	private int itemModifyHitRate;			// 현재 명중 수치
	private int itemModifyHp;				// 현재 HP 수치
	private int itemModifyForcePoint;		// 현재 FP 수치
	private int poison;						// 독 저항
	private int sleep;						// 수면 저항
	private int stone;						// 석화 저항
	private int drunk;						// 취기 저항
	private int confusion;					// 혼란 저항
	private int amnesia;					// 망각 저항
	private int itemLevel;					// 아이템 랭크
	private String itemTrueName;			// 아이템 이름
	private String itemSaveIspoolItem;		// 아이템 저장 위치 0: 장착 또는 인벤 / 1: 창고
	private int itemDataPlaceNumber;		// 아이템 현재 위치 순서
	private int itemCreateTime;				// 아이템 생성 시간
	private String formattedCreateTime;		// 아이템 생성시간 Unix -> 일반시간 변환
	private int poolGold;					// 창고 골드
	private String formattedGold;
	private String formattedTotalGold;
	private String formattedPoolGold;
	private String formattedTotalPoolGold;
	private int totalGold;
	private int totalItemRemain;
	private int totalPoolGold;
	private int totalFireRemain;
	private int totalWindRemain;
	private int totalWaterRemain;
	private int totalEarthRemain;
	private String reincarnation;
	
	/* 펫 VO */
	private int unk1;						//		
	private int level;						// 레벨
	private int baseImgNum;					// 이미지 번호
	private int baseBaseImgNum;				// 원 이미지 번호
	private String petName;					// 펫 이름
	private String userPetName;				// 펫 닉네임
	private String uuid;					//
	private int mp;							// 펫 FP
	private String petVital;				// 펫 HP
	private String petStr;					// 펫 근력
	private String petTgh;					// 펫 방어력
	private String petQuick;				// 펫 순발력
	private String petMagic;				// 펫 정신
	private double earth;					// 지속성
	private double water;					// 수속성
	private double fire;					// 화속성
	private double wind;					// 풍속성
	private int petCri;						// 크리티컬
	private int petCounter;					// 반격
	private int petHit;						// 명중
	private int petAvoid;					// 회피
	private int skillSlot;					// 스킬 칸
	private int unk98;						// 펫 위치 0: 펫창 / 1: 창고
	private int position;					// 펫 위치
	private String race;					// 펫 종족 코드
	private String ownerName;				// 소유 캐릭명
	
	private String boardCode;
	
	private int food1;	// 전골
	private int food2;	// 상어스프
	private int food3;	// 해물탕
	private int food4;	// 메론빵
	private int food5;	// 향수
	private int food6;	// 50붕어빵
	private int food7;	// 75붕어빵

	public String getCdKey() {
		return cdKey;
	}

	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}

	public int getRegistNumber() {
		return registNumber;
	}

	public void setRegistNumber(int registNumber) {
		this.registNumber = registNumber;
	}

	public String getAuctionType() {
		return auctionType;
	}

	public void setAuctionType(String auctionType) {
		this.auctionType = auctionType;
	}

	public int getWhichType() {
		return whichType;
	}

	public void setWhichType(int whichType) {
		this.whichType = whichType;
	}

	public int getBaseImageNumber() {
		return baseImageNumber;
	}

	public void setBaseImageNumber(int baseImageNumber) {
		this.baseImageNumber = baseImageNumber;
	}

	public int getOriginalImageNumber() {
		return originalImageNumber;
	}

	public void setOriginalImageNumber(int originalImageNumber) {
		this.originalImageNumber = originalImageNumber;
	}

	public int getMapId() {
		return mapId;
	}

	public void setMapId(int mapId) {
		this.mapId = mapId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNameColor() {
		return nameColor;
	}

	public void setNameColor(int nameColor) {
		this.nameColor = nameColor;
	}

	public String getMainJob() {
		return mainJob;
	}

	public void setMainJob(String mainJob) {
		this.mainJob = mainJob;
	}

	public int getJobRank() {
		return jobRank;
	}

	public void setJobRank(int jobRank) {
		this.jobRank = jobRank;
	}

	public int getJobAncestry() {
		return jobAncestry;
	}

	public void setJobAncestry(int jobAncestry) {
		this.jobAncestry = jobAncestry;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getForcePoint() {
		return forcePoint;
	}

	public void setForcePoint(int forcePoint) {
		this.forcePoint = forcePoint;
	}

	public int getVital() {
		return vital;
	}

	public void setVital(int vital) {
		this.vital = vital;
	}

	public int getStr() {
		return str;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public int getTough() {
		return tough;
	}

	public void setTough(int tough) {
		this.tough = tough;
	}

	public int getQuick() {
		return quick;
	}

	public void setQuick(int quick) {
		this.quick = quick;
	}

	public int getMagic() {
		return magic;
	}

	public void setMagic(int magic) {
		this.magic = magic;
	}

	public int getDuelPoint() {
		return duelPoint;
	}

	public void setDuelPoint(int duelPoint) {
		this.duelPoint = duelPoint;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemBaseImageNumber() {
		return itemBaseImageNumber;
	}

	public void setItemBaseImageNumber(int itemBaseImageNumber) {
		this.itemBaseImageNumber = itemBaseImageNumber;
	}

	public int getItemCost() {
		return itemCost;
	}

	public void setItemCost(int itemCost) {
		this.itemCost = itemCost;
	}

	public int getItemType() {
		return itemType;
	}

	public void setItemType(int itemType) {
		this.itemType = itemType;
	}

	public int getItemRemain() {
		return itemRemain;
	}

	public void setItemRemain(int itemRemain) {
		this.itemRemain = itemRemain;
	}

	public int getItemDurability() {
		return itemDurability;
	}

	public void setItemDurability(int itemDurability) {
		this.itemDurability = itemDurability;
	}

	public int getItemMaxDurability() {
		return itemMaxDurability;
	}

	public void setItemMaxDurability(int itemMaxDurability) {
		this.itemMaxDurability = itemMaxDurability;
	}

	public int getItemModifyAttack() {
		return itemModifyAttack;
	}

	public void setItemModifyAttack(int itemModifyAttack) {
		this.itemModifyAttack = itemModifyAttack;
	}

	public int getItemModifyDefence() {
		return itemModifyDefence;
	}

	public void setItemModifyDefence(int itemModifyDefence) {
		this.itemModifyDefence = itemModifyDefence;
	}

	public int getItemModifyAgility() {
		return itemModifyAgility;
	}

	public void setItemModifyAgility(int itemModifyAgility) {
		this.itemModifyAgility = itemModifyAgility;
	}

	public int getItemModifyRecovery() {
		return itemModifyRecovery;
	}

	public void setItemModifyRecovery(int itemModifyRecovery) {
		this.itemModifyRecovery = itemModifyRecovery;
	}

	public int getItemModifyMagic() {
		return itemModifyMagic;
	}

	public void setItemModifyMagic(int itemModifyMagic) {
		this.itemModifyMagic = itemModifyMagic;
	}

	public int getItemModifyCounter() {
		return itemModifyCounter;
	}

	public void setItemModifyCounter(int itemModifyCounter) {
		this.itemModifyCounter = itemModifyCounter;
	}

	public int getItemModifyCritical() {
		return itemModifyCritical;
	}

	public void setItemModifyCritical(int itemModifyCritical) {
		this.itemModifyCritical = itemModifyCritical;
	}

	public int getItemModifyAvoid() {
		return itemModifyAvoid;
	}

	public void setItemModifyAvoid(int itemModifyAvoid) {
		this.itemModifyAvoid = itemModifyAvoid;
	}

	public int getItemModifyHitRate() {
		return itemModifyHitRate;
	}

	public void setItemModifyHitRate(int itemModifyHitRate) {
		this.itemModifyHitRate = itemModifyHitRate;
	}

	public int getItemModifyHp() {
		return itemModifyHp;
	}

	public void setItemModifyHp(int itemModifyHp) {
		this.itemModifyHp = itemModifyHp;
	}

	public int getItemModifyForcePoint() {
		return itemModifyForcePoint;
	}

	public void setItemModifyForcePoint(int itemModifyForcePoint) {
		this.itemModifyForcePoint = itemModifyForcePoint;
	}

	public int getPoison() {
		return poison;
	}

	public void setPoison(int poison) {
		this.poison = poison;
	}

	public int getSleep() {
		return sleep;
	}

	public void setSleep(int sleep) {
		this.sleep = sleep;
	}

	public int getStone() {
		return stone;
	}

	public void setStone(int stone) {
		this.stone = stone;
	}

	public int getDrunk() {
		return drunk;
	}

	public void setDrunk(int drunk) {
		this.drunk = drunk;
	}

	public int getConfusion() {
		return confusion;
	}

	public void setConfusion(int confusion) {
		this.confusion = confusion;
	}

	public int getAmnesia() {
		return amnesia;
	}

	public void setAmnesia(int amnesia) {
		this.amnesia = amnesia;
	}

	public int getItemLevel() {
		return itemLevel;
	}

	public void setItemLevel(int itemLevel) {
		this.itemLevel = itemLevel;
	}

	public String getItemTrueName() {
		return itemTrueName;
	}

	public void setItemTrueName(String itemTrueName) {
		this.itemTrueName = itemTrueName;
	}

	public String getItemSaveIspoolItem() {
		return itemSaveIspoolItem;
	}

	public void setItemSaveIspoolItem(String itemSaveIspoolItem) {
		this.itemSaveIspoolItem = itemSaveIspoolItem;
	}

	public int getItemDataPlaceNumber() {
		return itemDataPlaceNumber;
	}

	public void setItemDataPlaceNumber(int itemDataPlaceNumber) {
		this.itemDataPlaceNumber = itemDataPlaceNumber;
	}

	public int getUnk1() {
		return unk1;
	}

	public void setUnk1(int unk1) {
		this.unk1 = unk1;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getBaseImgNum() {
		return baseImgNum;
	}

	public void setBaseImgNum(int baseImgNum) {
		this.baseImgNum = baseImgNum;
	}

	public int getBaseBaseImgNum() {
		return baseBaseImgNum;
	}

	public void setBaseBaseImgNum(int baseBaseImgNum) {
		this.baseBaseImgNum = baseBaseImgNum;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public String getUserPetName() {
		return userPetName;
	}

	public void setUserPetName(String userPetName) {
		this.userPetName = userPetName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getMp() {
		return mp;
	}

	public void setMp(int mp) {
		this.mp = mp;
	}

	public String getPetVital() {
		return petVital;
	}

	public void setPetVital(String petVital) {
		this.petVital = petVital;
	}

	public String getPetStr() {
		return petStr;
	}

	public void setPetStr(String petStr) {
		this.petStr = petStr;
	}

	public String getPetTgh() {
		return petTgh;
	}

	public void setPetTgh(String petTgh) {
		this.petTgh = petTgh;
	}

	public String getPetQuick() {
		return petQuick;
	}

	public void setPetQuick(String petQuick) {
		this.petQuick = petQuick;
	}

	public String getPetMagic() {
		return petMagic;
	}

	public void setPetMagic(String petMagic) {
		this.petMagic = petMagic;
	}

	public double getEarth() {
		return earth;
	}

	public void setEarth(double earth) {
		this.earth = earth;
	}

	public double getWater() {
		return water;
	}

	public void setWater(double water) {
		this.water = water;
	}

	public double getFire() {
		return fire;
	}

	public void setFire(double fire) {
		this.fire = fire;
	}

	public double getWind() {
		return wind;
	}

	public void setWind(double wind) {
		this.wind = wind;
	}

	public int getPetCri() {
		return petCri;
	}

	public void setPetCri(int petCri) {
		this.petCri = petCri;
	}

	public int getPetCounter() {
		return petCounter;
	}

	public void setPetCounter(int petCounter) {
		this.petCounter = petCounter;
	}

	public int getPetHit() {
		return petHit;
	}

	public void setPetHit(int petHit) {
		this.petHit = petHit;
	}

	public int getPetAvoid() {
		return petAvoid;
	}

	public void setPetAvoid(int petAvoid) {
		this.petAvoid = petAvoid;
	}

	public int getSkillSlot() {
		return skillSlot;
	}

	public void setSkillSlot(int skillSlot) {
		this.skillSlot = skillSlot;
	}

	public int getUnk98() {
		return unk98;
	}

	public void setUnk98(int unk98) {
		this.unk98 = unk98;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public int getItemCreateTime() {
		return itemCreateTime;
	}

	public void setItemCreateTime(int itemCreateTime) {
		this.itemCreateTime = itemCreateTime;
	}

	public String getFormattedCreateTime() {
		return formattedCreateTime;
	}

	public void setFormattedCreateTime(String formattedCreateTime) {
		this.formattedCreateTime = formattedCreateTime;
	}

	public int getPoolGold() {
		return poolGold;
	}

	public void setPoolGold(int poolGold) {
		this.poolGold = poolGold;
	}

	public String getFormattedGold() {
		return formattedGold;
	}

	public void setFormattedGold(String formattedGold) {
		this.formattedGold = formattedGold;
	}

	public String getFormattedPoolGold() {
		return formattedPoolGold;
	}

	public void setFormattedPoolGold(String formattedPoolGold) {
		this.formattedPoolGold = formattedPoolGold;
	}

	public int getTotalGold() {
		return totalGold;
	}

	public void setTotalGold(int totalGold) {
		this.totalGold = totalGold;
	}

	public int getTotalItemRemain() {
		return totalItemRemain;
	}

	public void setTotalItemRemain(int totalItemRemain) {
		this.totalItemRemain = totalItemRemain;
	}

	public int getTotalPoolGold() {
		return totalPoolGold;
	}

	public void setTotalPoolGold(int totalPoolGold) {
		this.totalPoolGold = totalPoolGold;
	}

	public String getFormattedTotalGold() {
		return formattedTotalGold;
	}

	public void setFormattedTotalGold(String formattedTotalGold) {
		this.formattedTotalGold = formattedTotalGold;
	}

	public String getFormattedTotalPoolGold() {
		return formattedTotalPoolGold;
	}

	public void setFormattedTotalPoolGold(String formattedTotalPoolGold) {
		this.formattedTotalPoolGold = formattedTotalPoolGold;
	}

	public int getFeverHaveTime() {
		return feverHaveTime;
	}

	public void setFeverHaveTime(int feverHaveTime) {
		this.feverHaveTime = feverHaveTime;
	}

	public int getTotalFireRemain() {
		return totalFireRemain;
	}

	public void setTotalFireRemain(int totalFireRemain) {
		this.totalFireRemain = totalFireRemain;
	}

	public int getTotalWindRemain() {
		return totalWindRemain;
	}

	public void setTotalWindRemain(int totalWindRemain) {
		this.totalWindRemain = totalWindRemain;
	}

	public int getTotalWaterRemain() {
		return totalWaterRemain;
	}

	public void setTotalWaterRemain(int totalWaterRemain) {
		this.totalWaterRemain = totalWaterRemain;
	}

	public int getTotalEarthRemain() {
		return totalEarthRemain;
	}

	public void setTotalEarthRemain(int totalEarthRemain) {
		this.totalEarthRemain = totalEarthRemain;
	}

	public String getReincarnation() {
		return reincarnation;
	}

	public void setReincarnation(String reincarnation) {
		this.reincarnation = reincarnation;
	}

	public int getFood1() {
		return food1;
	}

	public void setFood1(int food1) {
		this.food1 = food1;
	}

	public int getFood2() {
		return food2;
	}

	public void setFood2(int food2) {
		this.food2 = food2;
	}

	public int getFood3() {
		return food3;
	}

	public void setFood3(int food3) {
		this.food3 = food3;
	}

	public int getFood4() {
		return food4;
	}

	public void setFood4(int food4) {
		this.food4 = food4;
	}

	public int getFood5() {
		return food5;
	}

	public void setFood5(int food5) {
		this.food5 = food5;
	}

	public int getFood6() {
		return food6;
	}

	public void setFood6(int food6) {
		this.food6 = food6;
	}

	public int getFood7() {
		return food7;
	}

	public void setFood7(int food7) {
		this.food7 = food7;
	}	
	
}